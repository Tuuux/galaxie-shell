Make a Static Shell binary
==========================

Actually Galaxie Shell use ``pyinstaller`` python package for build a static.

That document cover only GNU/Linux systems because nobody else have make it under other OS.

Download source from git
------------------------

.. code:: bash

  cd WORKING_DIRECTORY
  git https://gitlab.com/Tuuux/galaxie-shell.git

Use the Makefile static option
------------------------------

.. code:: bash

  $> cd WORKING_DIRECTORY/galaxie-shell
  $> make static
  VIRTUAL ENV CREATION
  VIRTUAL ENV CONTROL
  INSTALL PACKAGE AS DEVELOPER
  BUILD ENTRY POINT AS STATIC BINARY

That prepare a virtual env and download everything for you, finally the binary file is under ``dist`` directory.

.. code:: bash

  $> tree ./dist/
  ./dist/
  └── glxsh-x86_64.bin

As you can see it continue to be bind on the libc, libdl, libz, then that is not cross platform , you have to build on the target platform when you want a specific binary.

.. code:: bash

  $> ldd ./dist/glxsh-x86_64.bin
        linux-vdso.so.1 (0x00007ffe90939000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f7879526000)
        libz.so.1 => /lib/x86_64-linux-gnu/libz.so.1 (0x00007f7879308000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f7879147000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f787954c000)

Advanced build
--------------
Not done yet ...