Makefile
========

The Makefile hide lot of complexity, here the return of ``make help``

.. code:: bash

  > make help
  ******************************* GLXSH MAKEFILE ********************************


  GNU GENERAL PUBLIC LICENSE V3 OR LATER (GPLV3+)
  EXEC MAKE
  NO HOLOTAPE FOUND
  NO PLUGINS TO LOAD

  Build:
    static          - Call venv , install pyinstaller python module inside the
                      virtual environment. The build determine the arch type
                      and start the static build for generate the filename.
                      The generated file will be store in ./dist directory and
                      the file name glxsh-x86_64.bin in case of command arch
                      return x86_64.
    shell           - Call static and run the shell from ./dist/glxsh-`arch`.bin

  CleaningUp:
    clean           - Remove every created directory and restart from scratch

  Documentation:
    documentations  - Sphinx command it call venv, install docs/requirements.txt
                      requirements, run sphinx-apidoc, and generate html
                      documentation.
                      Documentation is store on ./docs/source and the
                      documentation build on ./docs/build

  VirtualEnvironment:
    venv-create     - Create virtual env directory with python venv module
    venv-update     - Call venv-create and install or update packages:
                      pip, wheel and setuptools
    venv            - Call venv-update and install Galaxie Shell on the virtual
                      environment as developer mode with pip install -e .
