.. Galaxie Shell documentation master file, created by
   sphinx-quickstart on Sat Nov  7 13:51:36 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. title:: index

===========================
Galaxie Shell documentation
===========================
.. figure::  images/logo_galaxie.png
   :align:   center

Description
-----------
The goal of Galaxie-Shell is to provide a POSIX Shell for micro system like pyBoard or MicroPi.

at end it should be possible to execute POSIX Script Shell on MicroPython (Long time target).

The application Galaxie Shell is a Read Eval Print Loop (`RELP <https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop>`_) write with `python <https://www.python.org/>`_ based on Native module call ``Cmd``

The project implement POSIX Standard from the OpenGroup
https://pubs.opengroup.org/onlinepubs/9699919799/utilities/

The OpenGroup permit a implementation where all utilities are builtins command, and that is what Galaxie-Shell do.
I code it, because i like understand how a system work. Copy the code , use it , that is here for the public community.

The project provide a application class name ``GLXShell`` oriented unix SHELL. and two entry point ``glxsh``, ``glxush``.

The goal is to permit everyone to start they own project around a SHELL as UI.

Links
-----

  Codeberg: https://codeberg.org/Tuuux/galaxie-shell/

  Read the Doc: https://galaxie-shell.readthedocs.io/

  PyPI: https://pypi.org/project/galaxie-shell/

  PuPI Test: https://test.pypi.org/project/galaxie-shell/

All ready implemented features
------------------------------

* Totally autonomous All in One shell
* Capability to build a **one-file** static binary file
* Can load a script file as argument
* Can execute command from passing arguments
* Interactive shell when call without arguments
* Builtins POSIX command (basename, cat, cd, mkdir, pwd, rmdir, uname, etc ...)
* Exit status for build in or external command
* POSIX Pipe
* Alias and UnAlias
* PS1 env var can be export

Contents
--------

.. toctree::
   :maxdepth: 2

   install

.. toctree::
   :maxdepth: 1

   modules

.. toctree::
   :maxdepth: 2

   manuals
   developers

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
