.. Galaxie Shell documentation master file, created by
   sphinx-quickstart on Sat Nov  7 13:51:36 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. title:: index

===========================
Galaxie Shell documentation
===========================
.. figure::  images/logo_galaxie.png
   :align:   center

Description
-----------
Make you own thing !!!

Galaxie Shell is a Reliable Event Logging Protocol (`RELP <https://en.wikipedia.org/wiki/Reliable_Event_Logging_Protocol>`_) write with `python <https://www.python.org/>`_ based on top of `cmd2 <https://github.com/python-cmd2/cmd2>`_ it self build on top of python builtins `cmd <https://docs.python.org/3/library/cmd.html>`_.

The project provide a application class name ``GLXShell`` oriented unix SHELL. The goal is to permit everyone to start they own project around a SHELL as UI.

The Galaxie Shell use a plugin name ``builtins plugin`` it try to integrate python version of `GNU Core Utils <https://www.maizure.org/projects/decoded-gnu-coreutils/>`_ command's set.

Links
-----

  GitLab: https://gitlab.com/Tuuux/galaxie-shell/

  Read the Doc: https://galaxie-shell.readthedocs.io/

  PyPI: https://pypi.org/project/galaxie-shell/

  PuPI Test: https://test.pypi.org/project/galaxie-shell/

Contents
--------

.. toctree::
   :maxdepth: 2

   install
   modules
   manuals
   developers

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
