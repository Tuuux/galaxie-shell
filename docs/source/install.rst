.. _instalation:

============
Installation
============
You can found the Galaxie Shell Repository here:
https://codeberg.org/Tuuux/galaxie-shell

Installation via pip
--------------------

.. code:: bash

    pip install galaxie-shell

Installation via pip (test)
---------------------------

.. code:: bash

    pip install -i https://test.pypi.org/simple/ galaxie-shell


Next Step
---------

Now you can the start the **glxsh** entry point

.. code:: bash

  $> glxsh
  ******************************* GLXSHELL V0.1.2 *******************************


  GNU GENERAL PUBLIC LICENSE V3 OR LATER (GPLV3+)
  LOADER #1 SMP DEBIAN 4.19.152-1 (2020-10-18)
  EXEC (VENV) PYTHON 3.7.3
  31.36GB RAM SYSTEM
  21.55GB FREE
  NO HOLOTAPE FOUND
  LOAD PLUGINS(1): BUILTINS 0.2A

  >

or use the python package

.. code-block:: python

  #!/usr/bin/env python

  import os
  import sys
  import argparse

  from GLXShell.libs.shell import GLXShell

  def main(argv=None):
      """Run when invoked from the operating system shell"""

      glxsh_parser = argparse.ArgumentParser(description="Commands as arguments")
      glxsh_parser.add_argument(
          "command",
          nargs="?",
          help="optional commands or file to run, if no commands given, enter an interactive shell",
      )
      glxsh_parser.add_argument(
          "command_args",
          nargs=argparse.REMAINDER,
          help="if commands is not a file use optional arguments for commands",
      )

      args = glxsh_parser.parse_args(argv)

      shell = GLXShell()

      sys_exit_code = 0
      if args.command:
          if os.path.isfile(args.command):
              # we have file to execute
              shell.onecmd_plus_hooks("@{command}".format(command=args.command))
          else:
              # we have a commands, run it and then exit
              shell.onecmd_plus_hooks(
                  "{command} {args}".format(
                      command=args.command, args=" ".join(args.command_args)
                  )
              )

      else:
          # we have no commands, drop into interactive mode
          sys_exit_code = shell.cmdloop()

      return sys_exit_code


  if __name__ == "__main__":
      sys.exit(main())


