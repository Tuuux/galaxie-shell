Manuals Pages
=============

.. toctree::
   :maxdepth: 1

   man/cp.rst
   man/head.rst
   man/date.rst
   man/pwd.rst
   man/dirname.rst
   man/ls.rst
   man/cd.rst
   man/unalias.rst
   man/clear.rst
   man/mkdir.rst
   man/time.rst
   man/basename.rst
   man/touch.rst
   man/tty.rst
   man/umask.rst
   man/rmdir.rst
   man/true.rst
   man/tee.rst
   man/mv.rst
   man/env.rst
   man/exit.rst
   man/chmod.rst
   man/echo.rst
   man/tail.rst
   man/df.rst
   man/false.rst
   man/du.rst
   man/sleep.rst
   man/cat.rst
   man/uname.rst
   man/alias.rst
   man/rm.rst
