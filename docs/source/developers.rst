Developers Pages
================

.. toctree::
   :maxdepth: 1

   dev/makefile
   dev/make_static_shell_binary
   dev/sphinx