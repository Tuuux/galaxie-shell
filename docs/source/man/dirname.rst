=======
dirname
=======

.. code-block:: text

  NAME
    dirname - return the directory portion of a pathname

  SYNOPSIS
    dirname [string]

  DESCRIPTION
    The string operand shall be treated as a pathname, as defined in XBD Pathname.
    The string string shall be converted to the name of the directory containing
    the filename corresponding to the last pathname component in string.

  OPERANDS
    string  A string

