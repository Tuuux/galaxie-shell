=====
uname
=====

.. code-block:: text

  NAME
    uname - return system name

  SYNOPSIS
    uname [-a] [-s] [-n] [-r] [-v] [-m]

  DESCRIPTION
    By default, the uname utility shall write the operating system name to standard
    output. When options are specified, symbols representing one or more system
    characteristics shall be written to the standard output.

  OPTIONS
    -a  Behave as though all of the options -mnrsv were specified.
    -s  Write the name of the implementation of the operating system.
    -n  Write the name of this node within an implementation-defined
        communications network.
    -r  Write the current release level of the operating system implementation.
    -v  Write the current version level of this release of the operating system
        implementation.
    -m  Write the name of the hardware type on which the system is running.

