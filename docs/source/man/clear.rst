=====
clear
=====

.. code-block:: text

  NAME
    clear

  SYNOPSIS
    clear

  DESCRIPTION
    Clear screen

