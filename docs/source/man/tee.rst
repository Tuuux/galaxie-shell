===
tee
===

.. code-block:: text

  NAME
    tee - duplicate standard input

  SYNOPSIS
    tee [-ai] [file...]

  DESCRIPTION
    The tee utility shall copy standard input to standard output, making a copy in
    zero or more files. The tee utility shall not buffer output.

    If the -a option
    is not specified, output files shall be written.

  OPERANDS
    file  A pathname of an output file. If a file operand is '-', it refer to a
          file named '-'

  OPTIONS
    -a  Append the output to the files.
    -i  Ignore the SIGINT signal.

  EXIT STATUS
    0   The standard input was successfully copied to all output files.
    >0  An error occurred.

