==
mv
==

.. code-block:: text

  NAME
    mv - move files

  SYNOPSIS
    mv [-f] [-i] [source_file] [target_file] [target_dir]

  DESCRIPTION
    The mv utility shall move the file named by the source_file operand to the
    destination specified by the target_file. This first synopsis form is assumed
    when the final operand does not name an existing directory and is not a
    symbolic link referring to an existing directory. In this case, if source_file
    names a non-directory file and target_file ends with a trailing <slash>
    character, mv shall treat this as an error and no source_file operands will be
    processed.

  OPERANDS
    source_file  A pathname of a file or directory to be moved.
    target_file  A new pathname for the file or directory being moved.
    target_dir   A pathname of an existing directory into which to move the input
                 files.

  OPTIONS
    -f  Do not prompt for confirmation if the destination path exists. Any
        previous occurrence of the -i option is ignored.
    -i  Prompt for confirmation if the destination path exists. Any previous
        occurrence of the -f option is ignored.

