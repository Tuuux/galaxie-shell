===
pwd
===

.. code-block:: text

  NAME
    pwd - return working directory name

  SYNOPSIS
    pwd [-L] [-P]

  DESCRIPTION
    The pwd utility shall write to standard output an absolute pathname of the
    current working directory, which does not contain the filenames dot or dot-dot.

  OPTIONS
    -L  Print the value of $PWD if it names the current working directory
    -P  Print the physical directory, without any symbolic links

