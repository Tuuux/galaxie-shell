=====
sleep
=====

.. code-block:: text

  NAME
    sleep - suspend execution for an interval

  SYNOPSIS
    sleep time

  DESCRIPTION
    The sleep utility shall suspend execution for at least the integral number of
    seconds specified by the time operand.

  OPERANDS
    time  A non-negative decimal integer or float specifying the number of
          seconds for which to suspend execution.

