=====
rmdir
=====

.. code-block:: text

  NAME
    rmdir - remove directories

  SYNOPSIS
    rmdir [-p] [dir...]

  DESCRIPTION
    The rmdir utility shall remove the directory entry specified by each dir
    operand.

    Directories shall be processed in the order specified. If a directory
    and a subdirectory of that directory are specified in a single invocation of
    the rmdir utility, the application shall specify the subdirectory before the
    parent directory so that the parent directory will be empty when the rmdir
    utility tries to remove it.

  OPERANDS
    dir  A pathname of an empty directory to be removed.

  OPTIONS
    -p  Remove all directories in a pathname.

