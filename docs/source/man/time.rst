====
time
====

.. code-block:: text

  NAME
    time - time a simple command

  SYNOPSIS
    time [-p] utility [argument...]

  DESCRIPTION
    The time utility shall invoke the utility named by the utility operand with
    arguments supplied as the argument operands and write a message to standard
    error that lists timing statistics for the utility.

  OPERANDS
    utility   The name of a utility that is to be invoked.
    argument  Any string to be supplied as an argument when invoking the utility
              named by the utility operand.

  OPTIONS
    -p  Write the timing output to standard error

  EXIT STATUS
    1-125  An error occurred in the time utility.
    126    The utility specified by utility was found but could not be invoked.
    127    The utility specified by utility could not be found.

