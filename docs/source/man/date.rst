====
date
====

.. code-block:: text

  NAME
    date - write the date and time

  SYNOPSIS
    date [-u] [format...]

  DESCRIPTION
    The date utility shall write the date and time to standard output or attempt to
    set the system date and time. By default, the current date and time shall be
    written. If an operand beginning with '+' is specified, the output format of
    date shall be controlled by the conversion specifications and other text in the
    operand.

  OPERANDS
    format  When the format is specified, each conversion specifier shall be
            replaced in the standard output by its corresponding value. All other
            characters shall be copied to the output without change. The output
            shall always be terminated with a <newline>.

  OPTIONS
    -u  Perform operations as if the TZ environment variable was set to the
        string 'UTC0', or its equivalent historical value of 'GMT0'. Otherwise,
        date shall use the timezone indicated by the TZ environment variable or
        the system default if that variable is unset or null.

