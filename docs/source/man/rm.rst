==
rm
==

.. code-block:: text

  NAME
    rm - remove directory entries

  SYNOPSIS
    rm [-i] [-R, -r] [-f] file...

  DESCRIPTION
    The rm utility shall remove the directory entry specified by each file
    argument.

    If either of the files dot or dot-dot are specified as the basename
    portion of an operand (that is, the final pathname component) or if an operand
    resolves to the root directory, rm shall write a diagnostic message to standard
    error and do nothing more with such operands.

  OPERANDS
    file  A pathname of a directory entry to be removed.

  OPTIONS
    -i      Prompt for confirmation as described previously. Any previous
            occurrences of the -f option shall be ignored.
    -R, -r  Remove file hierarchies. See the DESCRIPTION.
    -f      Do not prompt for confirmation. Do not write diagnostic messages or
            modify the exit status in the case of no file operands, or in the
            case of operands that do not exist. Any previous occurrences of the
            -i option shall be ignored.

