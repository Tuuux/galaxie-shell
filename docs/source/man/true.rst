====
true
====

.. code-block:: text

  NAME
    true - return true value

  SYNOPSIS
    true

  DESCRIPTION
    The true utility shall return with exit code zero.

  EXIT STATUS
    0

