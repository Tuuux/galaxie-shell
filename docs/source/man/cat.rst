===
cat
===

.. code-block:: text

  NAME
    cat - concatenate and print files

  SYNOPSIS
    cat [-u] [file...]

  DESCRIPTION
    The cat utility shall read files in sequence and shall write their contents to
    the standard output in the same sequence.

  OPERANDS
    file  A pathname of an input file. If no file operands are specified, the
          standard input shall be used. If a file is '-', the cat utility shall
          read from the standard input at that point in the sequence. The cat
          utility shall not close and reopen standard input when it is referenced
          in this way, but shall accept multiple occurrences of '-' as a file
          operand.

  OPTIONS
    -u  Ignored

