==
df
==

.. code-block:: text

  NAME
    df - report free disk space

  SYNOPSIS
    df [-h] [-k] [-P] [-t] [file]

  DESCRIPTION
    The df utility shall write the amount of available space and file slots for
    file systems on which the invoking user has appropriate read access. File
    systems shall be specified by the file operands; when none are specified,
    information shall be written for all file systems.The format of the default
    output from df is unspecified, but all space figures are reported in 512-byte
    units, unless the -k option is specified. This output shall contain at least
    the file system names, amount of available space on each of these file systems,
    and, if no options other than -t are specified, the number of free file slots,
    or inodes, available; when -t is specified, the output shall contain the total
    allocated space as well.

  OPERANDS
    file  A pathname of a file within the hierarchy of the desired file system.
          If a file other than a FIFO, a regular file, a directory, or a special
          file representing the device containing the file system (for example,
          /dev/dsk/0s1) is specified, the results are unspecified. If the file
          operand names a file other than a special file containing a file
          system, df shall write the amount of free space in the file system
          containing the specified file operand. Otherwise, df shall write the
          amount of free space in that file system.

  OPTIONS
    -h  Print sizes in powers of 1024 (e.g., 1023M)
    -k  Use 1024-byte units, instead of the default 512-byte units, when writing
        space figures.
    -P  Produce a POSIX output
    -t  Include total allocated-space figures in the output.

