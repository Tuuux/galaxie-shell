==
du
==

.. code-block:: text

  NAME
    du - estimate file space usage

  SYNOPSIS
    du [-a|-s] [-kx] [-H|-L] [file...]

  DESCRIPTION
    The du utility shall write to standard output the size of the file space
    allocated to, and the size of the file space allocated to each subdirectory of,
    the file hierarchy rooted in each of the specified files.

  OPERANDS
    file  The pathname of a file whose size is to be written. If no file is
          specified, the current directory shall be used.

  OPTIONS
    -a  In addition to the default output, report the size of each file not of
        type directory in the file hierarchy rooted in the specified file. The -a
        option shall not affect whether non-directories given as file operands
        are listed.
    -H  If a symbolic link is specified on the command line, du shall count the
        size of the file or file hierarchy referenced by the link.
    -k  Write the files sizes in units of 1024 bytes, rather than the default
        512-byte units.
    -L  If a symbolic link is specified on the command line or encountered during
        the traversal of a file hierarchy, du shall count the size of the file or
        file hierarchy referenced by the link.
    -s  Instead of the default output, report only the total sum for each of the
        specified files.
    -x  When evaluating file sizes, evaluate only those files that have the same
        device as the file specified by the file operand.

  EXIT STATUS
    0   Successful completion.
    >0  An error occurred.

