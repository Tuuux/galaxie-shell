====
exit
====

.. code-block:: text

  NAME
    exit

  SYNOPSIS
    exit [code...]

  DESCRIPTION
    exit shell with a given exit code

  OPERANDS
    code  Exit code

