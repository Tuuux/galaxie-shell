==
cd
==

.. code-block:: text

  NAME
    cd - change the working directory

  SYNOPSIS
    cd [-P] [-L] [directory]

  DESCRIPTION
    The cd utility shall change the working directory of the current shell
    execution environment

  OPERANDS
    directory  An absolute or relative pathname of the directory that shall
               become the new working directory. The interpretation of a relative
               pathname by cd depends on the -L option and the CDPATH and PWD
               environment variables. If directory is an empty string, the
               directory be come HOME environment variable.

  OPTIONS
    -P  Handle the operand dot-dot physically; symbolic link components shall be
        resolved before dot-dot components are processed
    -L  Handle the operand dot-dot logically; symbolic link components shall not
        be resolved before dot-dot components are processed

