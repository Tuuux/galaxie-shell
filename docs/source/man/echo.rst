====
echo
====

.. code-block:: text

  NAME
    echo - write arguments to standard output

  SYNOPSIS
    echo [-n] [string...]

  DESCRIPTION
    The echo utility writes its arguments to standard output, followed by a
    <newline>. If there are no arguments, only the <newline> is written.

  OPERANDS
    string  A string to be written to standard outputself.

  OPTIONS
    -n  Suppress the <newline> that would otherwise follow the final argument in
        the output.

