====
head
====

.. code-block:: text

  NAME
    head - copy the first part of

  SYNOPSIS
    head [-n] [file...]

  DESCRIPTION
    The head utility shall copy its input files to the standard output, ending the
    output for each file at a designated point.

  OPERANDS
    file  A pathname of an input file. If no file operands are specified, the
          standard input shall be used.

  OPTIONS
    -n  The first number lines of each input file shall be copied to standard
        output.

