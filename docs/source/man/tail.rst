====
tail
====

.. code-block:: text

  NAME
    tail - copy the last part of a file

  SYNOPSIS
    tail [-f] [-c number|-n number] [file]

  DESCRIPTION
    The tail utility shall copy its input file to the standard output beginning at
    a designated place.

    Copying shall begin at the point in the file indicated by
    the -c number or -n number options. The option-argument number shall be counted
    in units of lines or bytes, according to the options -n and -c. Both line and
    byte counts start from 1.

  OPERANDS
    file  A pathname of an input file. If no file operand is specified, the
          standard input shall be used.

  OPTIONS
    -f  If the input file is a regular file or if the file operand specifies a
        FIFO, do not terminate after the last line of the input file has been
        copied, but read and copy further bytes from the input file when they
        become available. If no file operand is specified and standard input is a
        pipe or FIFO, the -f option shall be ignored. If the input file is not a
        FIFO, pipe, or regular file, it is unspecified whether or not the -f
        option shall be ignored.
    -c  The origin for counting shall be 1; that is, -c +1 represents the first
        byte of the file, -c -1 the last.
    -n  This option shall be equivalent to -c number, except the starting
        location in the file shall be measured in lines instead of bytes. The
        origin for counting shall be 1; that is, -n +1 represents the first line
        of the file, -n -1 the last.

