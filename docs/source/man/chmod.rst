=====
chmod
=====

.. code-block:: text

  NAME
    chmod - change the file modes

  SYNOPSIS
    chmod [-R] mode file...

  DESCRIPTION
    The chmod utility shall change any or all of the file mode bits of the file
    named by each file operand in the way specified by the mode operandself.

    Only
    a process whose effective user ID matches the user ID of the file, or a process
    with appropriate privileges, shall be permitted to change the file mode bits of
    a file.

  OPERANDS
    mode  Represents the change to be made to the file mode bits of each file
          named by one of the file operands
    file  A pathname of a file whose file mode bits shall be modified.

  OPTIONS
    -R  Recursively change file mode bits.

