===
env
===

.. code-block:: text

  NAME
    env - set the environment for command invocation

  SYNOPSIS
    env [-i] [name] [utility] [argument]

  DESCRIPTION
    The env utility shall obtain the current environment, modify it according to
    its arguments, then invoke the utility named by the utility operand with the
    modified environment.

  OPERANDS
    name      Arguments of the form name= value shall modify the execution
              environment, and shall be placed into the inherited environment
              before the utility is invoked.
    utility   The name of the utility to be invoked. If the utility operand names
              any of the special built-in utilities in Special Built-In
              Utilities, the results are undefined.
    argument  A string to pass as an argument for the invoked utility.

  OPTIONS
    -i  Invoke utility with exactly the environment specified by the arguments;
        the inherited environment shall be ignored completely.

