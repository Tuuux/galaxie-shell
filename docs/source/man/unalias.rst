=======
unalias
=======

.. code-block:: text

  NAME
    unalias - remove alias definitions

  SYNOPSIS
    unalias alias-name...

  DESCRIPTION
    The unalias utility shall remove the definition for each alias name specified.

  OPERANDS
    alias-name  The name of an alias to be removed.

  OPTIONS
    -a  Remove all alias definitions from the current shell execution
        environment.

  EXIT STATUS
    0   Successful completion.
    >0  One of the alias-name operands specified did not represent a valid alias
        definition, or an error occurred.

