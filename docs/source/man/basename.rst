========
basename
========

.. code-block:: text

  NAME
    basename - return non-directory portion of a pathname

  SYNOPSIS
    basename [string] [suffix]

  DESCRIPTION
    Print string with any leading directory components removed. If specified, also
    remove a trailing suffix.

  OPERANDS
    string  A string
    suffix  A string

