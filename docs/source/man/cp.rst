==
cp
==

.. code-block:: text

  NAME
    cp - copy files

  SYNOPSIS
    cp [-f] [-i] source_file... target_file

  DESCRIPTION
    The first synopsis form is denoted by two operands, neither of which are
    existing files of type directory. The cp utility shall copy the contents of
    source_file (or, if source_file is a file of type symbolic link, the contents
    of the file referenced by source_file) to the destination path named by
    target_file.

  OPERANDS
    source_file  A pathname of a file to be copied. If a source_file operand is
                 '-', it shall refer to a file named -; implementations shall not
                 treat it as meaning standard input. target_file
    target_file  A pathname of an existing or nonexistent file, used for the
                 output when a single file is copied. If a target_file operand is
                 '-', it shall refer to a file named -; implementations shall not
                 treat it as meaning standard output.

  OPTIONS
    -f  If a file descriptor for a destination file cannot be obtained, as
        described in step 3.a.ii., attempt to unlink the destination file and
        proceed.
    -i  Write a prompt to standard error before copying to any existing non-
        directory destination file.

  EXIT STATUS
    0   The utility executed successfully and all requested changes were made.
    >0  An error occurred.

