===
tty
===

.. code-block:: text

  NAME
    tty - return user's terminal name

  SYNOPSIS
    tty

  DESCRIPTION
    The tty utility shall write to the standard output the name of the terminal
    that is open as standard input.

