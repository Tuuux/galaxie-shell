=====
false
=====

.. code-block:: text

  NAME
    false - return false value

  SYNOPSIS
    false

  DESCRIPTION
    The false utility shall return with a non-zero exit code.

  EXIT STATUS
    1

