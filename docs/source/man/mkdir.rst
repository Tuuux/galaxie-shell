=====
mkdir
=====

.. code-block:: text

  NAME
    mkdir - make directories

  SYNOPSIS
    mkdir [-p] [-m] dir...

  DESCRIPTION
    The mkdir utility shall create the directories specified by the operands

  OPERANDS
    dir  A pathname of a directory to be created.

  OPTIONS
    -p  Create any missing intermediate pathname components.
    -m  Set the file permission bits of the newly-created directory to the
        specified mode value.

