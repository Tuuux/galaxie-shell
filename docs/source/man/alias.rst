=====
alias
=====

.. code-block:: text

  NAME
    alias - define or display aliases

  SYNOPSIS
    alias [alias-name[=string]...]

  DESCRIPTION
    The alias utility shall create or redefine alias definitions or write the
    values of existing alias definitions to standard output. An alias definition
    provides a string value that shall replace a command name when it is
    encountered

  OPERANDS
    alias-name         Write the alias definition to standard output.
    alias-name=string  Assign the value of string to the alias alias-name.

  EXIT STATUS
    0   Successful completion.
    >0  One of the name operands specified did not have an alias definition, or an
        error occurred.

