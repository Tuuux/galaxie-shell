=====
umask
=====

.. code-block:: text

  NAME
    umask - get or set the file mode creation mask

  SYNOPSIS
    umask [-S] [mask]

  DESCRIPTION
    The umask utility shall set the file mode creation mask of the current shell
    execution environment (see Shell Execution Environment) to the value specified
    by the mask operand. This mask shall affect the initial value of the file
    permission bits of subsequently created files. If umask is called in a subshell
    or separate utility execution environment, such as one of the following:
    (umask 002)
    nohup umask ...
    find . -exec umask ... \;

    it shall not affect the
    file mode creation mask of the caller's environment.

    If the mask operand is
    not specified, the umask utility shall write to standard output the value of
    the file mode creation mask of the invoking process.

  OPERANDS
    mask  A string specifying the new file mode creation mask. The string is
          treated in the same way as the mode operand described in the EXTENDED
          DESCRIPTION section for chmod. For a symbolic_mode value, the new value
          of the file mode creation mask shall be the logical complement of the
          file permission bits portion of the file mode specified by the
          symbolic_mode string.  In a symbolic_mode value, the permissions op
          characters '+' and '-' shall be interpreted relative to the current
          file mode creation mask; '+' shall cause the bits for the indicated
          permissions to be cleared in the mask; '-' shall cause the bits for the
          indicated permissions to be set in the mask.  The interpretation of
          mode values that specify file mode bits other than the file permission
          bits is unspecified.  In the octal integer form of mode, the specified
          bits are set in the file mode creation mask.  The file mode creation
          mask shall be set to the resulting numeric value.  The default output
          of a prior invocation of umask on the same system with no operand also
          shall be recognized as a mask operand.

  OPTIONS
    -S  Produce symbolic output.

