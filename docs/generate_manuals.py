
import glxshell.lib.os as os
import glxshell.lib.textwrap as textwrap

from glxshell.lib.ush import GLXUsh
from io import StringIO
from unittest.mock import patch

directories = {
    "docs": os.path.dirname(
        __file__
    ),
    "source": os.path.join(
        os.path.dirname(__file__),
        "source"
    ),
    "man": os.path.join(
        os.path.dirname(__file__),
        "source",
        "man",
    ),
    "utilities": os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            "glxshell",
            "utilities",
        )
    ),
}


def get_utilities_list():
    the_value = []
    for item in os.listdir(directories["utilities"]):
        if os.path.isfile(os.path.join(directories["utilities"], item)):
            if item.lower().endswith(".py") and not item.lower().startswith("__"):
                the_value.append(os.path.basename(item, ".py"))

    return the_value


def get_man_list():
    the_value = []
    for item in os.listdir(directories["man"]):
        if os.path.isfile(os.path.join(directories["man"], item)):
            if item.lower().endswith(".rst"):
                the_value.append(os.path.basename(item, ".rst"))

    return the_value


def main():
    shell = GLXUsh()

    for key, value in directories.items():
        print("%s %s" % (key, value))
    for utility in get_utilities_list():
        utility = utility.replace(".py", "")
        with patch("sys.stdout", new=StringIO()) as fake_out:
            shell.environ["COLUMNS"] = 79
            shell.onecmd("man %s" % utility)
            the_man = fake_out.getvalue()

        with open(os.path.join(directories["man"], "%s.rst" % utility), "w") as manfile:
            sub = "=" * len(utility)
            manfile.write("%s\n" % sub)
            manfile.write("%s\n" % utility)
            manfile.write("%s\n" % sub)
            manfile.write("\n")
            manfile.write(".. code-block:: text\n")
            manfile.write("\n")
            manfile.write(textwrap.indent(the_man, "  "))
            manfile.write("\n")

    manuals_page_content = """Manuals Pages
=============

.. toctree::
   :maxdepth: 1

"""
    for man in get_man_list():
        manuals_page_content += "   man/%s\n" % man
    with open(os.path.join(directories["source"], "manuals.rst"), "w") as manuals_page:
        manuals_page.write(manuals_page_content)


if __name__ == "__main__":
    main()
