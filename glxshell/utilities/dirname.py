# inspired by: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/dirname.html

# Standard python module
import sys

# Internal python module
import glxshell.lib.argparse as argparse
import glxshell.lib.os as os

parser_dirname = argparse.ArgumentParser(
    name="dirname - return the directory portion of a pathname",
    description="The string operand shall be treated as a pathname, as defined in XBD Pathname. The string string "
                "shall be converted to the name of the directory containing the filename corresponding to the last "
                "pathname component in string.",
)
parser_dirname.add_argument(
    "string",
    nargs="?",
    const=0,
    help="A string",
)


def glxsh_dirname(string=None):
    try:
        sys.stdout.write("%s\n" % os.path.dirname(string))
        return 0
    except (Exception, ArithmeticError) as error:  # pragma: no cover
        sys.stderr.write("dirname: %s\n" % error)
        return 1
