# Internal python module
import glxshell.lib.argparse as argparse


parser_false = argparse.ArgumentParser(
    name="false - return false value",
    synopsis=["false"],
    description="The false utility shall return with a non-zero exit code.",
    exit_status={
        "1": "",
    }
)


def glxsh_false():
    return 1
