
APPLICATION_AUTHORS = ["Tuuuux"]
APPLICATION_DESCRIPTION = ""
APPLICATION_LICENSE = "License WTFPL v2"
APPLICATION_NAME = "glxsh"
APPLICATION_VERSION = "0.2.6"
APPLICATION_PATCH_LEVEL = "a1"
APPLICATION_WARRANTY = """Copyright (C) 2020-2022 Galaxie Shell Project.
License WTFPL Version 2, December 2004 <http://www.wtfpl.net/about/>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
"""
