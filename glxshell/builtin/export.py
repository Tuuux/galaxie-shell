# Standard python module
import sys

# Internal python module
import glxshell.lib.argparse as argparse

parser_export = argparse.ArgumentParser(
    name="export",
    description="set the export attribute for variables",
    synopsis=[
        "export name[=word]...",
        "export -p",
    ],
    exit_status={
        "0": "All name operands were successfully exported.",
        ">0": "At least one name could not be exported, or the -p option was specified and an error occurred.",
    },
)
parser_export.add_argument(
    "-p",
    action="store_true",
    default=False,
    help="Write to the standard output the names and values of all exported variables",
)


def glxsh_export(**kwargs):
    shell = kwargs.get("shell", None)
    line = kwargs.get("line", None)

    if code is None or not code:
        code = 0
    else:
        code = code[0]
    if shell:
        shell.exit_code = code
        sys.exit(code)
        # shell.do_EOF()
    else:
        return code

