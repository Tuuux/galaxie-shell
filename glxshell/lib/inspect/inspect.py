import sys


def getmembers(obj, pred=None):
    res = []
    for name in dir(obj):
        val = getattr(obj, name)
        if pred is None or pred(val):
            res.append((name, val))
    res.sort()
    return res


def isfunction(obj):
    return isinstance(obj, type(isfunction))


def isgeneratorfunction(obj):
    return isinstance(obj, type(lambda: (yield)))


def isgenerator(obj):
    return isinstance(obj, type(lambda: (yield)()))


class _Class:
    def meth():
        pass


_Instance = _Class()


def ismethod(obj):
    return isinstance(obj, type(_Instance.meth))


def isclass(obj):
    return isinstance(obj, type)


def ismodule(obj):
    return isinstance(obj, type(sys))


def getargspec(func):
    raise NotImplementedError("This is over-dynamic function, not supported by MicroPython")


def getmodule(obj, _filename=None):
    return None  # Not known


def getmro(cls):
    return [cls]


def getsourcefile(obj):
    return None  # Not known

def getsourcelines(object):
    """Return a list of source lines and starting line number for an object.

    The argument may be a module, class, method, function, traceback, frame,
    or code object.  The source code is returned as a list of the lines
    corresponding to the object and the line number indicates where in the
    original source file the first line of code was found.  An OSError is
    raised if the source code cannot be retrieved."""
    object = unwrap(object)
    lines, lnum = findsource(object)

    if istraceback(object):
        object = object.tb_frame

    # for module or frame that corresponds to module, return all source lines
    if (ismodule(object) or
        (isframe(object) and object.f_code.co_name == "<module>")):
        return lines, 0
    else:
        return getblock(lines[lnum:]), lnum + 1

def getfile(obj):
    return "<unknown>"


def getsource(obj):
    return "<source redacted to save you memory>"


def currentframe():
    return None


def getframeinfo(frame, context=1):
    return ("<unknown>", -1, "<unknown>", [""], 0)