# Replace built-in os module.
try:
    from uos import *
except ImportError:
    from os import *

# Provide optional dependencies (which may be installed separately).
try:
    from . import path
except ImportError:
    pass