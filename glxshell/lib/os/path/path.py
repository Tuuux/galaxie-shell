"""
Common operations on Posix pathnames.
"""
# Strings representing various path-related bits and pieces.
# These are primarily for export; internally, they are hardcoded.
# Should be set before imports for resolving cyclic dependency.
altsep = None
curdir = '.'
defpath = '/bin:/usr/bin'
devnull = '/dev/null'
extsep = '.'
pardir = '..'
pathsep = ':'
sep = '/'

import os
from glxshell.lib import stat

def _joinrealpath(path, rest, strict, seen):
    curdir = "."
    pardir = ".."

    if isabs(rest):
        rest = rest[1:]
        path = sep

    while rest:
        name, _, rest = rest.partition(sep)
        if not name or name == curdir:
            # current dir
            continue
        if name == pardir:
            # parent dir
            if path:
                path, name = split(path)
                if name == pardir:
                    path = join(path, pardir, pardir)
            else:
                path = pardir
            continue
        newpath = join(path, name)
        try:
            st = os.lstat(newpath)
        except OSError:
            if strict:
                raise
            is_link = False
        else:
            is_link = stat.S_ISLNK(st.st_mode)
        if not is_link:
            path = newpath
            continue
        # Resolve the symbolic link
        if newpath in seen:
            # Already seen this path
            path = seen[newpath]
            if path is not None:
                # use cached value
                continue
            # The symlink is not resolved, so we must have a symlink loop.
            if strict:
                # Raise OSError(errno.ELOOP)
                os.stat(newpath)
            else:
                # Return already resolved part + rest of the path unchanged.
                return join(newpath, rest), False
        seen[newpath] = None  # not resolved symlink
        path, ok = _joinrealpath(path, os.readlink(newpath), strict, seen)
        if not ok:
            return join(path, rest), False
        seen[newpath] = path  # resolved symlink

    return path, True


def abspath(path):
    """
    Return a normalized absolutized version of the pathname path. On most platforms, this is equivalent to
    calling the function :func:`normpath` as follows: ``normpath(join(os.getcwd(), path))``.

    :param path: A pathname.
    :type path: str
    :return: Normalized ``path`` or ``.``.
    :rtype: str
    """
    if not isabs(path):
        return normpath(join(os.getcwd(), path))
    return normpath(path)


def basename(path=None, suffix=None):
    """
    Return the base name of pathname path. This is the second element of the pair returned by passing path to
    the function :func:`split`.

    .. note::
        The result of this function is different from the Unix ``basename`` program;
        where basename for '/foo/bar/' returns 'bar', the :func:`basename` function returns an empty string ('').

    :param path: A pathname.
    :type path: str
    :param suffix: A suffix.
    :type suffix: str
    :return: The base name of pathname ``path``.
    :rtype: str
    """
    if not path:
        return "."

    if path in (sep, sep * 2, sep * 3):
        return sep

    if sep not in path:
        return path

    if suffix:
        return normpath(path).split(sep)[-1].replace(suffix, "")

    return normpath(path).split(sep)[-1]


def commonprefix(m):
    """
    Return the longest path prefix (taken character-by-character) that is a prefix of all paths in list.
    If list is empty, return the empty string ('').

    .. note::
        This function may return invalid paths because it works a character at a time. To obtain a valid path, see :func:`commonpath`.

        >>> os.path.commonprefix(['/usr/lib', '/usr/local/lib'])
        '/usr/l'
        >>> os.path.commonpath(['/usr/lib', '/usr/local/lib'])
        '/usr'

    :param m: Paths
    :type m: list
    :return: The longest common sub-path
    :rtype: str
    """
    if m is None:
        return ""
    s1 = min(m)
    s2 = max(m)
    for i, c in enumerate(s1):
        if c != s2[i]:
            return s1[:i]
    return s1


# From CPython git tag v3.4.10.
def dirname(path):
    """
    Return the directory name of pathname path.
    This is the first element of the pair returned by passing path to the function :func:`split`.

    :param path: A path.
    :type path: str
    :return: The directory name of pathname path
    :rtype: str
    """
    if not path or sep not in path:
        return "."

    if path in (sep, sep * 2, sep * 3):
        return sep

    r = normpath(path).rsplit(sep, 1)
    head = ""
    if len(r) > 1:
        if r[0]:
            head = r[0]
        else:
            head = sep

    if sep not in head:
        return "."

    return head


def exists(path):
    """
    Return ``True`` if path refers to an existing path or an open file descriptor.
    Returns ``False`` for broken symbolic links. On some platforms,
    this function may return ``False`` if permission is not granted to execute ``os.stat()`` on the
    requested file, even if the path physically exists.

    :param path: A path.
    :type path: str
    :return: ``True`` if path refers to an existing path or an open file descriptor.
    :rtype: bool
    """
    try:
        os.stat(path)
    except (OSError, ValueError):
        return False
    return True


def expanduser(path):
    """
    Return the argument with an initial component of ``~`` replaced by that user’s home directory.

    An initial ``~`` is replaced by the environment variable HOME if it is set; otherwise
    the current user’s home directory is looked up in the password directory through the built-in
    module pwd.

    If the expansion fails or if the path does not begin with a tilde, the path is returned unchanged.

    :param path: A path it eventually content a ``~``.
    :type path: str
    :return: If possible the path with `~`` replaced by HOME or the unchanged path
    :rtype: str
    """
    if path == "~" or path.startswith("~" + sep):
        h = os.getenv("HOME")
        return h + path[1:]
    if path[0] == "~":
        # Sorry folks, follow conventions
        return sep + "home" + sep + path[1:]

    return path


# Return the longest prefix of all list elements.
def isabs(path):
    """Test whether a path is absolute"""
    return path.startswith(sep)


# Return a canonical path (i.e. the absolute location of a file on the
# filesystem).


def isdir(path):
    try:
        return stat.S_ISDIR(os.stat(path)[0])
    except (OSError, ArithmeticError):
        return False


def isfile(path):
    try:
        return stat.S_ISREG(os.stat(path)[0])
    except (OSError, ArithmeticError):
        return False


def islink(path):
    try:
        return stat.S_ISLNK(os.lstat(path)[0])
    except (OSError, ArithmeticError):
        return False


def ismount(path):
    """
    Return ``True`` if pathname path is a mount point: a point in a file system where a different file system has been
    mounted.

    The function checks whether path’s parent, path/.., is on a different device than path, or whether path/.. and
    path point to the same i-node on the same device — this should detect mount points for all Unix and POSIX variants.

    It is not able to reliably detect bind mounts on the same filesystem.

    Parameters:
        path (str): A path.

    Returns:
        bool: ``True`` if pathname ``path`` is a mount point
    """
    try:
        s1 = os.lstat(path)
    except (OSError, ValueError):
        # It doesn't exist -- so not a mount point. :-)
        return False
    else:
        # A symlink can never be a mount point
        if stat.S_ISLNK(s1.st_mode):
            return False

    parent = realpath(join(os.fspath(path), pardir))
    try:
        s2 = os.lstat(parent)
    except (OSError, ValueError):
        return False

    dev1 = s1.st_dev
    dev2 = s2.st_dev
    if dev1 != dev2:
        return True  # path/.. on a different device as path
    ino1 = s1.st_ino
    ino2 = s2.st_ino
    if ino1 == ino2:
        return True  # path/.. is the same i-node as path
    return False


def join(*args):
    """
    Join one or more path segments intelligently. The return value is the concatenation of path and all members
    of paths, with exactly one directory separator following each non-empty part, except the last.
    That is, the result will only end in a separator if the last part is either empty or ends in a separator.
    If a segment is an absolute path, then all previous segments are ignored and joining continues from the
    absolute path segment.

    Parameters:
        args (any): A List of str

    Returns:
        str: A pathname composed by components joins by /
    """
    res = ""
    for a in args:
        if a.startswith(sep):
            res = a
        elif not res or res.endswith(sep):
            res += a
        else:
            res += sep + a
    return res.replace(sep * 2, sep)


def lexists(path):
    """
    Return ``True`` if path refers to an existing path.
    Returns ``False`` for broken symbolic links.

    Equivalent to :func:`exists` on platforms lacking ``os.lstat()``.
    """
    try:
        return os.access(path, os.F_OK) and os.readlink(path)
    except (OSError, ValueError):
        return False


def normcase(path):
    """
    Normalize the case of pathname.  Has no effect under Posix

    Parameters:
        path (str): A path.

    Returns:
        str: Normalized ``path`` or ``.``
    """
    return os.fspath(path)


def normpath(path):
    """
    Normalize a pathname by collapsing redundant separators and up-level references so that A//B,
    A/B/, A/./B and A/foo/../B all become A/B. This string manipulation may change the meaning
    of a path that contains symbolic links.

    Note:
        On POSIX systems, in accordance with IEEE Std 1003.1 2013 Edition; 4.13 Pathname Resolution, if a pathname
        begins with exactly two slashes, the first component following the leading characters may be interpreted in
        an implementation-defined manner, although more than two leading characters shall be treated as a single
        character.

    Parameters:
        path (str): A path.

    Returns:
        str: Normalized ``path`` or ``.``
    """
    if path == "":
        return "."
    initial_slashes = path.startswith(sep)
    # POSIX allows one or two initial slashes, but treats three or more
    # as single slash.
    # (see http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap04.html#tag_04_13)
    if initial_slashes and path.startswith(sep * 2) and not path.startswith(sep * 3):
        initial_slashes = 2
    new_comps = []
    for comp in path.split(sep):
        if comp in {"", "."}:
            continue
        if comp != ".." or (not initial_slashes and not new_comps) or (new_comps and new_comps[-1] == ".."):
            new_comps.append(comp)
        elif new_comps:
            new_comps.pop()
    path = sep.join(new_comps)
    if initial_slashes:
        path = sep * initial_slashes + path
    return path or "."


def realpath(filename, *, strict=False):
    """Return the canonical path of the specified filename, eliminating any
    symbolic links encountered in the path."""
    filename = os.fspath(filename)
    path, _ = _joinrealpath(filename[:0], filename, strict, {})
    return abspath(path)


def relpath(path, start=None):
    """Return a relative version of a path"""

    if not path:
        raise ValueError("no path specified")

    curdir = "."
    pardir = ".."

    if start is None:
        start = curdir

    start_list = [x for x in abspath(start).split(sep) if x]
    path_list = [x for x in abspath(path).split(sep) if x]

    # Work out how much of the filepath is shared by start and path.
    i = len(commonprefix([start_list, path_list]))

    rel_list = [pardir] * (len(start_list) - i) + path_list[i:]
    if not rel_list:
        return curdir
    return join(*rel_list)


def split(path):
    """
    Split the pathname path into a pair, (``head``, ``tail``) where ``tail`` is the last pathname component and head is
    everything leading up to that. The tail part will never contain a slash; if path ends in a slash, tail will
    be empty. If there is no slash in path, head will be empty. If path is empty, both head and tail are empty.
    Trailing slashes are stripped from head unless it is the root (one or more slashes only). I
    n all cases, ``join(head, tail)`` returns a path to the same location as path (but the strings may differ).
    Also see the functions ``dirname()`` and ``basename()``.

    :param path: A path.
    :type path: str
    :return: (``head``, ``tail``)
    :rtype: tuple
    """
    if path == "":
        return "", ""
    r = path.rsplit(sep, 1)
    if len(r) == 1:
        return "", path
    head = r[0]  # .rstrip(sep)
    if not head:
        head = sep
    return head, r[1]


def splitdrive(path):
    return "", path


# https://pubs.opengroup.org/onlinepubs/9699919799/utilities/dirname.html
def splitext(path):
    """
    Split the pathname path into a pair (root, ext) such that root + ext == path, and the extension, ext,
    is empty or begins with a period and contains at most one period.

    If the path contains no extension, ext will be '':

    >>> splitext('bar')
    ('bar', '')

    If the path contains an extension, then ext will be set to this extension, including the leading period.
    Note that previous periods will be ignored:

    >>> splitext('foo.bar.exe')
    ('foo.bar', '.exe')
    >>> splitext('foo/bar.exe')
    ('foo/bar', '.exe')

    Leading periods of the last component of the path are considered to be part of the root:

    >>> splitext('.cshrc')
    ('.cshrc', '.exe')
    >>> splitext('/foo/....jpg')
    ('/foo/....jpg', '')

    :param path: A path.
    :type path: str
    :return: (``head``, ``tail``)
    :rtype: tuple
    """
    sepIndex = path.rfind(sep)

    dotIndex = path.rfind(extsep)
    if dotIndex > sepIndex:
        # skip all leading dots
        filenameIndex = sepIndex + 1
        while filenameIndex < dotIndex:
            if path[filenameIndex:filenameIndex + 1] != extsep:
                return path[:dotIndex], path[dotIndex:]
            filenameIndex += 1

    return path, path[:0]



