# Galaxie lib os.path

That module is a mixe of Micropython and Cython os.path POSIX Path lib.
Certain function have been revisited for be more POSIX and remove strung bytes support.

Libs are tested under UNIX Micropyhton and python3

Original Libraries can be found here:
https://github.com/python/cpython/blob/main/Lib/posixpath.py
https://github.com/micropython/micropython-lib/blob/master/python-stdlib/os-path/os/path.py

the goal i to provide to Galaxie Shell a internal lib for manage path.

The python module have been isolated to glxshell/lib for permit unitest with python and micropython.
