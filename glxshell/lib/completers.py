import re

import glxshell.lib.glob as glob
import glxshell.lib.os as os


def _append_slash_if_dir(p):
    if p and os.path.isdir(p) and p[-1] != os.path.sep:
        return p + os.path.sep
    else:
        return p


def glxsh_completer_directory(_, line, __, ___):
    arg = line.split()[1:]

    if not arg:
        return [os.path.normpath(f) + os.path.sep for f in os.listdir(os.getcwd()) if os.path.isdir(f)]
    else:
        directory, part, base = arg[-1].rpartition(os.path.sep)
        if part == "":
            directory = os.getcwd()
        elif directory == "":
            directory = os.path.sep
        completions = []
        for f in os.listdir(directory):
            if f.startswith(base):
                if os.path.isdir(os.path.join(directory, f)):
                    completions.append(os.path.join(f, ""))
        return completions


def glxsh_completer_file(_, line, __, ___):
    arg = line.split()[1:]

    if not arg:
        return os.listdir(os.getcwd())
    else:
        directory, part, base = arg[-1].rpartition(os.path.sep)
        if part == "":
            directory = os.getcwd()
        elif directory == "":
            directory = os.path.sep
        return [os.path.normpath(f) + os.path.sep if os.path.isdir(f) else os.path.normpath(f) for f in
                os.listdir(directory) if f.startswith(base)]


def glxsh_complete_chmod(text, line, begidx, endidx):
    return glxsh_completer_file(text, line, begidx, endidx)


def glxsh_complete_rmdir2(_, line, begidx, endidx):
    before_arg = line.rfind(" ", 0, begidx)
    if before_arg == -1:
        return  # arg not found

    fixed = line[before_arg + 1:begidx]  # fixed portion of the arg
    arg = line[before_arg + 1:endidx]
    pattern = arg + '*'

    completions = []
    for path in glob.glob(pattern):
        path = _append_slash_if_dir(path)
        completions.append(path.replace(fixed, "", 1))
    return completions


def glxsh_complete_rmdir(_, line, __, ___):
    if line == "rmdir":
        return ["rmdir "]

    arg = line.split()[1:]

    if not arg:
        return [os.path.normpath(f) + os.path.sep for f in os.listdir(os.getcwd()) if os.path.isdir(f)]
    else:
        directory, part, base = arg[-1].rpartition(os.path.sep)
        if part == '':
            directory = '.' + os.path.sep
        elif directory == '':
            directory = os.path.sep

        completions = []
        for f in os.listdir(directory):
            if f.startswith(base):
                if os.path.isdir(os.path.join(directory, f)):
                    completions.append(f + os.path.sep)

    return completions


var_env_pattern = re.compile(r".*\$$", re.IGNORECASE)
var_env_name_pattern = re.compile(r".*\$(\w+)$", re.IGNORECASE)


def glxsh_complete_echo(_, line, __, ___, shell=None):
    if line == "echo":
        return ["echo "]

    arg = line.split()[1:]

    if not arg:
        pass
    else:
        completions = []
        if var_env_name_pattern.match(arg[-1]):
            search_result = re.search(var_env_name_pattern, arg[-1])
            if not search_result:
                return None

            for key, value in shell.environ.items():
                if str(key).startswith(search_result.group(1)) and key != search_result.group(1):
                    completions.append("%s " % key)
            return completions

        elif var_env_pattern.match(arg[-1]):
            search_result = re.search(var_env_pattern, arg[-1])
            if not search_result:
                return None

            for key, value in shell.environ.items():
                completions.append("%s " % key)
            return completions

        else:
            return None


def glxsh_complete_du(text, line, begidx, endidx, shell=None):
    if line == "du":
        return ["du "]

    arg = line.split()[1:]
    completions = []
    if not arg:
        for f in os.listdir(os.getcwd()):
            if os.path.isdir(f):
                completions.append(os.path.normpath(f) + os.path.sep)
            if os.path.isfile(f) or os.path.islink(f):
                completions.append(os.path.normpath(f))

    else:
        directory, part, base = arg[-1].rpartition(os.path.sep)
        if part == '':
            directory = '.' + os.path.sep
        elif directory == '':
            directory = os.path.sep

        completions = []
        for f in os.listdir(directory):
            if f.startswith(base) or f.startswith(base + os.path.sep):
                if os.path.isdir(f):
                    completions.append(os.path.normpath(f) + os.path.sep)
                if os.path.isfile(f) or os.path.islink(f):
                    completions.append(os.path.normpath(f))

    return completions

def glxsh_complete_vi(_, line, __, ___):
    arg = line.split()[1:]

    if not arg:
        return os.listdir(os.getcwd())
    else:
        directory, part, base = arg[-1].rpartition(os.path.sep)
        if part == "":
            directory = os.getcwd()
        elif directory == "":
            directory = os.path.sep
        return [os.path.normpath(f) + os.path.sep if os.path.isdir(f) else os.path.normpath(f) for f in
                os.listdir(directory) if f.startswith(base)]