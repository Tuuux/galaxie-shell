# argparse

## Description
That is a true functional argparse, fork from MicroPython and adapt for Galaxie-Shell

## Feature
 * Match with POSIX Utility Description
 * Match with POSIX Utility Arg
 * Support ``-a -b -c`` and ``-abc`` arguments
