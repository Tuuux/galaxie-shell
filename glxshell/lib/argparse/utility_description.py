class DescriptionString:
    def __set_name__(self, owner, name):
        self.public_name = name
        self.private_name = '_' + name

    def __get__(self, obj, objtype=None):
        return getattr(obj, self.private_name)

    def __set__(self, obj, value):
        if value and not isinstance(value, str):
            raise TypeError("'%s' value must be a str type or None" % self.public_name)
        if self.private_name != value:
            setattr(obj, self.private_name, value)


class SynopsisList:
    def __set_name__(self, owner, name):
        self.public_name = name
        self.private_name = '_' + name

    def __get__(self, obj, objtype=None):
        return getattr(obj, self.private_name)

    def __set__(self, obj, value):
        if value and not isinstance(value, list):
            raise TypeError("'%s' value must be a list type or None" % self.public_name)
        if self.private_name != value:
            setattr(obj, self.private_name, value)


class DescriptionDict:
    def __set_name__(self, owner, name):
        self.public_name = name
        self.private_name = '_' + name

    def __get__(self, obj, objtype=None):
        return getattr(obj, self.private_name)

    def __set__(self, obj, value):
        if value and not isinstance(value, dict):
            raise TypeError("'%s' value must be a dict type or None" % self.public_name)
        if self.private_name != value:
            setattr(obj, self.private_name, value)


class DescriptionBoolean:
    def __set_name__(self, owner, name):
        self.public_name = name
        self.private_name = '_' + name

    def __get__(self, obj, objtype=None):
        return getattr(obj, self.private_name)

    def __set__(self, obj, value):
        if value and not isinstance(value, bool):
            raise TypeError("'%s' value must be a dict type or None" % self.public_name)
        if self.private_name != value:
            setattr(obj, self.private_name, value)


class UtilityDescription:
    name = DescriptionString()
    synopsis = SynopsisList()
    description = DescriptionString()
    options = DescriptionString()
    operator = DescriptionString()
    stdin = DescriptionString()
    input_files = DescriptionString()
    environment_variables = DescriptionString()
    asynchronous_events = DescriptionString()
    stdout = DescriptionString()
    stderr = DescriptionString()
    output_files = DescriptionString()
    extended_description = DescriptionString()
    exit_status = DescriptionDict()
    consequences_of_errors = DescriptionString()
    application_usage = DescriptionString()
    examples = DescriptionString()
    rationale = DescriptionString()
    future_directions = DescriptionString()
    see_also = DescriptionString()
    change_history = DescriptionString()
    add_help = DescriptionBoolean()
    prog = DescriptionString()
    short_description = DescriptionString()

    def __init__(
            self,
            name=None,
            prog=None,
            synopsis=None,
            description=None,
            short_description=None,
            options=None,
            operator=None,
            stdin=None,
            input_files=None,
            environment_variables=None,
            asynchronous_events=None,
            stdout=None,
            stderr=None,
            output_files=None,
            extended_description=None,
            exit_status=None,
            consequences_of_errors=None,
            application_usage=None,
            examples=None,
            rationale=None,
            future_directions=None,
            see_also=None,
            change_history=None,
            add_help=None,
    ):
        self.name = name
        self.prog = prog
        self.synopsis = synopsis
        self.description = description
        self.short_description = short_description
        self.options = options
        self.operands = operator
        self.stdin = stdin
        self.input_files = input_files
        self.environment_variables = environment_variables
        self.asynchronous_events = asynchronous_events
        self.stdout = stdout
        self.stderr = stderr
        self.output_files = output_files
        self.extended_description = extended_description
        self.exit_status = exit_status
        self.consequences_of_errors = consequences_of_errors
        self.application_usage = application_usage
        self.examples = examples
        self.rationale = rationale
        self.future_directions = future_directions
        self.see_also = see_also
        self.change_history = change_history
        self.add_help = add_help
