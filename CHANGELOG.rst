GALAXIE SHELL - CHANGELOGS
--------------------------
**v0.2.3 - 20211009**
  * Add internal ``df`` command
**v0.2.2 - 20210927**
  * Fixe issue with CMD2 about ipython
  * Fixe issue with CMD2 about syntax changes
  * Better Makefile
  * Remove __pycache__ from tracking
  * Better ``cd`` unittest about ``OLDPWD`` environment variable
**v0.2.1 - 20210110**
  * Add sleep command
  * Add sleep documentation
**v0.2 - 20201113**
  * Better Makefile
  * Better documentation
  * Ready to me use on other project
**v0.1.2 - 20201112**
  * Add Application Class with properties Name, Version, License, Description, Authors, Warranty
  * Shell Class include Application class and all properties
  * Intro class (and other's) use Application class information's
  * First version it permit to generate a shell by include ``galaxie-shell`` in a new project
  * Plugins Manager review
**v0.1.2a2 - 20201111**
  * Plugins Manager refactoring
  * Delete utils modules
  * Makefile add documentations capability
  * Makefile clean care about documentation
**v0.1.2a1 - 20201107**
  * Fixe issue #2 about capability to shell to disable plugins auto load
  * Better Makefile
  * Change doc theme for python docs theme
  * Better version management
**v0.1.1 - 20201107**
  * Fixe issue #1 about wrong version
**v0.1 - 20201107**
  * Documentation publish on readthedocs
  * Documentation conversion to rst
**v0.1a5 - 20201106**
  * Better tests for Builtins Plugin
  * Refactor tests directory
  * 100% Code covering
**v0.1a4 - 20201105**
  * Fixe Tests
  * Better init
  * Add documentation