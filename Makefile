.PHONY: help
help: header
	@echo  'Build:'
	@echo  '  static            - Call venv , install pyinstaller python module inside the'
	@echo  '                      virtual environment. The build determine the arch type'
	@echo  '                      and start the static build for generate the filename.'
	@echo  '                      The generated file will be store in ./dist directory and '
	@echo  '                      the file name glxsh-x86_64.bin in case of command arch'
	@echo  '                      return x86_64.'
	@echo  '  shell             - Call static and run the shell from ./dist/glxsh-`arch`.bin'
	@echo  '  monofile          - Prepare a one file python script, it includ everything'

	@echo  ''
	@echo  'CleaningUp:'
	@echo  '  clean             - Remove every created directory and restart from scratch'
	@echo  ''
	@echo  'Documentation:'
	@echo  '  documentations    - Sphinx command it call venv, install docs/requirements.txt'
	@echo  '                      requirements, run sphinx-apidoc, and generate html '
	@echo  '                      documentation.'
	@echo  '                      Documentation is store on ./docs/source and the '
	@echo  '                      documentation build on ./docs/build'
	@echo  ''
	@echo  'VirtualEnvironment:'
	@echo  '  venv-create       - Create virtual env directory with python venv module'
	@echo  '  prepare           - Call venv-create and install or update packages:'
	@echo  '                      pip, wheel and setuptools'
	@echo  '  venv              - Call venv-update and install Galaxie Shell on the virtual '
	@echo  '                      environment as developer mode with pip install -e .'
	@echo  ''

.PHONY: install-python
install-python:
	@echo 	""
	@echo 	"******************************* INSTALL PYTHON *********************************"
	@apt-get update && apt install -y python3 python3-pip python3-venv

header:
	@echo "**************************** GALAXIE SHELL MAKEFILE ****************************"
	@echo ""
	@echo "License WTFPL V2"
	@echo "LOADER `uname -v`"
	@echo "EXEC MAKE"
	@echo "********************************************************************************"

##
## —————————————— ENVIRONEMENT —————————————————————————————————————————————————————
##
.PHONY: prepare
prepare: header ## Prepare virtual environment
	@pip3 install -U pip --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL PIP3" || \
	echo "[FAILED] INSTALL PIP3"

	@pip3 install -U wheel --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL WHEEL" || \
	echo "[FAILED] INSTALL WHEEL"

	@pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL SETUPTOOLS" || \
	echo "[FAILED] INSTALL SETUPTOOLS"

	@pip3 install -U coverage --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL COVERAGE" || \
	echo "FAILED] INSTALL COVERAGE"

	@ pip3 install -U astunparse --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL astunparse" || \
	echo "[FAILED] INSTALL astunparse"

	@ pip3 install -U ansible --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL ansible" || \
	echo "[FAILED] INSTALL ansible"
	
	@ pip3 install -U isort --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL isort" || \
	echo "[FAILED] INSTALL isort"

	@ pip3 install -U black --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL black" || \
	echo "[FAILED] INSTALL black"

	@pip3 install -U -e . --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL GLXSHELL AS DEV MODE" || \
	echo "[FAILED] INSTALL GLXSHELL AS DEV MODE"

	@ echo "*************** ANSIBLE-GALAXY REQUIREMENTS INSTALLATION ***********************"
	@ansible-galaxy install -fr ${PWD}/requirements.yml

monofile:
	@cd ansible && ansible-playbook build_monofile.yml

# https://github.com/micropython/micropython/
build-micropython:
	@cd ansible && ansible-playbook build_micropython.yml

.PHONY: tests
tests:
	@echo '********************************** RUN TESTS ***********************************'
#	@python -m unittest -b tests/lib/test_*.py tests/utilities/test_*.py tests/lib/*/test_*.py
	@coverage run -m unittest discover
	@coverage report -m

.PHONY: docs
docs:
	@echo '***************************** BUILD DOCUMENTATIONS *****************************'
	@pip3 install -r docs/requirements.txt --no-cache-dir --quiet &&\
    cd $(PWD)/docs && make html

clean:
	@echo '********************************* CLEANING UP **********************************'
	rm -rf ./.eggs
	rm -rf ./build
	rm -rf ./dist
	rm -rf ./*.egg-info
	rm -rf ./*.spec
	rm -rf ./venv
	rm -rf ./.direnv
	rm -rf ./pyenv
	rm -rf ./env
	rm -rf ./dep
	rm -rf ./docs/source/glxshell.libs.*
	rm -rf ./docs/source/glxshell.plugins.*
	rm -rf ./docs/source/glxshell.rst
	rm -rf ./docs/source/glxshell.glxsh.rst
	rm -rf ./docs/source/modules.rst
	rm -rf ./docs/build
