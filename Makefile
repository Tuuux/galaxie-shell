.PHONY: help install-python venv-create venv-requirements prepare docs tests clean static shell
include .env
export $(shell sed 's/=.*//' .env)

help: header
	@echo  'Build:'
	@echo  '  static            - Call venv , install pyinstaller python module inside the'
	@echo  '                      virtual environment. The build determine the arch type'
	@echo  '                      and start the static build for generate the filename.'
	@echo  '                      The generated file will be store in ./dist directory and '
	@echo  '                      the file name glxsh-x86_64.bin in case of command arch'
	@echo  '                      return x86_64.'
	@echo  '  shell             - Call static and run the shell from ./dist/glxsh-`arch`.bin'
	@echo  ''
	@echo  'CleaningUp:'
	@echo  '  clean             - Remove every created directory and restart from scratch'
	@echo  ''
	@echo  'Documentation:'
	@echo  '  documentations    - Sphinx command it call venv, install docs/requirements.txt'
	@echo  '                      requirements, run sphinx-apidoc, and generate html '
	@echo  '                      documentation.'
	@echo  '                      Documentation is store on ./docs/source and the '
	@echo  '                      documentation build on ./docs/build'
	@echo  ''
	@echo  'VirtualEnvironment:'
	@echo  '  venv-create       - Create virtual env directory with python venv module'
	@echo  '  venv-requirements - Call venv-create and install or update packages:'
	@echo  '                      pip, wheel and setuptools'
	@echo  '  venv              - Call venv-update and install Galaxie Shell on the virtual '
	@echo  '                      environment as developer mode with pip install -e .'
	@echo  ''

header:
	@echo  '******************************** GLXSH MAKEFILE ********************************'
	@echo  ''
	@echo  ''
	@echo  'GNU GENERAL PUBLIC LICENSE V3 OR LATER (GPLV3+)'
	@echo "LOADER `uname -v`"
	@echo  'EXEC MAKE'
#	@ echo "`free -t -b | grep -oP '\d+' | sed '1!d' | numfmt --to=iec --suffix=B --format %.2f` RAM SYSTEM"
#	@ echo "`free -t -b | grep -oP '\d+' | sed '3!d' | numfmt --to=iec --suffix=B --format %.2f` FREE"
	@echo  'NO HOLOTAPE FOUND'
	@echo  'NO PLUGINS TO LOAD'
	@echo "********************************************************************************"

venv-create: header
	@ if test -d $(GLXSH_VENV_DIRECTORY);\
	then echo "VIRTUAL ENVIRONMENT							[  OK  ]";\
	else echo "VIRTUAL ENVIRONMENT							[CREATE]" &&\
		cd $(GLXSH_DIRECTORY) && python3 -m venv $(GLXSH_VENV_NAME);\
	fi

venv-requirements: venv-create
	@${GLXSH_VENV_ACTIVATE} && pip3 install -U pip --no-cache-dir --quiet &&\
	echo "PIP3									[  OK  ]" || \
	echo "PIP3									[FAILED]"

	@${GLXSH_VENV_ACTIVATE} && pip3 install -U wheel --no-cache-dir --quiet &&\
	echo "WHEEL									[  OK  ]" || \
	echo "WHEEL									[FAILED]"

	@${GLXSH_VENV_ACTIVATE} && pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo "SETUPTOOLS								[  OK  ]" || \
	echo "SETUPTOOLS								[FAILED]"

	@${GLXSH_VENV_ACTIVATE} && pip install -U --no-cache-dir -q Pygments &&	python setup.py --quiet develop &&\
	echo "REQUIREMENTS								[  OK  ]" || \
	echo "REQUIREMENTS								[FAILED]"

prepare: venv-requirements

tests: prepare
	@echo 'RUN TESTS'
	@ ${GLXSH_VENV_ACTIVATE} && python setup.py tests

static: prepare
	@ echo "BUILD ENTRY POINT AS STATIC BINARY"
	@ ${GLXSH_VENV_ACTIVATE} &&\
 	pip3 install -U pyinstaller --no-cache-dir --quiet &&\
 	pyinstaller $(GLXSH_DIRECTORY)/GLXShell/glxsh.py --clean --log-level ERROR --noconfirm --onefile --name glxsh-$$(arch).bin

shell: static
	@ echo "START GLXSH"
	@ $(GLXSH_DIRECTORY)/dist/glxsh-$$(arch).bin

# https://www.devdungeon.com/content/how-build-python-source
#build-python:
#	test -d $(GLXSH_DIRECTORY)/env || mkdir $(GLXSH_DIRECTORY)/env &&\
#	test -d $(GLXSH_DIRECTORY)/pyenv || mkdir $(GLXSH_DIRECTORY)/pyenv &&\
#	test -d $(GLXSH_DIRECTORY)/dep || mkdir $(GLXSH_DIRECTORY)/dep &&\
#	cd $(GLXSH_DIRECTORY)/dep && wget -N -c https://github.com/python/cpython/archive/v3.9.0.tar.gz &&\
#	tar -xzvf v3.9.0.tar.gz &&\
#	cd cpython-3.9.0 &&\
#	./configure --enable-optimizations --prefix=$(GLXSH_DIRECTORY)/pyenv &&\
#	make &&\
#	make install


docs: venv
	@ echo "BUILD DOCUMENTATIONS"
	@ ${GLXSH_VENV_ACTIVATE} &&\
	pip3 install --upgrade pip setuptools wheel --no-cache-dir --quiet &&\
  	pip3 install -r docs/requirements.txt --no-cache-dir --quiet &&\
	cd $(GLXSH_DIRECTORY)/docs &&\
	sphinx-apidoc -e -f -o source/ ../GLXShell/ &&\
	make html

clean:
	@echo 'CLEAN UP'
	rm -rf ./.eggs
	rm -rf ./build
	rm -rf ./dist
	rm -rf ./*.egg-info
	rm -rf ./*.spec
	rm -rf ./venv
	rm -rf ./pyenv
	rm -rf ./env
	rm -rf ./dep
	rm -rf ./docs/source/GLXShell.libs.*
	rm -rf ./docs/source/GLXShell.plugins.*
	rm -rf ./docs/source/GLXShell.rst
	rm -rf ./docs/source/GLXShell.glxsh.rst
	rm -rf ./docs/source/modules.rst
	rm -rf ./docs/build
