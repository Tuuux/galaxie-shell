# GALAXIE SHELL - CHANGELOGS
**v0.2.7 - soon**
  * Add ``bashlex`` as internal parser
  * Add -q arg to ``ls`` command to enclose entry names in double quotes
  * Convert internal libs to python module
**v0.2.6 - 20240320**
  * Fixe ``ls -R``
  * Fixe partially ``alias`` where still in need of a True regexpr
  * Limit the number of warning of pylint
**v0.2.5 - 20240319**
  * Migrate to pyproject.toml
  * Man commands utilities use COLUMNS environment variable for text wrap
  * Make build mono-file work after internal libs migration
  * Migrate to internal TextWrap methods
  * Migrate to internal Tabulate methods
  * Migrate to internal Path methods
  * Add nargs="..." capability to argparse
  * Change license for WTFPL
  * Argparse with help it look like POSIX man
  * (Better exit code)
  * (Better stdout, stderr, stdin)
  * Remove ``help`` command and introduce ``man`` command
  * Remove all buildin plugin management
  * Migrate every command into utilities
  * Mono file refactoring
  * make docs generate the internal man page
  * Add ``tee``, ``touch``, ``date``, ``time``, ``alias``, ``unalias``, ``du``
  * Introduce PS1 management
  * ``ls -l`` return group and username in place of UID GID
**v0.2.4 - 20220407**
  * Fixe unittest due to Cmd2.ansi breakout
  * Continue migration to  full MicroPython
  * Add ``basename`` with MicroPython and python support
  * Migrate ``uname`` with MicroPython and python support
  * Update Argparse class and tests form py-copy project
  * Introduction to ``env`` experimental support
  * Remove ``arch`` builtins command
  * Optimize Makefile
  * Fixe prompt test
  * Optimize prompt speed
  * Add ``tty`` commandset
  * Minimize ``rmdir`` commandset
**v0.2.3 - 20211009**
  * Add internal ``df`` commandset
**v0.2.2 - 20210927**
  * Fixe issue with CMD2 about ipython
  * Fixe issue with CMD2 about syntax changes
  * Better Makefile
  * Remove __pycache__ from tracking
  * Better ``cd`` unittest about ``OLDPWD`` environment variable
**v0.2.1 - 20210110**
  * Add sleep command
  * Add sleep documentation
**v0.2 - 20201113**
  * Better Makefile
  * Better documentation
  * Ready to me use on other project
**v0.1.2 - 20201112**
  * Add Application Class with properties Name, Version, License, Description, Authors, Warranty
  * Shell Class include Application class and all properties
  * Intro class (and other's) use Application class information's
  * First version it permit to generate a shell by include ``galaxie-shell`` in a new project
  * Plugins Manager review
**v0.1.2a2 - 20201111**
  * Plugins Manager refactoring
  * Delete utils modules
  * Makefile add documentations capability
  * Makefile clean care about documentation
**v0.1.2a1 - 20201107**
  * Fixe issue #2 about capability to shell to disable plugins auto load
  * Better Makefile
  * Change doc theme for python docs theme
  * Better version management
**v0.1.1 - 20201107**
  * Fixe issue #1 about wrong version
**v0.1 - 20201107**
  * Documentation publish on readthedocs
  * Documentation conversion to rst
**v0.1a5 - 20201106**
  * Better tests for Builtins Plugin
  * Refactor tests directory
  * 100% Code covering
**v0.1a4 - 20201105**
  * Fixe Tests
  * Better init
  * Add documentation