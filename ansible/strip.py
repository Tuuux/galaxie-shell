import argparse
import ast
import astunparse
import sys

parser = argparse.ArgumentParser(
    add_help=True,
    prog="strip",
    description="Strip a file to remove comment's Docstring and spare newline"
)
parser.add_argument(
    "source",
    type=argparse.FileType('r'),
    help="source file"

)
parser.add_argument(
    "destination",
    help="destination file"

)
args = parser.parse_args()
try:
    with open(args.source.name) as source:
        lines = astunparse.unparse(ast.parse(source.read())).split('\n')

        with open(args.destination, "w") as destination:
            for line in lines:
                if line.lstrip()[:1] not in ("'", '"'):
                    destination.write("%s\n" % line)
except Exception as error:
    sys.stdout.write("strip: %s\n" % error)
