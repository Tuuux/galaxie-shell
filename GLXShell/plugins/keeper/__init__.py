__authors__ = ["Tuxa"]
__date__ = "20201104"
__version__ = "0.1"
__appname__ = "glxsh keeper plugin"
__description__ = "Galaxie Clans commands set"
__licence__ = """Copyright (C) 2020 Galaxie Shell Project.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
"""

from GLXShell.libs.commands import GLXShellCommands
from GLXShell.plugins.keeper.keeper import LoadableKeeper


class GLXShellPluginBuiltins(GLXShellCommands):
    def __init__(self):
        GLXShellCommands.__init__(self)
        self.commands = [
            {"name": "keeper", "object": LoadableKeeper()},
        ]
