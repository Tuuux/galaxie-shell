import unittest
import os
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.cat import glxsh_cat


class TestGLXArch(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

        if os.path.isfile(os.path.join(os.getcwd(), "test_cat_1.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_cat_1.tmp"))
        if os.path.isfile(os.path.join(os.getcwd(), "test_cat_2.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_cat_2.tmp"))

        self.tmp_file_1 = os.path.join(os.getcwd(), "test_cat_1.tmp")
        self.tmp_file_2 = os.path.join(os.getcwd(), "test_cat_2.tmp")

        file = open(self.tmp_file_1, "w")
        file.write("Hello 41\n\n\tHello 41\n\n\t\tHello 41\n")

        file.close()

        file = open(self.tmp_file_2, "w")
        file.write("Hello 42\n\n\tHello 42\n\n\t\tHello 42\n")
        file.close()

    def tearDown(self):
        if os.path.isfile(os.path.join(os.getcwd(), "test_cat_1.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_cat_1.tmp"))
        if os.path.isfile(os.path.join(os.getcwd(), "test_cat_2.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_cat_2.tmp"))

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  cat - concatenate and print files

SYNOPSIS
  cat [-u] [file...]

DESCRIPTION
  The cat utility shall read files in sequence and shall write their contents to
  the standard output in the same sequence.

OPERANDS
  file  A pathname of an input file. If no file operands are specified, the
        standard input shall be used. If a file is '-', the cat utility shall
        read from the standard input at that point in the sequence. The cat
        utility shall not close and reopen standard input when it is referenced
        in this way, but shall accept multiple occurrences of '-' as a file
        operand.

OPTIONS
  -u  Ignored
"""
            self.shell.onecmd("man cat")

            self.assertEqual(expected, fake_out.getvalue())

    def test_glxsh_cat(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_1, "r") as tmp_file_1:
                expected = "%s" % tmp_file_1.read()
            self.assertEqual(0, glxsh_cat(files=[self.tmp_file_1]))
            self.assertEqual(expected, fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_2, "r") as tmp_file_2:
                expected = "%s" % tmp_file_2.read()
            self.assertEqual(0, glxsh_cat(files=[self.tmp_file_2]))
            self.assertEqual(expected, fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_1, "r") as tmp_file_1, open(self.tmp_file_2, "r") as tmp_file_2:
                expected = "%s%s" % (tmp_file_1.read(), tmp_file_2.read())
            self.assertEqual(0, glxsh_cat(files=[self.tmp_file_1, self.tmp_file_2]))
            self.assertEqual(expected, fake_out.getvalue())


if __name__ == "__main__":
    unittest.main()
