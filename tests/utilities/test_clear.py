import unittest
import sys
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.clear import glxsh_clear


class TestUtilityClear(unittest.TestCase):

    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79\

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  clear

SYNOPSIS
  clear

DESCRIPTION
  Clear screen
"""
            self.shell.onecmd("man clear")
            self.assertEqual(expected, fake_out.getvalue())

    def test_over_glxush(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            sys.stdout.write("Hello.42")
            self.assertEqual(fake_out.getvalue(), "Hello.42")
            GLXUsh().onecmd("clear")
            self.assertEqual("Hello.42\x1b[2J\x1b[H", fake_out.getvalue())


if __name__ == "__main__":
    unittest.main()
