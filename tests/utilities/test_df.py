import os.path
import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.df import df_find_mount_point
from glxshell.utilities.df import df_get_device_information
from glxshell.utilities.df import df_get_devices
from glxshell.utilities.df import df_get_file_content
from glxshell.utilities.df import df_get_info_to_print
from glxshell.utilities.df import df_get_totals
from glxshell.utilities.df import df_print_final
from glxshell.utilities.df import glxsh_df


class TestUtilityDF(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  df - report free disk space

SYNOPSIS
  df [-h] [-k] [-P] [-t] [file]

DESCRIPTION
  The df utility shall write the amount of available space and file slots for
  file systems on which the invoking user has appropriate read access. File
  systems shall be specified by the file operands; when none are specified,
  information shall be written for all file systems.The format of the default
  output from df is unspecified, but all space figures are reported in 512-byte
  units, unless the -k option is specified. This output shall contain at least
  the file system names, amount of available space on each of these file systems,
  and, if no options other than -t are specified, the number of free file slots,
  or inodes, available; when -t is specified, the output shall contain the total
  allocated space as well.

OPERANDS
  file  A pathname of a file within the hierarchy of the desired file system.
        If a file other than a FIFO, a regular file, a directory, or a special
        file representing the device containing the file system (for example,
        /dev/dsk/0s1) is specified, the results are unspecified. If the file
        operand names a file other than a special file containing a file
        system, df shall write the amount of free space in the file system
        containing the specified file operand. Otherwise, df shall write the
        amount of free space in that file system.

OPTIONS
  -h  Print sizes in powers of 1024 (e.g., 1023M)
  -k  Use 1024-byte units, instead of the default 512-byte units, when writing
      space figures.
  -P  Produce a POSIX output
  -t  Include total allocated-space figures in the output.
"""
            self.shell.onecmd("man df")

            self.assertEqual(expected, fake_out.getvalue())

    def test_df_get_devices(self):
        # self.assertEqual("", self.df.get_devices())
        file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "data",
            "mtab",
        )
        data = [
            ["sysfs", "/sys", "sysfs", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
            ["proc", "/proc", "proc", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
            ["udev", "/dev", "devtmpfs", "rw,nosuid,relatime,size=3962900k,nr_inodes=990725,mode=755", "0", "0"],
            ["devpts", "/dev/pts", "devpts", "rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000", "0", "0"],
            ["tmpfs", "/run", "tmpfs", "rw,nosuid,nodev,noexec,relatime,size=796456k,mode=755", "0", "0"],
            ["/dev/sda2", "/", "ext4", "rw,relatime,errors=remount-ro", "0", "0"],
            ["securityfs", "/sys/kernel/security", "securityfs", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
            ["tmpfs", "/dev/shm", "tmpfs", "rw,nosuid,nodev", "0", "0"],
            ["tmpfs", "/run/lock", "tmpfs", "rw,nosuid,nodev,noexec,relatime,size=5120k", "0", "0"],
            [
                "cgroup2",
                "/sys/fs/cgroup",
                "cgroup2",
                "rw,nosuid,nodev,noexec,relatime,nsdelegate,memory_recursiveprot",
                "0",
                "0",
            ],
            ["pstore", "/sys/fs/pstore", "pstore", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
            ["efivarfs", "/sys/firmware/efi/efivars", "efivarfs", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
            ["none", "/sys/fs/bpf", "bpf", "rw,nosuid,nodev,noexec,relatime,mode=700", "0", "0"],
            [
                "systemd-1",
                "/proc/sys/fs/binfmt_misc",
                "autofs",
                "rw,relatime,fd=30,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=11343",
                "0",
                "0",
            ],
            ["tracefs", "/sys/kernel/tracing", "tracefs", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
            ["hugetlbfs", "/dev/hugepages", "hugetlbfs", "rw,relatime,pagesize=2M", "0", "0"],
            ["mqueue", "/dev/mqueue", "mqueue", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
            ["debugfs", "/sys/kernel/debug", "debugfs", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
            ["configfs", "/sys/kernel/config", "configfs", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
            ["fusectl", "/sys/fs/fuse/connections", "fusectl", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
            [
                "/dev/sda1",
                "/boot/efi",
                "vfat",
                "rw,relatime,fmask=0077,dmask=0077,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro",
                "0",
                "0",
            ],
            [
                "tmpfs",
                "/run/user/1000",
                "tmpfs",
                "rw,nosuid,nodev,relatime,size=796452k,nr_inodes=199113,mode=700,uid=1000,gid=1000",
                "0",
                "0",
            ],
            ["binfmt_misc", "/proc/sys/fs/binfmt_misc", "binfmt_misc", "rw,nosuid,nodev,noexec,relatime", "0", "0"],
        ]
        self.assertEqual(data, df_get_devices(file=file))

    def test_df_get_file_content(self):
        file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "data",
            "mtab",
        )
        data = """sysfs /sys sysfs rw,nosuid,nodev,noexec,relatime 0 0
proc /proc proc rw,nosuid,nodev,noexec,relatime 0 0
udev /dev devtmpfs rw,nosuid,relatime,size=3962900k,nr_inodes=990725,mode=755 0 0
devpts /dev/pts devpts rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000 0 0
tmpfs /run tmpfs rw,nosuid,nodev,noexec,relatime,size=796456k,mode=755 0 0
/dev/sda2 / ext4 rw,relatime,errors=remount-ro 0 0
securityfs /sys/kernel/security securityfs rw,nosuid,nodev,noexec,relatime 0 0
tmpfs /dev/shm tmpfs rw,nosuid,nodev 0 0
tmpfs /run/lock tmpfs rw,nosuid,nodev,noexec,relatime,size=5120k 0 0
cgroup2 /sys/fs/cgroup cgroup2 rw,nosuid,nodev,noexec,relatime,nsdelegate,memory_recursiveprot 0 0
pstore /sys/fs/pstore pstore rw,nosuid,nodev,noexec,relatime 0 0
efivarfs /sys/firmware/efi/efivars efivarfs rw,nosuid,nodev,noexec,relatime 0 0
none /sys/fs/bpf bpf rw,nosuid,nodev,noexec,relatime,mode=700 0 0
systemd-1 /proc/sys/fs/binfmt_misc autofs rw,relatime,fd=30,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=11343 0 0
tracefs /sys/kernel/tracing tracefs rw,nosuid,nodev,noexec,relatime 0 0
hugetlbfs /dev/hugepages hugetlbfs rw,relatime,pagesize=2M 0 0
mqueue /dev/mqueue mqueue rw,nosuid,nodev,noexec,relatime 0 0
debugfs /sys/kernel/debug debugfs rw,nosuid,nodev,noexec,relatime 0 0
configfs /sys/kernel/config configfs rw,nosuid,nodev,noexec,relatime 0 0
fusectl /sys/fs/fuse/connections fusectl rw,nosuid,nodev,noexec,relatime 0 0
/dev/sda1 /boot/efi vfat rw,relatime,fmask=0077,dmask=0077,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro 0 0
tmpfs /run/user/1000 tmpfs rw,nosuid,nodev,relatime,size=796452k,nr_inodes=199113,mode=700,uid=1000,gid=1000 0 0
binfmt_misc /proc/sys/fs/binfmt_misc binfmt_misc rw,nosuid,nodev,noexec,relatime 0 0"""
        self.assertEqual(data, df_get_file_content(file=file))

        self.assertRaises(FileExistsError, df_get_file_content, file="Hello.42")

    def test_df_print_final(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            data = [["sysfs", "0", "0", "0", "-", "/sys"], ["total", 507297544, 191836496, 315461048, "38%", "-"]]
            df_print_final(block_size_text="512", tabular_data=data)
            self.assertEqual(
                """Filesystem       512      Used Available Capacity Mounted on
sysfs              0         0         0        - /sys
total      507297544 191836496 315461048      38% -
""",
                fake_out.getvalue(),
            )

    def test_df_get_info_to_print(self):
        data = [["sysfs", "-", "-", "-", "-", "/sys"], ["total", "42", "42", "42", "38%", "-"]]
        self.assertEqual(
            ("Size", [["sysfs", "-", "-", "-", "-", "/sys"], ["total", "21.00K", "21.00K", "21.00K", "38%", "-"]]),
            df_get_info_to_print(block_size=512, devices_list=data, human_readable=True),
        )

    def test_df_get_totals(self):
        data = [
            ["sysfs", "0", "0", "0", "-", "/sys"],
            ["sysfs", "-", "-", "-", "-", "/sys"],
            ["total", 42, 42, 42, "38%", "-"],
        ]
        self.assertEqual((42, 42, 42), df_get_totals(devices_list=data))

    def test_df_find_mount_point(self):
        self.assertEqual("/dev", df_find_mount_point("/dev/null"))
        self.assertEqual("/proc", df_find_mount_point("/proc/meminfo"))

    def test_df_get_device_information(self):
        data = df_get_device_information(file_system_name="ext4", file_system_root="/", block_size=512)
        self.assertEqual("ext4", data[0])
        self.assertEqual("/", data[5])

    def test_df(self):
        self.assertEqual("df: Hello.42: No such file or directory", glxsh_df(file="Hello.42"))


if __name__ == "__main__":
    unittest.main()
