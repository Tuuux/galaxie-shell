import os
import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.mkdir import glxsh_mkdir


class TestUtilityMkdir(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79
        self.temporary_directory_name_1 = "tests_mkdir"
        self.temporary_directory_name_2 = os.path.join(self.temporary_directory_name_1, "42")

    def tearDown(self):
        if os.path.exists(self.temporary_directory_name_2):
            if os.path.isdir(self.temporary_directory_name_2):
                os.rmdir(self.temporary_directory_name_2)
            if os.path.isfile(self.temporary_directory_name_2) or os.path.islink(self.temporary_directory_name_2):
                os.remove(self.temporary_directory_name_2)

        if os.path.exists(self.temporary_directory_name_1):
            if os.path.isdir(self.temporary_directory_name_1):
                os.rmdir(self.temporary_directory_name_1)
            if os.path.isfile(self.temporary_directory_name_1) or os.path.islink(self.temporary_directory_name_1):
                os.remove(self.temporary_directory_name_1)

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  mkdir - make directories

SYNOPSIS
  mkdir [-p] [-m] dir...

DESCRIPTION
  The mkdir utility shall create the directories specified by the operands

OPERANDS
  dir  A pathname of a directory to be created.

OPTIONS
  -p  Create any missing intermediate pathname components.
  -m  Set the file permission bits of the newly-created directory to the
      specified mode value.
"""
            self.shell.onecmd("man mkdir")
            self.assertEqual(expected, fake_out.getvalue())

    def test_mkdir(self):

        if os.path.exists(self.temporary_directory_name_2):
            if os.path.isdir(self.temporary_directory_name_2):
                os.rmdir(self.temporary_directory_name_2)
            if os.path.isfile(self.temporary_directory_name_2) or os.path.islink(self.temporary_directory_name_2):
                os.remove(self.temporary_directory_name_2)

        if os.path.exists(self.temporary_directory_name_1):
            if os.path.isdir(self.temporary_directory_name_1):
                os.rmdir(self.temporary_directory_name_1)
            if os.path.isfile(self.temporary_directory_name_1) or os.path.islink(self.temporary_directory_name_1):
                os.remove(self.temporary_directory_name_1)

        self.assertFalse(os.path.isdir(self.temporary_directory_name_1))
        self.assertFalse(os.path.isdir(self.temporary_directory_name_2))

        returned_value = glxsh_mkdir(
            directories=[
                self.temporary_directory_name_1,
                self.temporary_directory_name_2,
            ],
            parents=False,
            mode="755",
        )
        self.assertEqual(0, returned_value)
        self.assertTrue(os.path.isdir(self.temporary_directory_name_1))
        self.assertTrue(os.path.isdir(self.temporary_directory_name_2))
        with patch("sys.stderr", new=StringIO()) as fake_out:
            returned_value = glxsh_mkdir(
                directories=[
                    self.temporary_directory_name_1,
                    self.temporary_directory_name_2,
                ],
                parents=False,
                mode="755",
            )
            expected = (
                "mkdir: '{0}': File exists\n"
                "mkdir: '{1}': File exists\n".format(
                    self.temporary_directory_name_1, self.temporary_directory_name_2
                )
            )
            self.assertEqual(1, returned_value)
            self.assertEqual(expected, fake_out.getvalue())

        with patch("sys.stderr", new=StringIO()) as fake_out:
            expected = (
                "mkdir: '{0}': File exists\n"
                "mkdir: '{1}': File exists\n".format(
                    self.temporary_directory_name_1, self.temporary_directory_name_2
                )
            )
            GLXUsh().onecmd("mkdir %s %s -m '755'" % (self.temporary_directory_name_1, self.temporary_directory_name_2))
            self.assertEqual(expected, fake_out.getvalue())

        if os.path.exists(self.temporary_directory_name_2):
            if os.path.isdir(self.temporary_directory_name_2):
                os.rmdir(self.temporary_directory_name_2)
            if os.path.isfile(self.temporary_directory_name_2) or os.path.islink(self.temporary_directory_name_2):
                os.remove(self.temporary_directory_name_2)

        if os.path.exists(self.temporary_directory_name_1):
            if os.path.isdir(self.temporary_directory_name_1):
                os.rmdir(self.temporary_directory_name_1)
            if os.path.isfile(self.temporary_directory_name_1) or os.path.islink(self.temporary_directory_name_1):
                os.remove(self.temporary_directory_name_1)


if __name__ == "__main__":
    unittest.main()
