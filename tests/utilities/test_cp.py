import os
import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.cp import glxsh_cp


class TestUtilityCp(unittest.TestCase):

    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  cp - copy files

SYNOPSIS
  cp [-f] [-i] source_file... target_file

DESCRIPTION
  The first synopsis form is denoted by two operands, neither of which are
  existing files of type directory. The cp utility shall copy the contents of
  source_file (or, if source_file is a file of type symbolic link, the contents
  of the file referenced by source_file) to the destination path named by
  target_file.

OPERANDS
  source_file  A pathname of a file to be copied. If a source_file operand is
               '-', it shall refer to a file named -; implementations shall not
               treat it as meaning standard input. target_file
  target_file  A pathname of an existing or nonexistent file, used for the
               output when a single file is copied. If a target_file operand is
               '-', it shall refer to a file named -; implementations shall not
               treat it as meaning standard output.

OPTIONS
  -f  If a file descriptor for a destination file cannot be obtained, as
      described in step 3.a.ii., attempt to unlink the destination file and
      proceed.
  -i  Write a prompt to standard error before copying to any existing non-
      directory destination file.

EXIT STATUS
  0   The utility executed successfully and all requested changes were made.
  >0  An error occurred.
"""
            self.shell.onecmd("man cp")

            self.assertEqual(expected, fake_out.getvalue())

    def test_cp(self):
        source_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../utilities/data", "cp.txt")
        target_file = "{0}.copy".format(source_file)

        if os.path.exists(target_file):
            if os.path.isdir(target_file):
                os.rmdir(target_file)
            if os.path.isfile(target_file) or os.path.islink(target_file):
                os.remove(target_file)

        self.assertFalse(os.path.exists(target_file))

        glxsh_cp(source_file=source_file, target_file=target_file, interactive=False)

        self.assertTrue(os.path.exists(target_file))

        with patch("builtins.input", return_value="yes"), patch("sys.stdout", new=StringIO()) as fake_out:
            glxsh_cp(source_file=source_file, target_file=target_file, interactive=True)
            self.assertEqual(fake_out.getvalue().strip(), "")

        with patch("builtins.input", return_value="no"), patch("sys.stdout", new=StringIO()) as fake_out:
            glxsh_cp(source_file=source_file, target_file=target_file, interactive=True)
            self.assertEqual(fake_out.getvalue().strip(), "")

        if os.path.exists(target_file):
            if os.path.isdir(target_file):
                os.rmdir(target_file)
            if os.path.isfile(target_file) or os.path.islink(target_file):
                os.remove(target_file)

        with patch("sys.stderr", new=StringIO()) as fake_out:
            glxsh_cp(source_file=target_file, target_file=target_file)
            self.assertEqual(
                fake_out.getvalue(),
                "cp: No such file or directory: '{0}'\n".format(target_file),
            )


if __name__ == "__main__":
    unittest.main()
