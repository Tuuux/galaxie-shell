import os
import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.cd import glxsh_cd


class TestGLXCd(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

        self.temp_dir_1 = "tests_cd"
        self.temp_dir_2 = os.path.join(self.temp_dir_1, "42")
        self.maxDiff = None

        if os.path.exists(self.temp_dir_2):
            if os.path.isdir(self.temp_dir_2):
                os.rmdir(self.temp_dir_2)
            if os.path.isfile(self.temp_dir_2) or os.path.islink(self.temp_dir_2):
                os.remove(self.temp_dir_2)

        if os.path.exists(self.temp_dir_1):
            if os.path.isdir(self.temp_dir_1):
                os.rmdir(self.temp_dir_1)
            if os.path.isfile(self.temp_dir_1) or os.path.islink(self.temp_dir_1):
                os.remove(self.temp_dir_1)

    def tearDown(self):
        if os.path.exists(self.temp_dir_2):
            if os.path.isdir(self.temp_dir_2):
                os.rmdir(self.temp_dir_2)
            if os.path.isfile(self.temp_dir_2) or os.path.islink(self.temp_dir_2):
                os.remove(self.temp_dir_2)

        if os.path.exists(self.temp_dir_1):
            if os.path.isdir(self.temp_dir_1):
                os.rmdir(self.temp_dir_1)
            if os.path.isfile(self.temp_dir_1) or os.path.islink(self.temp_dir_1):
                os.remove(self.temp_dir_1)

    # def test_cd(self):
    #     self.assertEqual(pwd(logical=True, physical=True), os.path.normpath(os.getcwd()))
    #     self.assertEqual(pwd(logical=False, physical=True), os.path.realpath(os.getcwd()))

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  cd - change the working directory

SYNOPSIS
  cd [-P] [-L] [directory]

DESCRIPTION
  The cd utility shall change the working directory of the current shell
  execution environment

OPERANDS
  directory  An absolute or relative pathname of the directory that shall
             become the new working directory. The interpretation of a relative
             pathname by cd depends on the -L option and the CDPATH and PWD
             environment variables. If directory is an empty string, the
             directory be come HOME environment variable.

OPTIONS
  -P  Handle the operand dot-dot physically; symbolic link components shall be
      resolved before dot-dot components are processed
  -L  Handle the operand dot-dot logically; symbolic link components shall not
      be resolved before dot-dot components are processed
"""
            self.shell.onecmd("man cd")
            self.assertEqual(expected, fake_out.getvalue())


if __name__ == "__main__":
    unittest.main()
