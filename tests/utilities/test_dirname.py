import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.dirname import glxsh_dirname


class TestUtilityDirname(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_glxsh_dirname(self):
        # Test form https://pubs.opengroup.org/onlinepubs/9699919799/functions/basename.html#tag_16_32
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """.
.
.
/
/
/
/
/usr
//usr
/home/dwc
"""
            self.assertEqual(0, glxsh_dirname("usr"))
            self.assertEqual(0, glxsh_dirname("usr/"))
            self.assertEqual(0, glxsh_dirname(""))
            self.assertEqual(0, glxsh_dirname("/"))
            self.assertEqual(0, glxsh_dirname("//"))
            self.assertEqual(0, glxsh_dirname("///"))
            self.assertEqual(0, glxsh_dirname("/usr/"))
            self.assertEqual(0, glxsh_dirname("/usr/lib"))
            self.assertEqual(0, glxsh_dirname("//usr//lib"))
            self.assertEqual(0, glxsh_dirname("/home//dwc//test"))

            self.assertEqual(expected, fake_out.getvalue())

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  dirname - return the directory portion of a pathname

SYNOPSIS
  dirname [string]

DESCRIPTION
  The string operand shall be treated as a pathname, as defined in XBD Pathname.
  The string string shall be converted to the name of the directory containing
  the filename corresponding to the last pathname component in string.

OPERANDS
  string  A string
"""
            self.shell.onecmd("man dirname")

            self.assertEqual(expected, fake_out.getvalue())


if __name__ == "__main__":
    unittest.main()
