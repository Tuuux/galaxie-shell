import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.env import glxsh_env


class TestUtilityEnv(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79
    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  env - set the environment for command invocation

SYNOPSIS
  env [-i] [name] [utility] [argument]

DESCRIPTION
  The env utility shall obtain the current environment, modify it according to
  its arguments, then invoke the utility named by the utility operand with the
  modified environment.

OPERANDS
  name      Arguments of the form name= value shall modify the execution
            environment, and shall be placed into the inherited environment
            before the utility is invoked.
  utility   The name of the utility to be invoked. If the utility operand names
            any of the special built-in utilities in Special Built-In
            Utilities, the results are undefined.
  argument  A string to pass as an argument for the invoked utility.

OPTIONS
  -i  Invoke utility with exactly the environment specified by the arguments;
      the inherited environment shall be ignored completely.
"""
            self.shell.onecmd("man env")

            self.assertEqual(expected, fake_out.getvalue())

    def test_env(self):
        pass


if __name__ == "__main__":
    unittest.main()
