import os
import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.pwd import glxsh_pwd


class TestGLXPwd(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_glxsh_pwd(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            exit_code = glxsh_pwd(logical=True, physical=True)
            self.assertEqual("%s\n" % os.path.normpath(os.getcwd()), fake_out.getvalue())
            self.assertEqual(0, exit_code)

        with patch("sys.stdout", new=StringIO()) as fake_out:
            exit_code = glxsh_pwd(logical=False, physical=True)
            self.assertEqual("%s\n" % os.path.realpath(os.getcwd()), fake_out.getvalue())
            self.assertEqual(0, exit_code)

        with patch("sys.stdout", new=StringIO()) as fake_out:
            exit_code = glxsh_pwd(logical=False, physical=False)
            self.assertEqual("%s\n" % os.path.normpath(os.getcwd()), fake_out.getvalue())
            self.assertEqual(0, exit_code)

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  pwd - return working directory name

SYNOPSIS
  pwd [-L] [-P]

DESCRIPTION
  The pwd utility shall write to standard output an absolute pathname of the
  current working directory, which does not contain the filenames dot or dot-dot.

OPTIONS
  -L  Print the value of $PWD if it names the current working directory
  -P  Print the physical directory, without any symbolic links
"""
            self.shell.onecmd("man pwd")
            self.assertEqual(expected, fake_out.getvalue())

    def test_over_glxush(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            GLXUsh().onecmd("pwd -P -L")
            self.assertEqual("%s\n" % os.path.normpath(os.getcwd()), fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            GLXUsh().onecmd("pwd -P")
            self.assertEqual("%s\n" % os.path.realpath(os.getcwd()), fake_out.getvalue())


if __name__ == "__main__":
    unittest.main()
