import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.touch import glxsh_touch


class TestUtilityTouch(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  touch - change file access and modification times

SYNOPSIS
  touch [-acm] [-r ref_file|-t time|-d date_time] file...

DESCRIPTION
  The touch utility shall change the last data modification timestamps, the last
  data access timestamps, or both.

  The time used can be specified by the -t time
  option-argument, the corresponding time fields of the file referenced by the -r
  ref_file option-argument, or the -d date_time option-argument, as specified in
  the following sections. If none of these are specified, touch shall use the
  current time.

OPERANDS
  file  A pathname of a file whose times shall be modified.

OPTIONS
  -a  Change the access time of file. Do not change the modification time
      unless -m is also specified.
  -c  Do not create a specified file if it does not exist. Do not write any
      diagnostic messages concerning this condition.
  -m  Change the modification time of file. Do not change the access time
      unless -a is also specified.
  -d  Use the specified date_time instead of the current time
  -r  Use the corresponding time of the file named by the pathname ref_file
      instead of the current time.
  -t  Use the specified time instead of the current time. The option-argument
      shall be a decimal number of the form

EXIT STATUS
  0   The utility executed successfully and all requested changes were made.
  >0  An error occurred.
"""
            self.shell.onecmd("man touch")

            self.assertEqual(expected, fake_out.getvalue())

    def test_glxsh_touch(self):
        with patch("sys.stderr", new=StringIO()) as fake_out:
            exit_code = glxsh_touch()
            self.assertEqual(0, exit_code)


if __name__ == "__main__":
    unittest.main()
