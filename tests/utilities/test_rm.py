import unittest
import os
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.rm import glxsh_rm


class TestUtilityRm(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79
        self.temporary_directory_name_1 = "tests_rm"
        self.temporary_directory_name_2 = os.path.join(self.temporary_directory_name_1, "42")

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  rm - remove directory entries

SYNOPSIS
  rm [-i] [-R, -r] [-f] file...

DESCRIPTION
  The rm utility shall remove the directory entry specified by each file
  argument.

  If either of the files dot or dot-dot are specified as the basename
  portion of an operand (that is, the final pathname component) or if an operand
  resolves to the root directory, rm shall write a diagnostic message to standard
  error and do nothing more with such operands.

OPERANDS
  file  A pathname of a directory entry to be removed.

OPTIONS
  -i      Prompt for confirmation as described previously. Any previous
          occurrences of the -f option shall be ignored.
  -R, -r  Remove file hierarchies. See the DESCRIPTION.
  -f      Do not prompt for confirmation. Do not write diagnostic messages or
          modify the exit status in the case of no file operands, or in the
          case of operands that do not exist. Any previous occurrences of the
          -i option shall be ignored.
"""
            self.shell.onecmd("man rm")
            self.assertEqual(expected, fake_out.getvalue())

    def test_glxsh_rm(self):
        if not os.path.exists(self.temporary_directory_name_1):
            os.mkdir(self.temporary_directory_name_1)
        if not os.path.exists(self.temporary_directory_name_2):
            os.mkdir(self.temporary_directory_name_2)

        self.assertTrue(os.path.isdir(self.temporary_directory_name_1))
        self.assertTrue(os.path.isdir(self.temporary_directory_name_2))

        with patch("sys.stderr", new=StringIO()) as fake_out:
            glxsh_rm(file=[self.temporary_directory_name_1])
            self.assertEqual(
                "rm: Is a directory: '%s'\n" % self.temporary_directory_name_1, fake_out.getvalue()
            )

        self.assertTrue(os.path.isdir(self.temporary_directory_name_1))
        self.assertTrue(os.path.isdir(self.temporary_directory_name_2))

        with patch("sys.stdout", new=StringIO()) as fake_out:
            GLXUsh().onecmd("rm %s" % self.temporary_directory_name_1)
            expected = ""
            self.assertEqual(expected, fake_out.getvalue())


if __name__ == "__main__":
    unittest.main()
