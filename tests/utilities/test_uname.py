import os
import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.uname import glxsh_uname


class TestBaseName(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_help(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  uname - return system name

SYNOPSIS
  uname [-a] [-s] [-n] [-r] [-v] [-m]

DESCRIPTION
  By default, the uname utility shall write the operating system name to standard
  output. When options are specified, symbols representing one or more system
  characteristics shall be written to the standard output.

OPTIONS
  -a  Behave as though all of the options -mnrsv were specified.
  -s  Write the name of the implementation of the operating system.
  -n  Write the name of this node within an implementation-defined
      communications network.
  -r  Write the current release level of the operating system implementation.
  -v  Write the current version level of this release of the operating system
      implementation.
  -m  Write the name of the hardware type on which the system is running.
"""
            self.shell.onecmdhooks("man uname")
            self.assertEqual(expected, fake_out.getvalue())

    def test_glxsh_uname(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            exit_code = glxsh_uname()
            self.assertEqual(0, exit_code)
            self.assertEqual("%s\n" % os.uname().sysname, fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            exit_code = glxsh_uname(sysname=True)
            self.assertEqual(0, exit_code)
            self.assertEqual("%s\n" % os.uname().sysname, fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            exit_code = glxsh_uname(nodename=True)
            self.assertEqual(0, exit_code)
            self.assertEqual("%s\n" % os.uname().nodename, fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            exit_code = glxsh_uname(release=True)
            self.assertEqual(0, exit_code)
            self.assertEqual("%s\n" % os.uname().release, fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            exit_code = glxsh_uname(version=True)
            self.assertEqual(0, exit_code)
            self.assertEqual("%s\n" % os.uname().version, fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            exit_code = glxsh_uname(machine=True)
            self.assertEqual(0, exit_code)
            self.assertEqual("%s\n" % os.uname().machine, fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            exit_code = glxsh_uname(all=True)
            self.assertEqual(0, exit_code)
            self.assertEqual(
                "%s %s %s %s %s\n"
                % (
                    os.uname().sysname,
                    os.uname().nodename,
                    os.uname().release,
                    os.uname().version,
                    os.uname().machine,
                ),
                fake_out.getvalue(),
            )


if __name__ == "__main__":
    unittest.main()
