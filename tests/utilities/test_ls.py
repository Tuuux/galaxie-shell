import unittest
from io import StringIO
from unittest.mock import patch
from glxshell.utilities.ls import glxsh_ls
from glxshell.lib.ush import GLXUsh


class TestLs(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79
        self.utility = glxsh_ls()
        self.maxDiff = None

    def test_ls(self):
        pass

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  ls - list directory contents

SYNOPSIS
  ls [-ikqrs] [-glno] [-A|-a] [-C|-m|-x|-1] [-F|-p] [-H|-L] [-R|-d] [-S|-f|-t]
  [-c|-u] [file...]

DESCRIPTION
  List information about the files (the current directory by default).
  Sort
  entries alphabetically if none of -cftuvSUX is specified.

OPERANDS
  file  A pathname of a file to be written. If the file specified is not found,
        a diagnostic message shall be output on standard error.

OPTIONS
  -A  Do not list implied . and ..
  -C  List entries by columns
  -F  Append indicator (one of */=>@|) to entries
  -H  Follow symbolic links listed on the command line
  -L  When showing file information for a symbolic link, show information for
      the file the link references rather than for the link itself
  -R  List subdirectories recursively
  -S  Sort by file size, largest first
  -a  Do not ignore entries starting with .
  -c  With -lt: sort by, and show, ctime (time of last modification of file
      status information); otherwise: sort by ctime, newest first list entries
      by columns
  -d  List directories themselves, not their contents
  -f  Do not sort, enable -aU, disable -ls --color
  -g  Group directories before files;
  -i  Print the index number of each file
  -k  Default to 1024-byte blocks for disk usage; used only with -s and per
      directory totals
  -l  Use a long listing format
  -m  Fill width with a comma separated list of entries
  -n  Like -l, but list numeric user and group IDs
  -o  Like -l, but do not list group information
  -p  Append / indicator to directories
  -q  Enclose entry names in double quotes
  -r  Reverse order while sorting
  -s  Print the allocated size of each file, in blocks
  -t  Sort by time, newest first
  -u  With -lt: sort by, and show, access time; with -l: show access time and
      sort by name; otherwise: sort by access time, newest first
  -x  List entries by lines instead of by columns
  -1  List one file per line.  Avoid ' ' with -q or -b
"""
            self.shell.onecmd("man ls")
            self.assertEqual(expected, fake_out.getvalue())

    def test_ls(self):
        self.utility = glxsh_ls(
                A=None
                )
