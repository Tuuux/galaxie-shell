import unittest
import os
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.head import glxsh_head


class TestUtilityHead(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79
        self.source_file = os.path.normpath(
            os.path.join(os.path.dirname(os.path.abspath(__file__)), "../utilities/data", "head.txt")
        )

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  head - copy the first part of

SYNOPSIS
  head [-n] [file...]

DESCRIPTION
  The head utility shall copy its input files to the standard output, ending the
  output for each file at a designated point.

OPERANDS
  file  A pathname of an input file. If no file operands are specified, the
        standard input shall be used.

OPTIONS
  -n  The first number lines of each input file shall be copied to standard
      output.
"""
            self.shell.onecmd("man head")

            self.assertEqual(expected, fake_out.getvalue())

    def test_head(self):
        expected = """Hello.42
Hello.42
Hello.42
Hello.42
"""
        with patch("sys.stdout", new=StringIO()) as fake_out:
            glxsh_head(files=[self.source_file], number=4)
            self.assertEqual(
                expected,
                fake_out.getvalue(),
            )

    def test_over_glxush(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """Hello.42
Hello.42
Hello.42
Hello.42
"""
            GLXUsh().onecmd("head -n 4 %s" % self.source_file)

            self.assertEqual(expected, fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """Usage: head [-n] file
"""
            # with patch("sys.stdin", new=StringIO()) as fake_in:
            #
            #     GLXUsh().onecmd("head")

                # self.assertEqual(expected, fake_out.getvalue())


if __name__ == "__main__":
    unittest.main()
