import unittest

from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.false import glxsh_false


class TestUtilitiesTrue(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  false - return false value

SYNOPSIS
  false

DESCRIPTION
  The false utility shall return with a non-zero exit code.

EXIT STATUS
  1
"""
            self.shell.onecmd("man false")
            self.assertEqual(expected, fake_out.getvalue())

    def test_over_glxush(self):
        self.shell.exit_code = 42
        self.assertEqual(42, self.shell.exit_code)
        self.shell.onecmd("false")

        self.assertEqual(1, self.shell.exit_code)


if __name__ == "__main__":
    unittest.main()
