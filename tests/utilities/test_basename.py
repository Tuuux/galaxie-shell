import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.basename import glxsh_basename


class TestBaseName(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  basename - return non-directory portion of a pathname

SYNOPSIS
  basename [string] [suffix]

DESCRIPTION
  Print string with any leading directory components removed. If specified, also
  remove a trailing suffix.

OPERANDS
  string  A string
  suffix  A string
"""
            self.shell.onecmd("man basename")

            self.assertEqual(expected, fake_out.getvalue())

    def test_glxsh_basename(self):
        # Test form https://pubs.opengroup.org/onlinepubs/9699919799/functions/basename.html#tag_16_32

        def test_basename_over_shell(self):
            with patch("sys.stdout", new=StringIO()) as fake_out:
                expected = """usr
usr
.
/
//
/
usr
lib
lib
test
test
"""
                self.assertEqual(0, glxsh_basename("usr"))
                self.assertEqual(0, glxsh_basename("usr/"))
                self.assertEqual(0, glxsh_basename(""))
                self.assertEqual(0, glxsh_basename("/"))
                self.assertEqual(0, glxsh_basename("//"))
                self.assertEqual(0, glxsh_basename("///"))
                self.assertEqual(0, glxsh_basename("/usr/"))
                self.assertEqual(0, glxsh_basename("/usr/lib"))
                self.assertEqual(0, glxsh_basename("//usr//lib//"))
                self.assertEqual(0, glxsh_basename("/home//dwc//test"))
                self.assertEqual(0, glxsh_basename("/home//dwc//test.txt .txt"))

                self.assertEqual(expected, fake_out.getvalue())

    def test_basename_over_shell(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """usr
usr
.
/
/
/
usr
lib
lib
test
test
"""
            GLXUsh().onecmd("basename usr")
            GLXUsh().onecmd("basename usr/")
            GLXUsh().onecmd("basename")
            GLXUsh().onecmd("basename /")
            GLXUsh().onecmd("basename //")
            GLXUsh().onecmd("basename ///")
            GLXUsh().onecmd("basename /usr/")
            GLXUsh().onecmd("basename /usr/lib")
            GLXUsh().onecmd("basename //usr//lib//")
            GLXUsh().onecmd("basename /home//dwc//test")
            GLXUsh().onecmd("basename /home//dwc//test.txt .txt")

            self.assertEqual(expected, fake_out.getvalue())


if __name__ == "__main__":
    unittest.main()
