import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh


class TestUtilitiesTrue(unittest.TestCase):

    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  true - return true value

SYNOPSIS
  true

DESCRIPTION
  The true utility shall return with exit code zero.

EXIT STATUS
  0
"""
            self.shell.onecmd("man true")
            self.assertEqual(expected, fake_out.getvalue())

    def test_over_glxush(self):
        self.shell.exit_code = 42
        self.assertEqual(42, self.shell.exit_code)
        self.shell.onecmd("true")

        self.assertEqual(0, self.shell.exit_code)


if __name__ == "__main__":
    unittest.main()
