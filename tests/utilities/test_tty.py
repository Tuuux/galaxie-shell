import os
import sys
import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.tty import glxsh_tty


class TestUtilityTty(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  tty - return user's terminal name

SYNOPSIS
  tty

DESCRIPTION
  The tty utility shall write to the standard output the name of the terminal
  that is open as standard input.
"""
            self.shell.onecmd("man tty")
            self.assertEqual(expected, fake_out.getvalue())

    # def test_glxc_touch(self):
    #     with patch("sys.stdout", new=StringIO()) as fake_out:
    #         glxsh_tty()
    #         master, slave = os.openpty()
    #         if os.isatty(master):
    #         #     self.assertTrue("not a tty" not in fake_out.getvalue())
    #         else:
    #             self.assertEqual("not a tty\n", fake_out.getvalue())


if __name__ == "__main__":
    unittest.main()
