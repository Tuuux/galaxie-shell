import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh

class TestUtilitiesEcho(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  echo - write arguments to standard output

SYNOPSIS
  echo [-n] [string...]

DESCRIPTION
  The echo utility writes its arguments to standard output, followed by a
  <newline>. If there are no arguments, only the <newline> is written.

OPERANDS
  string  A string to be written to standard outputself.

OPTIONS
  -n  Suppress the <newline> that would otherwise follow the final argument in
      the output.
"""
            self.shell.onecmd("man echo")
            self.assertEqual(expected, fake_out.getvalue())

    # def test_glxsh_echo(self):
    #     pass


if __name__ == "__main__":
    unittest.main()
