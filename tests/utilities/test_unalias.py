import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.unalias import glxsh_unalias


class TestUnAlias(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_help(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  unalias - remove alias definitions

SYNOPSIS
  unalias alias-name...

DESCRIPTION
  The unalias utility shall remove the definition for each alias name specified.

OPERANDS
  alias-name  The name of an alias to be removed.

OPTIONS
  -a  Remove all alias definitions from the current shell execution
      environment.

EXIT STATUS
  0   Successful completion.
  >0  One of the alias-name operands specified did not represent a valid alias
      definition, or an error occurred.
"""
            self.shell.onecmdhooks("man unalias")
            self.assertEqual(expected, fake_out.getvalue())

    def test_glxsh_unalias_exit_code(self):
        exit_code = glxsh_unalias()
        self.assertEqual(1, exit_code)

    def test_glxsh_unalias(self):
        self.shell.alias = None
        self.shell.alias["ll"] = "ls -la"
        self.assertEqual(self.shell.alias, {"ll": "ls -la"})
        exit_code = self.shell.onecmdhooks("unalias ll")
        self.assertEqual(self.shell.alias, {})
        self.assertEqual(0, exit_code)
        exit_code = self.shell.onecmdhooks("unalias ll")
        self.assertEqual(1, exit_code)

    def test_glxsh_unalias_a(self):
        self.shell.alias = None
        self.shell.alias["ll"] = "ls -la"
        exit_code = self.shell.onecmdhooks("unalias -a")
        self.assertEqual(self.shell.alias, {})
        self.assertEqual(0, exit_code)


if __name__ == "__main__":
    unittest.main()
