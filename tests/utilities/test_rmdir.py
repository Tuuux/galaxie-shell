import unittest
import os
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.rmdir import glxsh_rmdir


class TestUtilityRmdir(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79
        self.temporary_directory_name_1 = "tests_rmdir"
        self.temporary_directory_name_2 = os.path.join(self.temporary_directory_name_1, "42")

        if os.path.exists(self.temporary_directory_name_2):
            if os.path.isdir(self.temporary_directory_name_2):
                os.rmdir(self.temporary_directory_name_2)
            if os.path.isfile(self.temporary_directory_name_2) or os.path.islink(self.temporary_directory_name_2):
                os.remove(self.temporary_directory_name_2)

        if os.path.exists(self.temporary_directory_name_1):
            if os.path.isdir(self.temporary_directory_name_1):
                os.rmdir(self.temporary_directory_name_1)
            if os.path.isfile(self.temporary_directory_name_1) or os.path.islink(self.temporary_directory_name_1):
                os.remove(self.temporary_directory_name_1)

    def tearDown(self):
        if os.path.exists(self.temporary_directory_name_2):
            if os.path.isdir(self.temporary_directory_name_2):
                os.rmdir(self.temporary_directory_name_2)
            if os.path.isfile(self.temporary_directory_name_2) or os.path.islink(self.temporary_directory_name_2):
                os.remove(self.temporary_directory_name_2)

        if os.path.exists(self.temporary_directory_name_1):
            if os.path.isdir(self.temporary_directory_name_1):
                os.rmdir(self.temporary_directory_name_1)
            if os.path.isfile(self.temporary_directory_name_1) or os.path.islink(self.temporary_directory_name_1):
                os.remove(self.temporary_directory_name_1)

    def test_man(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  rmdir - remove directories

SYNOPSIS
  rmdir [-p] [dir...]

DESCRIPTION
  The rmdir utility shall remove the directory entry specified by each dir
  operand.

  Directories shall be processed in the order specified. If a directory
  and a subdirectory of that directory are specified in a single invocation of
  the rmdir utility, the application shall specify the subdirectory before the
  parent directory so that the parent directory will be empty when the rmdir
  utility tries to remove it.

OPERANDS
  dir  A pathname of an empty directory to be removed.

OPTIONS
  -p  Remove all directories in a pathname.
"""
            self.shell.onecmd("man rmdir")
            self.assertEqual(expected, fake_out.getvalue())

        if not os.path.exists(self.temporary_directory_name_1):
            os.mkdir(self.temporary_directory_name_1)
        if not os.path.exists(self.temporary_directory_name_2):
            os.mkdir(self.temporary_directory_name_2)

        self.assertTrue(os.path.isdir(self.temporary_directory_name_1))
        self.assertTrue(os.path.isdir(self.temporary_directory_name_2))

        with patch("sys.stderr", new=StringIO()) as fake_out:
            self.shell.onecmd("rmdir %s" % self.temporary_directory_name_1)
            self.assertEqual(
                "rmdir: Directory not empty: '%s'\n" % self.temporary_directory_name_1, fake_out.getvalue()
            )

        self.assertTrue(os.path.isdir(self.temporary_directory_name_1))
        self.assertTrue(os.path.isdir(self.temporary_directory_name_2))

        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.shell.onecmd("rmdir -p %s" % self.temporary_directory_name_1)
            expected = ""
            self.assertEqual(expected, fake_out.getvalue())

        self.assertFalse(os.path.isdir(self.temporary_directory_name_1))
        self.assertFalse(os.path.isdir(self.temporary_directory_name_2))

    def test_rmdir(self):

        if not os.path.exists(self.temporary_directory_name_1):
            os.mkdir(self.temporary_directory_name_1)
        if not os.path.exists(self.temporary_directory_name_2):
            os.mkdir(self.temporary_directory_name_2)

        self.assertTrue(os.path.isdir(self.temporary_directory_name_1))
        self.assertTrue(os.path.isdir(self.temporary_directory_name_2))

        with patch("sys.stderr", new=StringIO()) as fake_out:
            glxsh_rmdir(
                directories=[self.temporary_directory_name_1],
                parents=False,
            )
            self.assertEqual(
                "rmdir: Directory not empty: '%s'\n" % self.temporary_directory_name_1, fake_out.getvalue()
            )

        self.assertTrue(os.path.isdir(self.temporary_directory_name_1))
        self.assertTrue(os.path.isdir(self.temporary_directory_name_2))

        # Test with a link inside directory
        # Open a file
        path = "%s_hello" % self.temporary_directory_name_2
        fd = os.open(path, os.O_RDWR | os.O_CREAT)
        # Close opened file
        os.close(fd)
        # Now create another copy of the above file.
        dst = "%s_42" % self.temporary_directory_name_2
        os.link(path, dst)
        # remove source file then the link is still here but dead
        os.remove(path)

        self.assertEqual(
            0,
            glxsh_rmdir(
                directories=[self.temporary_directory_name_1],
                parents=True,
            ),
        )

        self.assertFalse(os.path.isdir(self.temporary_directory_name_1))
        self.assertFalse(os.path.isdir(self.temporary_directory_name_2))

        with patch("sys.stderr", new=StringIO()) as fake_out:
            returned_value = glxsh_rmdir(
                directories=[self.temporary_directory_name_1],
                parents=False,
            )
            self.assertEqual(1, returned_value)
            self.assertEqual(
                "rmdir: No such file or directory: '%s'\n" % self.temporary_directory_name_1, fake_out.getvalue()
            )

        with patch("sys.stderr", new=StringIO()) as fake_out:
            returned_value = glxsh_rmdir(
                directories=[self.temporary_directory_name_1],
                parents=True,
            )
            self.assertEqual(1, returned_value)
            self.assertEqual(
                "rmdir: No such file or directory: '%s'\n" % self.temporary_directory_name_1, fake_out.getvalue()
            )

        self.assertFalse(os.path.isdir(self.temporary_directory_name_1))
        self.assertFalse(os.path.isdir(self.temporary_directory_name_2))

        with patch("sys.stderr", new=StringIO()) as fake_out:
            glxsh_rmdir(
                directories=[self.temporary_directory_name_1],
                parents=True,
            )
            self.assertEqual(
                "rmdir: No such file or directory: '%s'\n" % self.temporary_directory_name_1, fake_out.getvalue()
            )


if __name__ == "__main__":
    unittest.main()
