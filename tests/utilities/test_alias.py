import unittest
import os
from io import StringIO
from unittest.mock import patch
from glxshell.lib.ush import GLXUsh
from glxshell.utilities.alias import glxsh_alias


class TestAlias(unittest.TestCase):

    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_help(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  alias - define or display aliases

SYNOPSIS
  alias [alias-name[=string]...]

DESCRIPTION
  The alias utility shall create or redefine alias definitions or write the
  values of existing alias definitions to standard output. An alias definition
  provides a string value that shall replace a command name when it is
  encountered

OPERANDS
  alias-name         Write the alias definition to standard output.
  alias-name=string  Assign the value of string to the alias alias-name.

EXIT STATUS
  0   Successful completion.
  >0  One of the name operands specified did not have an alias definition, or an
      error occurred.
"""
            self.shell.onecmdhooks("man alias")
            self.assertEqual(expected, fake_out.getvalue())

    def test_glxsh_alias_exit_code(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.assertEqual(1, glxsh_alias())
            self.assertEqual(0, glxsh_alias(shell=GLXUsh()))

    def test_glxsh_alias(self):
        shell = GLXUsh()
        shell.alias = None
        self.assertEqual(shell.alias, {})

        exit_code = shell.onecmdhooks('alias ll="ls -la"')
        self.assertEqual(shell.alias["ll"], "ls -la")
        self.assertEqual(0, exit_code)
        expected = """ll='ls -la'
"""
        with patch("sys.stdout", new=StringIO()) as fake_out:
            shell.onecmdhooks("alias")
            self.assertEqual(expected, fake_out.getvalue())
        with patch("sys.stdout", new=StringIO()) as fake_out:
            shell.onecmdhooks("alias ll")
            self.assertEqual(expected, fake_out.getvalue())






if __name__ == "__main__":
    unittest.main()
