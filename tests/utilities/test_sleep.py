import unittest
from io import StringIO
from unittest.mock import patch

from glxshell.lib.ush import GLXUsh
from glxshell.utilities.sleep import glxsh_sleep


class TestUtilitySleep(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXUsh()
        self.shell.environ["COLUMNS"] = 79

    def test_over_glxush(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = """NAME
  sleep - suspend execution for an interval

SYNOPSIS
  sleep time

DESCRIPTION
  The sleep utility shall suspend execution for at least the integral number of
  seconds specified by the time operand.

OPERANDS
  time  A non-negative decimal integer or float specifying the number of
        seconds for which to suspend execution.
"""
            self.shell.onecmd("man sleep")

            self.assertEqual(expected, fake_out.getvalue())

    def test_glxsh_sleep(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            returned_value = glxsh_sleep(1)
            self.assertEqual(0, returned_value)

            self.assertEqual("", fake_out.getvalue())

        with patch("sys.stderr", new=StringIO()) as fake_out:
            returned_value = glxsh_sleep("Hello.42")
            self.assertEqual(1, returned_value)

            possible_value = [
                "sleep: an integer is required (got type str)\n",
                "sleep: 'str' object cannot be interpreted as an integer\n"
            ]
            self.assertTrue(fake_out.getvalue() in possible_value)


if __name__ == "__main__":
    unittest.main()
