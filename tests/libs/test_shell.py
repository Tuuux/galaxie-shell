import unittest
import cmd2

from GLXShell.libs.shell import GLXShell
from GLXShell.libs.properties.shortcuts import GLXShPropertyShortcuts
from GLXShell.libs.properties.history import GLXShPropertyHistory
from GLXShell.libs.properties.config import GLXShPropertyConfig
from GLXShell.libs.properties.shell import GLXShPropertyShell
from GLXShell.libs.plugins import GLXShPluginsManager
from GLXShell.libs.intro import GLXShIntro
from GLXShell.libs.prompt import GLXShPrompt
from GLXShell.libs.settable import GLXShSettable


class TestGLXShell(unittest.TestCase):
    def setUp(self) -> None:
        self.shell = GLXShell(debug=True)

    def test_isinstance(self):
        self.assertTrue(isinstance(self.shell, GLXShPropertyShortcuts))
        self.assertTrue(isinstance(self.shell, GLXShPropertyHistory))
        self.assertTrue(isinstance(self.shell, GLXShPropertyConfig))
        self.assertTrue(isinstance(self.shell, GLXShPropertyShell))
        self.assertTrue(isinstance(self.shell, GLXShIntro))
        self.assertTrue(isinstance(self.shell, GLXShPrompt))
        self.assertTrue(isinstance(self.shell, cmd2.Cmd))
        self.assertTrue(isinstance(self.shell, GLXShSettable))
        self.assertTrue(isinstance(self.shell, GLXShPluginsManager))


if __name__ == "__main__":
    unittest.main()
