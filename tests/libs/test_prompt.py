import getpass
import socket
import unittest
import os

import GLXShell
from GLXShell.libs.prompt import GLXShPrompt
from GLXShell.libs.config import GLXShConfig


class TestGLXShPrompt(unittest.TestCase):
    def setUp(self) -> None:
        if os.path.isfile(os.path.join(os.getcwd(), "test_prompt.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_prompt.tmp"))

        self.config = GLXShConfig()
        self.config.file = os.path.join(os.getcwd(), "test_prompt.tmp")
        self.config.data = None
        self.config.write_config()
        self.prompt = GLXShPrompt(config=self.config)

    def tearDown(self):
        if os.path.isfile(os.path.join(os.getcwd(), "test_prompt.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_prompt.tmp"))

    def test_prompt_show_info(self):

        self.prompt.onchange_prompt_show_info(
            param_name=None, old_value=None, new_value=None
        )

        self.assertFalse(self.prompt.prompt_show_info)
        self.assertFalse(self.config.data["prompt"]["show"]["info"])
        self.prompt.prompt_show_info = True
        self.assertTrue(self.prompt.prompt_show_info)
        self.assertTrue(self.config.data["prompt"]["show"]["info"])
        self.prompt.prompt_show_info = None
        self.assertFalse(self.prompt.prompt_show_info)
        self.assertFalse(self.config.data["prompt"]["show"]["info"])

        self.assertRaises(TypeError, setattr, self.prompt, "prompt_show_info", 42)

    def test_prompt_show_cursor(self):
        self.prompt.onchange_prompt_show_cursor(
            param_name=None, old_value=None, new_value=None
        )

        self.assertTrue(self.prompt.prompt_show_cursor)
        self.assertTrue(self.config.data["prompt"]["show"]["cursor"])
        self.prompt.prompt_show_cursor = False
        self.assertFalse(self.prompt.prompt_show_cursor)
        self.assertFalse(self.config.data["prompt"]["show"]["cursor"])
        self.prompt.prompt_show_cursor = None
        self.assertTrue(self.prompt.prompt_show_cursor)
        self.assertTrue(self.config.data["prompt"]["show"]["cursor"])

        self.assertRaises(TypeError, setattr, self.prompt, "prompt_show_cursor", 42)

    def test_prompt_env_is_virtual(self):
        if os.environ.get("VIRTUAL_ENV"):
            old_var = os.environ.get("VIRTUAL_ENV")
        else:
            old_var = None

        if os.environ.get("VIRTUAL_ENV"):
            del os.environ["VIRTUAL_ENV"]
        self.assertFalse(self.prompt.prompt_env_is_virtual)

        os.environ["VIRTUAL_ENV"] = "Hello.42"
        self.assertTrue(self.prompt.prompt_env_is_virtual)
        del os.environ["VIRTUAL_ENV"]

        if old_var:
            os.environ["VIRTUAL_ENV"] = old_var

    def test_prompt_env_text(self):
        if os.environ.get("VIRTUAL_ENV"):
            old_virtual_env_var = os.environ.get("VIRTUAL_ENV")
        else:
            old_virtual_env_var = None

        if os.environ.get("VIRTUAL_ENV"):
            del os.environ["VIRTUAL_ENV"]
        self.assertEqual("", self.prompt.prompt_env_name_text)

        os.environ["VIRTUAL_ENV"] = "Hello.42"
        self.assertEqual("(Hello.42) ", self.prompt.prompt_env_name_text)
        del os.environ["VIRTUAL_ENV"]

        if os.environ.get("CONDA_DEFAULT_ENV"):
            old_conda_default_env_var = os.environ.get("CONDA_DEFAULT_ENV")
        else:
            old_conda_default_env_var = None

        if os.environ.get("CONDA_DEFAULT_ENV"):
            del os.environ["CONDA_DEFAULT_ENV"]
        self.assertEqual("", self.prompt.prompt_env_name_text)

        os.environ["CONDA_DEFAULT_ENV"] = "Hello.42"
        self.assertEqual("(Hello.42) ", self.prompt.prompt_env_name_text)
        del os.environ["CONDA_DEFAULT_ENV"]

        if old_conda_default_env_var:
            os.environ["CONDA_DEFAULT_ENV"] = old_conda_default_env_var
        if old_virtual_env_var:
            os.environ["VIRTUAL_ENV"] = old_virtual_env_var

    def test_prompt_username_text(self):
        self.assertEqual(getpass.getuser(), self.prompt.prompt_username_text)

    def test_prompt_hostname_text(self):
        self.assertEqual(socket.gethostname(), self.prompt.prompt_hostname_text)

    def test_prompt_path_text(self):
        self.assertEqual(
            os.getcwd().replace(os.path.realpath(os.path.expanduser("~")), "~"),
            self.prompt.prompt_path_text,
        )

    def test_prompt_cursor_text(self):
        self.assertEqual(">", self.prompt.prompt_cursor_text)

    def test_prompt_to_display(self):
        self.prompt.prompt_show_info = True
        self.prompt.prompt_show_cursor = True

        self.assertEqual(2, len(self.prompt.prompt_to_display.split("\n")))
        # self.assertEqual(
        #     "{venv}{username}@{hostname}:{path} {symbol} ".format(
        #         venv=self.prompt.prompt_env_name_text,
        #         username=self.prompt.prompt_username_text,
        #         hostname=self.prompt.prompt_hostname_text,
        #         path=self.prompt.prompt_path_text,
        #         symbol=self.prompt.prompt_symbol_text,
        #     ),
        #     self.prompt.prompt_to_display.split("\n")[0],
        # )
        self.assertEqual("> ", self.prompt.prompt_to_display.split("\n")[1])

        self.prompt.prompt_show_info = False
        self.prompt.prompt_show_cursor = True
        self.assertEqual(1, len(self.prompt.prompt_to_display.split("\n")))
        self.assertEqual("> ", self.prompt.prompt_to_display.split("\n")[0])

        self.prompt.prompt_show_info = False
        self.prompt.prompt_show_cursor = False
        self.assertEqual(1, len(self.prompt.prompt_to_display.split("\n")))
        self.assertEqual("", self.prompt.prompt_to_display.split("\n")[0])


if __name__ == "__main__":
    unittest.main()
