import unittest
import os
import tempfile
import json

from GLXShell.libs.config import GLXShConfig
from GLXShell.libs.xdg_base_directory import XDGBaseDirectory


class TestGLXShConfig(unittest.TestCase):
    def setUp(self) -> None:
        self.config = GLXShConfig()
        self.xdg_base_dir = XDGBaseDirectory()

    def test_file(self):
        self.assertEqual(
            os.path.join(self.xdg_base_dir.config_path, "config.json"),
            self.config.file,
        )

        self.assertRaises(TypeError, setattr, self.config, "file", 42)

    def test_data(self):
        self.config.data = None
        self.assertEqual({}, self.config.data)
        self.config.data["prompt_show_info"] = True
        self.config.data["prompt_show_cursor"] = True
        self.assertTrue(self.config.data["prompt_show_info"])
        self.assertTrue(self.config.data["prompt_show_cursor"])

        self.assertRaises(TypeError, setattr, self.config, "data", 42)

    def test_touch_config(self):
        f = tempfile.NamedTemporaryFile(delete=True)
        temporary_file_name = f.name
        f.close()
        self.config.file = temporary_file_name
        self.assertFalse(os.path.exists(self.config.file))
        self.config.touch_config()
        self.assertTrue(os.path.exists(self.config.file))

        with open(self.config.file, "r") as config_file:
            config_file_data = json.load(config_file)
        self.assertEqual({}, config_file_data)

        if os.path.exists(self.config.file):
            os.remove(self.config.file)

        # Test subdirectory creation use temporary_file_name as directory name
        self.config.file = os.path.join(temporary_file_name, "file")
        self.config.touch_config()
        self.assertTrue(os.path.isfile(self.config.file))
        self.assertTrue(os.path.isdir(temporary_file_name))

        if os.path.exists(self.config.file):
            os.remove(self.config.file)

        if os.path.exists(temporary_file_name):
            if os.path.isdir(temporary_file_name):
                os.rmdir(temporary_file_name)
            if os.path.isfile(temporary_file_name) or os.path.islink(
                temporary_file_name
            ):
                os.remove(temporary_file_name)

    def test_load_config(self):
        f = tempfile.NamedTemporaryFile(delete=True)
        temporary_file_name = f.name
        f.close()
        self.config.file = temporary_file_name
        self.config.touch_config()

        info = {"hello42": "42", "hello43": "43", "hello44": "44", "hello45": "45"}

        with open(self.config.file, "w") as config_file:
            json.dump(info, config_file)
        self.config.data = None

        self.config.load_config()

        self.assertEqual(info, self.config.data)

        if os.path.exists(self.config.file):
            os.remove(self.config.file)

    def test_write_config(self):
        f = tempfile.NamedTemporaryFile(delete=True)
        temporary_file_name = f.name
        f.close()
        self.config.file = temporary_file_name
        self.config.touch_config()

        self.config.data = {
            "hello42": "42",
            "hello43": "43",
            "hello44": "44",
            "hello45": "45",
        }

        self.config.write_config()

        with open(self.config.file, "r") as config_file:
            config_file_data = json.load(config_file)

        self.assertEqual(config_file_data, self.config.data)

        if os.path.exists(self.config.file):
            os.remove(self.config.file)


if __name__ == "__main__":
    unittest.main()
