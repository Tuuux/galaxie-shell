import unittest
import os

from GLXShell.libs.plugins import GLXShPluginsManager
from GLXShell.libs.shell import GLXShell
from GLXShell.libs.config import GLXShConfig
from GLXShell import APPLICATION_PLUGINS

from argparse import Namespace


class TestGLXShPluginsManager(unittest.TestCase):
    def setUp(self) -> None:
        if os.path.isfile(os.path.join(os.getcwd(), "test_plugins.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_plugins.tmp"))

        self.config = GLXShConfig()
        self.config.file = os.path.join(os.getcwd(), "test_plugins.tmp")
        self.config.data = None
        self.config.write_config()
        self.plugins_manager = GLXShPluginsManager()
        self.plugins_manager.shell = GLXShell()
        self.plugins_manager.shell.debug = True
        self.plugins_manager.config = self.config
        self.plugins_manager.plugins = APPLICATION_PLUGINS
        self.plugins_manager.plugins_manager_control_config()

    def tearDown(self):
        if os.path.isfile(os.path.join(os.getcwd(), "test_plugins.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_plugins.tmp"))

    def test_load_plugins(self):
        self.plugins_manager.load_plugins()

    def test_load(self):
        self.plugins_manager.load(Namespace(name="builtins"))

    def test_unload(self):
        self.plugins_manager.unload(Namespace(name="builtins"))

    def test_reload(self):
        self.plugins_manager.reload(Namespace(name="builtins"))

    def test_disable(self):
        self.plugins_manager.disable(Namespace(name="builtins"))

    def test_enable(self):

        for plugin in self.plugins_manager.plugins:
            self.plugins_manager.config.data["plugins"][plugin["name"]][
                "enabled"
            ] = False
        self.plugins_manager.enable(Namespace(name="builtins"))


if __name__ == "__main__":
    unittest.main()
