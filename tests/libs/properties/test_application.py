import unittest

from GLXShell.libs.properties.application import GLXShPropertyApplication
from GLXShell import APPLICATION_AUTHORS
from GLXShell import APPLICATION_DESCRIPTION
from GLXShell import APPLICATION_LICENSE
from GLXShell import APPLICATION_NAME
from GLXShell import APPLICATION_VERSION
from GLXShell import APPLICATION_WARRANTY


class TestGLXShPropertyApplication(unittest.TestCase):
    def setUp(self) -> None:
        self.application_property = GLXShPropertyApplication()

    def test_authors(self):
        self.application_property.authors = None
        self.assertEqual(APPLICATION_AUTHORS, self.application_property.authors)

        self.application_property.authors = ["Hello", "42"]
        self.assertEqual(["Hello", "42"], self.application_property.authors)

        self.assertRaises(TypeError, setattr, self.application_property, "authors", 42)

    def test_description(self):
        self.application_property.description = None
        self.assertEqual(APPLICATION_DESCRIPTION, self.application_property.description)

        self.application_property.description = "Hello.42"
        self.assertEqual("Hello.42", self.application_property.description)

        self.assertRaises(
            TypeError, setattr, self.application_property, "description", 42
        )

    def test_license(self):
        self.application_property.license = None
        self.assertEqual(APPLICATION_LICENSE, self.application_property.license)

        self.application_property.license = "Hello.42"
        self.assertEqual("Hello.42", self.application_property.license)

        self.assertRaises(TypeError, setattr, self.application_property, "license", 42)

    def test_name(self):
        self.application_property.name = None
        self.assertEqual(APPLICATION_NAME, self.application_property.name)

        self.application_property.name = "Hello.42"
        self.assertEqual("Hello.42", self.application_property.name)

        self.assertRaises(TypeError, setattr, self.application_property, "name", 42)

    def test_version(self):
        self.application_property.version = None
        self.assertEqual(APPLICATION_VERSION, self.application_property.version)

        self.application_property.version = "Hello.42"
        self.assertEqual("Hello.42", self.application_property.version)

        self.assertRaises(TypeError, setattr, self.application_property, "version", 42)

    def test_warranty(self):
        self.application_property.warranty = None
        self.assertEqual(APPLICATION_WARRANTY, self.application_property.warranty)

        self.application_property.warranty = "Hello.42"
        self.assertEqual("Hello.42", self.application_property.warranty)

        self.assertRaises(TypeError, setattr, self.application_property, "warranty", 42)


if __name__ == "__main__":
    unittest.main()
