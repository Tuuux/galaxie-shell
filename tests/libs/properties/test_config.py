import unittest
from GLXShell.libs.config import GLXShConfig
from GLXShell.libs.properties.config import GLXShPropertyConfig


class TestGLXShPropertyConfig(unittest.TestCase):
    def setUp(self) -> None:
        self.config_property = GLXShPropertyConfig()
        self.config = GLXShConfig()

    def test_config_property(self):
        self.config_property.config = None
        self.assertTrue(isinstance(self.config_property.config, GLXShConfig))

        self.config_property.config = self.config
        self.assertTrue(isinstance(self.config_property.config, GLXShConfig))
        self.assertEqual(self.config, self.config_property.config)

        self.assertRaises(TypeError, setattr, self.config_property, "config", 42)


if __name__ == "__main__":
    unittest.main()
