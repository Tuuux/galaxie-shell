import unittest
import os

from GLXShell.libs.properties.history import GLXShPropertyHistory
from GLXShell.libs.xdg_base_directory import XDGBaseDirectory


class TestGLXShPropertyHistory(unittest.TestCase):
    def setUp(self) -> None:
        self.history = GLXShPropertyHistory()
        self.xdg_base_dir = XDGBaseDirectory()

    def test_persistent_history_file(self):
        self.assertEqual(
            os.path.join(self.xdg_base_dir.config_path, "history"),
            self.history.persistent_history_file,
        )

    def test_persistent_history_length(self):
        self.assertEqual(500, self.history.persistent_history_length)
        self.history.persistent_history_length = 1000
        self.assertEqual(1000, self.history.persistent_history_length)
        self.history.persistent_history_length = None
        self.assertEqual(500, self.history.persistent_history_length)

        self.assertRaises(
            TypeError, setattr, self.history, "persistent_history_length", "Hello.42"
        )
        self.assertRaises(
            ValueError, setattr, self.history, "persistent_history_length", -1
        )


if __name__ == "__main__":
    unittest.main()
