import unittest
import cmd2

from GLXShell.libs.properties.shortcuts import GLXShPropertyShortcuts


class TestGLXShPropertyShortcuts(unittest.TestCase):
    def setUp(self) -> None:
        self.shortcuts = GLXShPropertyShortcuts()

    def test_shortcuts(self):
        self.assertEqual(cmd2.DEFAULT_SHORTCUTS, self.shortcuts.shortcuts)

        self.shortcuts.shortcuts = {"Hello": 42}
        self.assertEqual({"Hello": 42}, self.shortcuts.shortcuts)

        self.shortcuts.shortcuts = None
        self.assertEqual({}, self.shortcuts.shortcuts)

        self.assertRaises(TypeError, setattr, self.shortcuts, "shortcuts", "Hello.42")


if __name__ == "__main__":
    unittest.main()
