import unittest

from GLXShell.libs.properties.commands import GLXShellPropertyCommands


class TestGLXShellPropertyCommands(unittest.TestCase):
    def setUp(self) -> None:
        self.commands_property = GLXShellPropertyCommands()
        self.commands = [
            {"name": "commands", "object": GLXShellPropertyCommands()},
        ]

    def test_config_property(self):
        self.commands_property.commands = None
        self.assertTrue(isinstance(self.commands_property.commands, list))

        self.commands_property.commands = self.commands
        self.assertTrue(isinstance(self.commands_property.commands, list))
        self.assertEqual(self.commands, self.commands_property.commands)

        self.assertRaises(TypeError, setattr, self.commands_property, "commands", 42)


if __name__ == "__main__":
    unittest.main()
