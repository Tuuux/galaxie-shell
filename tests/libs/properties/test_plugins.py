import unittest

from GLXShell.libs.properties.plugins import GLXShPropertyPlugins


class TestGLXShellPropertyPlugins(unittest.TestCase):
    def setUp(self) -> None:
        self.plugins_property = GLXShPropertyPlugins()
        self.plugins = [
            {"name": "commands", "version": "42", "object": GLXShPropertyPlugins()},
        ]

    def test_config_property(self):
        self.plugins_property.plugins = None
        self.assertTrue(isinstance(self.plugins_property.plugins, list))

        self.plugins_property.plugins = self.plugins
        self.assertTrue(isinstance(self.plugins_property.plugins, list))
        self.assertEqual(self.plugins, self.plugins_property.plugins)

        self.assertRaises(TypeError, setattr, self.plugins_property, "plugins", 42)


if __name__ == "__main__":
    unittest.main()
