import unittest

from GLXShell.libs.commands import GLXShellCommands
from GLXShell.libs.shell import GLXShell
from GLXShell.plugins.builtins.arch import GLXArch


class TestGLXShellPlugin(unittest.TestCase):
    def setUp(self) -> None:
        self.plugin = GLXShellCommands()
        self.plugin.shell = GLXShell()
        self.plugin.commands = [
            {"name": "arch", "object": GLXArch()},
        ]
        for cmd in self.plugin.commands:
            self.plugin.shell.unregister_command_set(cmd["object"])
        self.plugin.shell.debug = True

    def test_load(self):
        self.plugin.load()

    def test_unload(self):
        self.plugin.unload()

    def test_reload(self):
        self.plugin.reload()


if __name__ == "__main__":
    unittest.main()
