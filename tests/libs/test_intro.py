import unittest
import os
import sys
import platform

from GLXShell.libs.intro import GLXShIntro
from GLXShell.libs.config import GLXShConfig
from GLXShell.libs.intro import get_size
from GLXShell.libs.intro import get_memory_available
from GLXShell.libs.intro import get_memory_total


class TestGLXShIntro(unittest.TestCase):
    def setUp(self) -> None:
        if os.path.isfile(os.path.join(os.getcwd(), "test_intro.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_intro.tmp"))

        self.config = GLXShConfig()
        self.config.file = os.path.join(os.getcwd(), "test_intro.tmp")
        self.config.data = None
        self.config.write_config()
        self.intro = GLXShIntro(config=self.config)

    def tearDown(self):
        if os.path.isfile(os.path.join(os.getcwd(), "test_intro.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_intro.tmp"))

    def test_intro_show_exec(self):
        self.assertTrue(self.intro.intro_show_exec)
        self.assertTrue(self.config.data["intro"]["show"]["exec"])
        self.intro.intro_show_exec = False
        self.assertFalse(self.intro.intro_show_exec)
        self.assertFalse(self.config.data["intro"]["show"]["exec"])
        self.intro.intro_show_exec = None
        self.assertTrue(self.intro.intro_show_exec)
        self.assertTrue(self.config.data["intro"]["show"]["exec"])

        self.assertRaises(TypeError, setattr, self.intro, "intro_show_exec", 42)

    def test_intro_show_holotape(self):
        self.assertTrue(self.intro.intro_show_holotape)
        self.assertTrue(self.config.data["intro"]["show"]["holotape"])
        self.intro.intro_show_holotape = False
        self.assertFalse(self.intro.intro_show_holotape)
        self.assertFalse(self.config.data["intro"]["show"]["holotape"])
        self.intro.intro_show_holotape = None
        self.assertTrue(self.intro.intro_show_holotape)
        self.assertTrue(self.config.data["intro"]["show"]["holotape"])

        self.assertRaises(TypeError, setattr, self.intro, "intro_show_holotape", 42)

    def test_intro_show_license(self):
        self.assertTrue(self.intro.intro_show_license)
        self.assertTrue(self.config.data["intro"]["show"]["license"])
        self.intro.intro_show_license = False
        self.assertFalse(self.intro.intro_show_license)
        self.assertFalse(self.config.data["intro"]["show"]["license"])
        self.intro.intro_show_license = None
        self.assertTrue(self.intro.intro_show_license)
        self.assertTrue(self.config.data["intro"]["show"]["license"])

        self.assertRaises(TypeError, setattr, self.intro, "intro_show_license", 42)

    def test_intro_show_loader(self):
        self.assertTrue(self.intro.intro_show_loader)
        self.assertTrue(self.config.data["intro"]["show"]["loader"])
        self.intro.intro_show_loader = False
        self.assertFalse(self.intro.intro_show_loader)
        self.assertFalse(self.config.data["intro"]["show"]["loader"])
        self.intro.intro_show_loader = None
        self.assertTrue(self.intro.intro_show_loader)
        self.assertTrue(self.config.data["intro"]["show"]["loader"])

        self.assertRaises(TypeError, setattr, self.intro, "intro_show_loader", 42)

    def test_intro_show_memory_free(self):
        self.assertTrue(self.intro.intro_show_memory_free)
        self.assertTrue(self.config.data["intro"]["show"]["memory_free"])
        self.intro.intro_show_memory_free = False
        self.assertFalse(self.intro.intro_show_memory_free)
        self.assertFalse(self.config.data["intro"]["show"]["memory_free"])
        self.intro.intro_show_memory_free = None
        self.assertTrue(self.intro.intro_show_memory_free)
        self.assertTrue(self.config.data["intro"]["show"]["memory_free"])

        self.assertRaises(TypeError, setattr, self.intro, "intro_show_memory_free", 42)

    def test_intro_show_memory_total(self):
        self.assertTrue(self.intro.intro_show_memory_total)
        self.assertTrue(self.config.data["intro"]["show"]["memory_total"])
        self.intro.intro_show_memory_total = False
        self.assertFalse(self.intro.intro_show_memory_total)
        self.assertFalse(self.config.data["intro"]["show"]["memory_total"])
        self.intro.intro_show_memory_total = None
        self.assertTrue(self.intro.intro_show_memory_total)
        self.assertTrue(self.config.data["intro"]["show"]["memory_total"])

        self.assertRaises(TypeError, setattr, self.intro, "intro_show_memory_total", 42)

    def test_intro_show_rom(self):
        self.assertTrue(self.intro.intro_show_rom)
        self.assertTrue(self.config.data["intro"]["show"]["rom"])
        self.intro.intro_show_rom = False
        self.assertFalse(self.intro.intro_show_rom)
        self.assertFalse(self.config.data["intro"]["show"]["rom"])
        self.intro.intro_show_rom = None
        self.assertTrue(self.intro.intro_show_rom)
        self.assertTrue(self.config.data["intro"]["show"]["rom"])

        self.assertRaises(TypeError, setattr, self.intro, "intro_show_rom", 42)

    def test_intro_show_spacing(self):
        self.assertTrue(self.intro.intro_show_spacing)
        self.assertTrue(self.config.data["intro"]["show"]["spacing"])
        self.intro.intro_show_spacing = False
        self.assertFalse(self.intro.intro_show_spacing)
        self.assertFalse(self.config.data["intro"]["show"]["spacing"])
        self.intro.intro_show_spacing = None
        self.assertTrue(self.intro.intro_show_spacing)
        self.assertTrue(self.config.data["intro"]["show"]["spacing"])

        self.assertRaises(TypeError, setattr, self.intro, "intro_show_spacing", 42)

    def test_intro_show_title(self):
        self.assertTrue(self.intro.intro_show_title)
        self.assertTrue(self.config.data["intro"]["show"]["title"])
        self.intro.intro_show_title = False
        self.assertFalse(self.intro.intro_show_title)
        self.assertFalse(self.config.data["intro"]["show"]["title"])
        self.intro.intro_show_title = None
        self.assertTrue(self.intro.intro_show_title)
        self.assertTrue(self.config.data["intro"]["show"]["title"])

        self.assertRaises(TypeError, setattr, self.intro, "intro_show_title", 42)

    def test_onchange_intro_show_exec(self):
        self.assertIsNone(self.intro.onchange_intro_show_exec(None, None, None))

    def test_onchange_intro_show_holotape(self):
        self.assertIsNone(self.intro.onchange_intro_show_holotape(None, None, None))

    def test_onchange_intro_show_license(self):
        self.assertIsNone(self.intro.onchange_intro_show_license(None, None, None))

    def test_onchange_intro_show_loader(self):
        self.assertIsNone(self.intro.onchange_intro_show_loader(None, None, None))

    def test_onchange_intro_show_memory_free(self):
        self.assertIsNone(self.intro.onchange_intro_show_memory_free(None, None, None))

    def test_onchange_intro_show_memory_total(self):
        self.assertIsNone(self.intro.onchange_intro_show_memory_total(None, None, None))

    def test_onchange_intro_show_rom(self):
        self.assertIsNone(self.intro.onchange_intro_show_rom(None, None, None))

    def test_onchange_intro_show_spacing(self):
        self.assertIsNone(self.intro.onchange_intro_show_spacing(None, None, None))

    def test_onchange_intro_show_title(self):
        self.assertIsNone(self.intro.onchange_intro_show_title(None, None, None))

    def test_intro_to_display(self):
        intro_to_display = "LOADER {platform_version}".format(
            platform_version=platform.version().upper()
        )
        self.assertEqual(intro_to_display, self.intro.intro_to_display.split("\n")[4])

    def test_intro_venv(self):
        if os.environ.get("VIRTUAL_ENV"):
            old_var_virtual_env = os.environ.get("VIRTUAL_ENV")
        else:
            old_var_virtual_env = None
        if os.environ.get("CONDA_DEFAULT_ENV"):
            old_var_conda_default_env = os.environ.get("CONDA_DEFAULT_ENV")
        else:
            old_var_conda_default_env = None

        # TEST VENV
        if os.environ.get("CONDA_DEFAULT_ENV"):
            del os.environ["CONDA_DEFAULT_ENV"]

        os.environ["VIRTUAL_ENV"] = os.path.abspath(os.path.join(os.getcwd(), "venv"))
        exec_venv = "({env_name}) ".format(
            env_name=os.path.basename(
                os.path.abspath(os.path.join(os.getcwd(), "venv"))
            )
        ).upper()

        intro_to_display = "EXEC {exec_venv}PYTHON {python_major}.{python_minor}.{python_micro}".format(
            exec_venv=exec_venv,
            python_major=sys.version_info.major,
            python_minor=sys.version_info.minor,
            python_micro=sys.version_info.micro,
            platform_version=platform.version().upper(),
        )
        self.assertEqual(intro_to_display, self.intro.intro_to_display.split("\n")[5])

        # TEST CONDA
        if os.environ.get("VIRTUAL_ENV"):
            del os.environ["VIRTUAL_ENV"]
        os.environ["CONDA_DEFAULT_ENV"] = os.path.abspath(
            os.path.join(os.getcwd(), "venv")
        )
        exec_venv = "({env_name}) ".format(
            env_name=os.path.basename(
                os.path.abspath(os.path.join(os.getcwd(), "venv"))
            )
        ).upper()
        intro_to_display = "EXEC {exec_venv}PYTHON {python_major}.{python_minor}.{python_micro}".format(
            exec_venv=exec_venv,
            python_major=sys.version_info.major,
            python_minor=sys.version_info.minor,
            python_micro=sys.version_info.micro,
            platform_version=platform.version().upper(),
        )
        self.assertEqual(intro_to_display, self.intro.intro_to_display.split("\n")[5])

        # CLEAN UP
        if os.environ.get("VIRTUAL_ENV"):
            del os.environ["VIRTUAL_ENV"]
        if os.environ.get("CONDA_DEFAULT_ENV"):
            del os.environ["CONDA_DEFAULT_ENV"]
        intro_to_display = (
            "EXEC PYTHON {python_major}.{python_minor}.{python_micro}".format(
                python_major=sys.version_info.major,
                python_minor=sys.version_info.minor,
                python_micro=sys.version_info.micro,
                platform_version=platform.version().upper(),
            )
        )
        self.assertEqual(intro_to_display, self.intro.intro_to_display.split("\n")[5])

        # Restore variable in case
        if old_var_virtual_env:
            os.environ["VIRTUAL_ENV"] = old_var_virtual_env

        if old_var_conda_default_env:
            os.environ["CONDA_DEFAULT_ENV"] = old_var_conda_default_env

    def test_get_size(self):
        self.assertEqual("9.77KB", get_size(size=10000, suffix="B"))
        self.assertEqual("953.67MB", get_size(size=1000000000, suffix="B"))
        self.assertEqual("93.13GB", get_size(size=100000000000, suffix="B"))
        self.assertEqual("9.09TB", get_size(size=10000000000000, suffix="B"))

    def test_get_memory_available(self):
        self.assertEqual(
            os.sysconf("SC_PAGE_SIZE") * os.sysconf("SC_AVPHYS_PAGES"),
            get_memory_available(),
        )

    def test_get_memory_total(self):
        self.assertEqual(
            os.sysconf("SC_PAGE_SIZE") * os.sysconf("SC_PHYS_PAGES"), get_memory_total()
        )


if __name__ == "__main__":
    unittest.main()
