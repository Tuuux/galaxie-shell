import unittest
from glxshell.lib.symbolic_mode import symbolic_mode


class TestSynbolicMode(unittest.TestCase):
    def setUp(self) -> None:
        self.function = ""

    def test_symbolic_mode(self):
        self.assertEqual(symbolic_mode("u+w", mode=0o444), 0o644)
        self.assertEqual(symbolic_mode("=x", mode=0o444, umask=0o700), 0o411)
        self.assertEqual(symbolic_mode("og-rxw", mode=0o777),0o700)
        self.assertEqual(symbolic_mode("o-rxw,g-rxw", mode=0o777),0o700)
        self.assertEqual(symbolic_mode("a=rx,u+w"), 0o755)
        self.assertEqual(symbolic_mode("og+X", mode=0o644, isdir=0), 0o644)
        self.assertEqual(symbolic_mode("og+X", mode=0o644, isdir=1), 0o655)
        
        # self.assertEqual(symbolic_mode("", umask=0o022), 0o655)
