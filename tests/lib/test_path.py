import os
import unittest
import sys

from glxshell.lib.os.path.path import normcase
from glxshell.lib.os.path.path import normpath
from glxshell.lib.os.path.path import abspath
from glxshell.lib.os.path.path import join as joinpath
from glxshell.lib.os.path.path import split
from glxshell.lib.os.path.path import splitext
from glxshell.lib.os.path.path import splitdrive
from glxshell.lib.os.path.path import dirname
from glxshell.lib.os.path.path import basename
from glxshell.lib.os.path.path import exists
from glxshell.lib.os.path.path import lexists
from glxshell.lib.os.path.path import isfile
from glxshell.lib.os.path.path import isdir
from glxshell.lib.os.path.path import islink
from glxshell.lib.os.path.path import isabs
from glxshell.lib.os.path.path import relpath
from glxshell.lib.os.path.path import realpath


class TestLibPath(unittest.TestCase):
    def setUp(self) -> None:
        self.dir = "../../glxshell/lib"
        if "/" in __file__:
            self.dir = __file__.rsplit("/", 1)[0]

        sys.path[0] = self.dir + "/os"

    #     "normcase",
    def test_normcase(self):
        # Normalize the case of a pathname.  Trivial in Posix, string.lower on Mac.
        # On MS-DOS this may also turn slashes into backslashes; however, other
        # normalizations (such as optimizing '../' away) are not allowed
        self.assertEqual("/Hello.42", normcase("/Hello.42"))

    #     "normpath",
    def test_normpath(self):
        # A//B, A/./B and A/foo/../B all become A/B.
        self.assertEqual(".", normpath(""))
        self.assertEqual("//A/B", normpath("//A//B"))
        self.assertEqual("A/B", normpath("A//B"))
        self.assertEqual("A/B", normpath("A/./B"))
        self.assertEqual("A/B", normpath("A/foo/../B"))

    #     "abspath",
    def test_abspath(self):
        self.assertEqual("%s/A/B" % os.getcwd(), abspath("A//B"))
        self.assertEqual("/A/B", abspath("/A//B"))

    #     "join",
    def test_join(self):
        self.assertEqual("a/b", joinpath("a", "b"))
        self.assertEqual("a/b", joinpath("a/", "b"))
        self.assertEqual("/b", joinpath("a", "/b"))
        self.assertEqual("a", joinpath("a"))
        self.assertEqual("a/", joinpath("a/"))
        self.assertEqual("/a", joinpath("/a"))
        self.assertEqual("", joinpath("", ""))
        self.assertEqual("", joinpath(""))

    #     "split",
    def test_split(self):
        self.assertEqual( ("", ""), split(""))
        self.assertEqual(("/", ""), split("/"))
        self.assertEqual(("//", ""), split("///"))
        self.assertEqual(("", "hello"), split("hello"))
        self.assertEqual(("/", "foo"), split("/foo"))
        self.assertEqual(("/foo", ""), split("/foo/"))
        self.assertEqual(("/foo", "bar"), split("/foo/bar"))

    #     "splitext",
    def test_splitext(self):
        self.assertEqual(("bar", ""), splitext("bar"))
        self.assertEqual(("foo.bar", ".exe"), splitext("foo.bar.exe"))
        self.assertEqual(("/foo/bar", ".exe"), splitext("/foo/bar.exe"))
        self.assertEqual((".cshrc", ""), splitext(".cshrc"))
        self.assertEqual(("/foo/....jpg", ""), splitext("/foo/....jpg"))

    #     "splitdrive",
    def test_splitdrive(self):
        self.assertEqual(("", "/foo/bar"), splitdrive("/foo/bar"))

    #     "dirname",
    def test_dirname(self):
        # Test form https://pubs.opengroup.org/onlinepubs/9699919799/functions/basename.html#tag_16_32

        self.assertEqual(".", dirname("usr"))
        self.assertEqual(".", dirname("usr/"))
        self.assertEqual(".", dirname(""))
        self.assertEqual("/", dirname("/"))
        self.assertEqual("/", dirname("//"))
        self.assertEqual("/", dirname("///"))
        self.assertEqual("/", dirname("/usr/"))
        self.assertEqual("/usr", dirname("/usr/lib"))
        self.assertEqual("//usr", dirname("//usr//lib"))
        self.assertEqual("/home/dwc", dirname("/home//dwc//test"))
        self.assertEqual("/home/dwc", dirname("/home//dwc//test//"))

    #     "basename",
    def test_basename(self):
        # Test form https://pubs.opengroup.org/onlinepubs/9699919799/functions/basename.html#tag_16_32
        self.assertEqual("usr", basename("usr"))
        self.assertEqual("usr", basename("usr/"))
        self.assertEqual(".", basename(""))
        self.assertEqual("/", basename("/"))
        self.assertEqual("/", basename("//"))
        self.assertEqual("/", basename("///"))
        self.assertEqual("usr", basename("/usr/"))
        self.assertEqual("lib", basename("/usr/lib"))
        self.assertEqual("lib", basename("//usr//lib"))
        self.assertEqual("test", basename("/home//dwc//test"))
        self.assertEqual("test", basename("/home//dwc//test.txt", ".txt"))

    #     "exists",
    def test_exists(self):
        self.assertTrue(exists(self.dir))
        self.assertTrue(exists(__file__))
        self.assertFalse(exists("Hello.42"))

    #     "exists",
    def test_lexists(self):
        self.assertTrue(lexists(joinpath(self.dir, "data", "link")))
        self.assertFalse(lexists(__file__))
        # self.assertFalse(lexists("Hello.42"))

    #     "isfile",
    def test_isfile(self):
        self.assertTrue(isfile(__file__))
        self.assertFalse(isfile(self.dir))
        self.assertFalse(isfile("Hello.42"))

    #     "isdir",
    def test_isdir(self):
        self.assertFalse(isdir(__file__))
        self.assertTrue(isdir(self.dir))
        self.assertFalse(isdir("Hello.42"))

    #     "islink",
    def test_islink(self):
        self.assertTrue(islink(joinpath(self.dir, "data", "link")))
        self.assertFalse(islink(__file__))
        self.assertFalse(islink(self.dir))
        self.assertFalse(islink("Hello.42"))

    #     "isabs",
    def test_isabs(self):
        self.assertTrue(isabs("/Hello.42"))
        self.assertFalse(isabs("Hello.42"))

    #     "realpath",
    def test_realpath(self):
        self.assertTrue(realpath("/Hello.42"))

        # print(
        #     GLXShell.lib.path.realpath(
        #         GLXShell.lib.path.join(
        #             GLXShell.lib.path.dirname(__file__),
        #             "..",
        #         )
        #     )
        # )
        self.assertTrue(realpath(joinpath(dirname(__file__), "..")))

    #     "expanduser",
    #     "commonprefix",
    #     "relpath",


if __name__ == "__main__":
    unittest.main()
