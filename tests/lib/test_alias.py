import unittest

from glxshell.lib.alias import GLXAlias


class TestLibalias(unittest.TestCase):

    def test_lib_alias(self):
        lib_alias = GLXAlias()
        self.assertEqual(lib_alias.alias, {})

        lib_alias.alias = None
        self.assertEqual(lib_alias.alias, {})

        lib_alias.alias = {"ls": "ls -la"}
        self.assertEqual(lib_alias.alias, {"ls": "ls -la"})

        self.assertRaises(TypeError, setattr, lib_alias, "alias", 42)


if __name__ == "__main__":
    unittest.main()
