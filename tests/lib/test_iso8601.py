import unittest
from glxshell.lib.iso8601 import is_iso8601


class TestIso8601(unittest.TestCase):
    def test_is_iso8601(self):
        """Test if is_iso8601 can determine if a string is on iso8601 date format"""

        self.assertTrue(is_iso8601("2024-03-19"))
        self.assertTrue(is_iso8601("2014-12-05T12:30:45.123456-05:30"))
        self.assertTrue(is_iso8601("2024-03-19T18:27:09Z"))

        # Should normally be True
        self.assertFalse(is_iso8601("2024-W12-2T18:27:09Z"))
        self.assertFalse(is_iso8601("18:27:09Z"))
