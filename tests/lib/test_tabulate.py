import unittest

from glxshell.lib.tabulate import tabulate


class TestLibTabulate(unittest.TestCase):
    def test_tabulate(self):
        value = [["sysfs", "0", "0", "0", "-", "/sys"], ["total", 507297544, 191836496, 315461048, "38%", "-"]]
        headers = ["Filesystem", "512B", "Used", "Available", "Capacity", "Mounted on"]
        tablefmt = "plain"
        colalign = ("left", "center", "right", "right", "right", "left")

        expected = """Filesystem   512B         Used Available Capacity Mounted on
sysfs          0             0         0        - /sys
total      507297544 191836496 315461048      38% -"""
        self.assertEqual(
            expected,
            tabulate(
                tabular_data=value,
                headers=headers,
                tablefmt=tablefmt,
                colalign=colalign,
            ),
        )


if __name__ == "__main__":
    unittest.main()
