import unittest

from glxshell.lib.textwrap import indent
from glxshell.lib.textwrap import wrap


class TestLibGlxTexWrap(unittest.TestCase):
    def test_indent(self):
        s = "hello\n\n \nworld"
        self.assertEqual("  hello\n\n \n  world", indent(s, "  "))
        self.assertEqual(
            """+ hello
+ 
+  
+ world""",
            indent(s, "+ ", lambda line: True),
        )

    def test_wrap(self):
        s = "hello\n\n \nworld"
        self.assertEqual(["hello", "world"], wrap(s, 5))


if __name__ == "__main__":
    unittest.main()
