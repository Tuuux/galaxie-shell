import unittest
from glxshell.lib.environ import GLXEnviron


class TestEnviron(unittest.TestCase):
    def setUp(self) -> None:
        self.environ = GLXEnviron()

    def test_environ(self):
        self.assertTrue(type(self.environ.environ) == dict)

        self.environ.environ = {"Hello": 42}
        self.assertEqual(42, self.environ.environ["Hello"])

        self.environ.environ["Hello"] = 43
        self.assertEqual(43, self.environ.environ["Hello"])

        self.assertRaises(TypeError, setattr, self.environ.environ, "Hello")

    def test_get(self):
        self.assertIsNone(self.environ.getenv("Hello"))

        self.environ.environ = {"Hello": 42}
        self.assertEqual(42, self.environ.getenv("Hello"))

    def test_set(self):
        self.assertIsNone(self.environ.getenv("Hello"))
        self.environ.setenv("Hello", "42", 0)
        self.assertEqual("42", self.environ.getenv("Hello"))

        self.environ.setenv("Hello", "42")
        self.assertEqual("42", self.environ.getenv("Hello"))

        self.environ.setenv("Hello", "43", 0)
        self.assertEqual("42", self.environ.getenv("Hello"))

        self.environ.setenv("Hello", "43", 1)
        self.assertEqual("43", self.environ.getenv("Hello"))

        self.assertRaises(TypeError, self.environ.setenv, "Hello", 42)
        self.assertRaises(TypeError, self.environ.setenv, 42, "Hello")

    def test_unsetenv(self):
        self.assertIsNone(self.environ.getenv("Hello"))
        self.environ.setenv("Hello", "42", 0)
        self.assertEqual("42", self.environ.getenv("Hello"))


if __name__ == "__main__":
    unittest.main()
