import unittest

from glxshell.lib.ush import GLXUsh


class TestGLXTouch(unittest.TestCase):

    def test_cmdline_split(self):
        shell = GLXUsh()
        self.assertEqual(
            ['echo', 'toto', '&&', 'ls', '-C', '>', 'toto.txt'],
            shell.cmdline_split("echo toto && ls -C > toto.txt", 1)
        )


if __name__ == "__main__":
    unittest.main()
