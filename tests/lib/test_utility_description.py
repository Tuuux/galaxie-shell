import unittest
from glxshell.lib.argparse.utility_description import UtilityDescription


class TestUtilityDescription(unittest.TestCase):
    def setUp(self):
        self.utility_description = UtilityDescription()

    def test_name(self):
        self.assertIsNone(self.utility_description.name)

        self.utility_description.name = "Hello.42"
        self.assertEqual("Hello.42", self.utility_description.name)

        self.utility_description.name = None
        self.assertIsNone(self.utility_description.name)

        self.assertRaises(TypeError, setattr, self.utility_description, "name", 42)

    def test_synoptis(self):
        self.assertIsNone(self.utility_description.synopsis)

        self.utility_description.synopsis = ["synopsis.42"]
        self.assertEqual(["synopsis.42"], self.utility_description.synopsis)

        self.utility_description.synopsis = None
        self.assertIsNone(self.utility_description.synopsis)

        self.assertRaises(TypeError, setattr, self.utility_description, "synopsis", 42)

    def test_description(self):
        self.assertIsNone(self.utility_description.description)

        self.utility_description.description = "description.42"
        self.assertEqual("description.42", self.utility_description.description)

        self.utility_description.description = None
        self.assertIsNone(self.utility_description.description)

        self.assertRaises(TypeError, setattr, self.utility_description, "description", 42)

    def test_multi(self):
        self.utility_description.name = "Hello.42"
        self.utility_description.description = "Hello.43"

        self.assertEqual("Hello.42", self.utility_description.name)
        self.assertEqual("Hello.43", self.utility_description.description)
