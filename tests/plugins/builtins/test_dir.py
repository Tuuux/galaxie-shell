import unittest
import os
from io import StringIO
from unittest.mock import patch

from GLXShell.plugins.builtins.dir import GLXDir
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY


class TestGLXDir(unittest.TestCase):
    def setUp(self) -> None:
        self.dir = GLXDir()

    def test_dir_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "dir ({appname}) v{version}\n{licence}\n".format(
                appname=PLUGIN_DESCRIPTION,
                version=PLUGIN_VERSION,
                licence=PLUGIN_WARRANTY,
            )
            self.dir.dir_print_version()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_dir(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = "{0}\n".format("\n".join(os.listdir(os.getcwd())))
            self.dir.dir()
            self.assertEqual(fake_out.getvalue(), expected)


if __name__ == "__main__":
    unittest.main()
