import unittest
import os
from io import StringIO
from unittest.mock import patch
import time

from GLXShell.plugins.builtins.sleep import GLXSleep
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY


class TestGLXArch(unittest.TestCase):
    def setUp(self) -> None:
        self.sleep = GLXSleep()

    def test_result(self):
        self.assertEqual(self.sleep.result, os.uname().machine)

    def test_sleep_work_with(self):
        self.sleep.work_with = None
        self.assertEqual("seconds", self.sleep.work_with)

        for value in ['seconds', 'minutes', 'hours', 'days']:
            self.sleep.work_with = value
            self.assertEqual(value, self.sleep.work_with)

        self.assertRaises(TypeError, setattr, self.sleep, 'work_with', 42)
        self.assertRaises(ValueError, setattr, self.sleep, 'work_with', "Hello.42")

    def test_sleep_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.sleep.sleep_print_version()
            self.assertEqual(fake_out.getvalue(),
                             "sleep ({app_name}) v{version}\n{licence}\n".format(app_name=PLUGIN_DESCRIPTION,
                                                                                 version=PLUGIN_VERSION,
                                                                                 licence=PLUGIN_WARRANTY, )
                             )

    def test_sleep_values_to_seconds(self):
        self.assertEqual(42.0, self.sleep.values_to_seconds(values=["42s"]))
        self.assertEqual(1.0, self.sleep.values_to_seconds(values=["1s"]))
        self.assertEqual(60.0, self.sleep.values_to_seconds(values=["1m"]))
        self.assertEqual(3600.0, self.sleep.values_to_seconds(values=["1h"]))
        self.assertEqual(86400.0, self.sleep.values_to_seconds(values=["1d"]))

        self.assertEqual(86400.0 + 3600.0 + 60.0 + 1.0 + 42.0,
                         self.sleep.values_to_seconds(values=["1d", "1h", "1m", "1s", "42"]))

        self.assertRaises(TypeError, self.sleep.values_to_seconds, 42)


    def test_sleep_days_to_seconds(self):
        self.assertEqual(86400.0, self.sleep.days_to_seconds(days=1))
        self.assertEqual(8640.0, self.sleep.days_to_seconds(days=0.1))
        self.assertEqual(0.0, self.sleep.days_to_seconds(days=None))
        self.assertRaises(TypeError, self.sleep.days_to_seconds, "Hello.42")

    def test_sleep_hours_to_seconds(self):
        self.assertEqual(3600.0, self.sleep.hours_to_seconds(hours=1))
        self.assertEqual(360.0, self.sleep.hours_to_seconds(hours=0.1))
        self.assertEqual(0.0, self.sleep.hours_to_seconds(hours=None))
        self.assertRaises(TypeError, self.sleep.hours_to_seconds, "Hello.42")

    def test_sleep_minutes_to_seconds(self):
        self.assertEqual(60.0, self.sleep.minutes_to_seconds(minutes=1))
        self.assertEqual(6.0, self.sleep.minutes_to_seconds(minutes=0.1))
        self.assertEqual(0.0, self.sleep.minutes_to_seconds(minutes=None))
        self.assertRaises(TypeError, self.sleep.minutes_to_seconds, "Hello.42")

    def test_sleep_seconds_to_seconds(self):
        self.assertEqual(1.0, self.sleep.seconds_to_seconds(seconds=1))
        self.assertEqual(0.1, self.sleep.seconds_to_seconds(seconds=0.1))
        self.assertEqual(0.0, self.sleep.seconds_to_seconds(seconds=None))
        self.assertRaises(TypeError, self.sleep.seconds_to_seconds, "Hello.42")

    def test_sleep(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_result = ""
            self.assertTrue(self.sleep.sleep())
            start = time.time()
            self.sleep.sleep(values=["1"])
            stop = time.time()
            self.assertGreaterEqual(0.01, stop - start - 1.0)
            start = time.time()
            self.sleep.sleep(values=["1", "1"])
            stop = time.time()
            self.assertGreaterEqual(0.01, stop - start - 2.0)
            self.assertEqual(fake_out.getvalue(), expected_result)


if __name__ == "__main__":
    unittest.main()
