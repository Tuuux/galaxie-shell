import unittest
import platform
import sys
from io import StringIO
from unittest.mock import patch

from GLXShell.plugins.builtins.uname import GLXUname
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY


class TestGLXUname(unittest.TestCase):
    def setUp(self) -> None:
        self.uname = GLXUname()

    def test_uname_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "uname ({appname}) v{version}\n{licence}\n".format(
                appname=PLUGIN_DESCRIPTION,
                version=PLUGIN_VERSION,
                licence=PLUGIN_WARRANTY,
            )
            self.uname.uname_print_version()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_uname_nothing(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            processor = platform.uname().processor
            if processor == "":
                processor = "unknown"
            with patch("sys.stdout", new=StringIO()) as fake_out:
                expected_version = "{0}\n".format(
                    platform.uname().system,
                    platform.uname().node,
                    platform.uname().release,
                    platform.uname().version,
                    platform.uname().machine,
                    processor,
                    "unknown",
                    sys.platform,
                )
                self.uname.uname(
                    all_info=False,
                    kernel_name=False,
                    nodename=False,
                    kernel_release=False,
                    kernel_version=False,
                    machine=False,
                    processor=False,
                    hardware_platform=False,
                    operating_system=False,
                )
                self.assertEqual(fake_out.getvalue(), expected_version)

    def test_uname_all(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            processor = platform.uname().processor
            if processor == "":
                processor = "unknown"
            with patch("sys.stdout", new=StringIO()) as fake_out:
                expected_version = "{0} {1} {2} {3} {4} {7}\n".format(
                    platform.uname().system,
                    platform.uname().node,
                    platform.uname().release,
                    platform.uname().version,
                    platform.uname().machine,
                    processor,
                    "unknown",
                    sys.platform,
                )
                self.uname.uname(
                    all_info=True,
                    kernel_name=False,
                    nodename=False,
                    kernel_release=False,
                    kernel_version=False,
                    machine=False,
                    processor=False,
                    hardware_platform=False,
                    operating_system=False,
                )
                self.assertEqual(fake_out.getvalue(), expected_version)

    def test_uname_everything(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            processor = platform.uname().processor
            if processor == "":
                processor = "unknown"
            with patch("sys.stdout", new=StringIO()) as fake_out:
                expected_version = "{0} {1} {2} {3} {4} {5} {6} {7}\n".format(
                    platform.uname().system,
                    platform.uname().node,
                    platform.uname().release,
                    platform.uname().version,
                    platform.uname().machine,
                    processor,
                    "unknown",
                    sys.platform,
                )
                self.uname.uname(
                    all_info=False,
                    kernel_name=True,
                    nodename=True,
                    kernel_release=True,
                    kernel_version=True,
                    machine=True,
                    processor=True,
                    hardware_platform=True,
                    operating_system=True,
                )
                self.assertEqual(fake_out.getvalue(), expected_version)


if __name__ == "__main__":
    unittest.main()
