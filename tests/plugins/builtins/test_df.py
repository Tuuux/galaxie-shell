import os.path
import unittest
from io import StringIO
from unittest.mock import patch
import tempfile

from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY
from GLXShell.plugins.builtins.df import GLXDf


class TestGLXTouch(unittest.TestCase):
    def setUp(self) -> None:
        self.df = GLXDf()

    def test_df_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.df.df_print_version()
            self.assertEqual(fake_out.getvalue(),
                             "df ({app_name}) v{version}\n{licence}\n".format(app_name=PLUGIN_DESCRIPTION,
                                                                              version=PLUGIN_VERSION,
                                                                              licence=PLUGIN_WARRANTY, )
                             )

    def test_df_get_devices(self):
        # self.assertEqual("", self.df.get_devices())
        file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'data',
            'mtab',
        )
        data = [['sysfs', '/sys', 'sysfs', 'rw,nosuid,nodev,noexec,relatime', '0', '0'],
                ['proc', '/proc', 'proc', 'rw,nosuid,nodev,noexec,relatime', '0', '0'],
                ['udev',
                 '/dev',
                 'devtmpfs',
                 'rw,nosuid,relatime,size=3962900k,nr_inodes=990725,mode=755',
                 '0',
                 '0'],
                ['devpts',
                 '/dev/pts',
                 'devpts',
                 'rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000',
                 '0',
                 '0'],
                ['tmpfs',
                 '/run',
                 'tmpfs',
                 'rw,nosuid,nodev,noexec,relatime,size=796456k,mode=755',
                 '0',
                 '0'],
                ['/dev/sda2', '/', 'ext4', 'rw,relatime,errors=remount-ro', '0', '0'],
                ['securityfs',
                 '/sys/kernel/security',
                 'securityfs',
                 'rw,nosuid,nodev,noexec,relatime',
                 '0',
                 '0'],
                ['tmpfs', '/dev/shm', 'tmpfs', 'rw,nosuid,nodev', '0', '0'],
                ['tmpfs',
                 '/run/lock',
                 'tmpfs',
                 'rw,nosuid,nodev,noexec,relatime,size=5120k',
                 '0',
                 '0'],
                ['cgroup2',
                 '/sys/fs/cgroup',
                 'cgroup2',
                 'rw,nosuid,nodev,noexec,relatime,nsdelegate,memory_recursiveprot',
                 '0',
                 '0'],
                ['pstore',
                 '/sys/fs/pstore',
                 'pstore',
                 'rw,nosuid,nodev,noexec,relatime',
                 '0',
                 '0'],
                ['efivarfs',
                 '/sys/firmware/efi/efivars',
                 'efivarfs',
                 'rw,nosuid,nodev,noexec,relatime',
                 '0',
                 '0'],
                ['none',
                 '/sys/fs/bpf',
                 'bpf',
                 'rw,nosuid,nodev,noexec,relatime,mode=700',
                 '0',
                 '0'],
                ['systemd-1',
                 '/proc/sys/fs/binfmt_misc',
                 'autofs',
                 'rw,relatime,fd=30,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=11343',
                 '0',
                 '0'],
                ['tracefs',
                 '/sys/kernel/tracing',
                 'tracefs',
                 'rw,nosuid,nodev,noexec,relatime',
                 '0',
                 '0'],
                ['hugetlbfs',
                 '/dev/hugepages',
                 'hugetlbfs',
                 'rw,relatime,pagesize=2M',
                 '0',
                 '0'],
                ['mqueue',
                 '/dev/mqueue',
                 'mqueue',
                 'rw,nosuid,nodev,noexec,relatime',
                 '0',
                 '0'],
                ['debugfs',
                 '/sys/kernel/debug',
                 'debugfs',
                 'rw,nosuid,nodev,noexec,relatime',
                 '0',
                 '0'],
                ['configfs',
                 '/sys/kernel/config',
                 'configfs',
                 'rw,nosuid,nodev,noexec,relatime',
                 '0',
                 '0'],
                ['fusectl',
                 '/sys/fs/fuse/connections',
                 'fusectl',
                 'rw,nosuid,nodev,noexec,relatime',
                 '0',
                 '0'],
                ['/dev/sda1',
                 '/boot/efi',
                 'vfat',
                 'rw,relatime,fmask=0077,dmask=0077,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro',
                 '0',
                 '0'],
                ['tmpfs',
                 '/run/user/1000',
                 'tmpfs',
                 'rw,nosuid,nodev,relatime,size=796452k,nr_inodes=199113,mode=700,uid=1000,gid=1000',
                 '0',
                 '0'],
                ['binfmt_misc',
                 '/proc/sys/fs/binfmt_misc',
                 'binfmt_misc',
                 'rw,nosuid,nodev,noexec,relatime',
                 '0',
                 '0']]
        self.assertEqual(data, self.df.df_get_devices(file=file))

    def test_df_get_file_content(self):
        file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'data',
            'mtab',
        )
        data = """sysfs /sys sysfs rw,nosuid,nodev,noexec,relatime 0 0
proc /proc proc rw,nosuid,nodev,noexec,relatime 0 0
udev /dev devtmpfs rw,nosuid,relatime,size=3962900k,nr_inodes=990725,mode=755 0 0
devpts /dev/pts devpts rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000 0 0
tmpfs /run tmpfs rw,nosuid,nodev,noexec,relatime,size=796456k,mode=755 0 0
/dev/sda2 / ext4 rw,relatime,errors=remount-ro 0 0
securityfs /sys/kernel/security securityfs rw,nosuid,nodev,noexec,relatime 0 0
tmpfs /dev/shm tmpfs rw,nosuid,nodev 0 0
tmpfs /run/lock tmpfs rw,nosuid,nodev,noexec,relatime,size=5120k 0 0
cgroup2 /sys/fs/cgroup cgroup2 rw,nosuid,nodev,noexec,relatime,nsdelegate,memory_recursiveprot 0 0
pstore /sys/fs/pstore pstore rw,nosuid,nodev,noexec,relatime 0 0
efivarfs /sys/firmware/efi/efivars efivarfs rw,nosuid,nodev,noexec,relatime 0 0
none /sys/fs/bpf bpf rw,nosuid,nodev,noexec,relatime,mode=700 0 0
systemd-1 /proc/sys/fs/binfmt_misc autofs rw,relatime,fd=30,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=11343 0 0
tracefs /sys/kernel/tracing tracefs rw,nosuid,nodev,noexec,relatime 0 0
hugetlbfs /dev/hugepages hugetlbfs rw,relatime,pagesize=2M 0 0
mqueue /dev/mqueue mqueue rw,nosuid,nodev,noexec,relatime 0 0
debugfs /sys/kernel/debug debugfs rw,nosuid,nodev,noexec,relatime 0 0
configfs /sys/kernel/config configfs rw,nosuid,nodev,noexec,relatime 0 0
fusectl /sys/fs/fuse/connections fusectl rw,nosuid,nodev,noexec,relatime 0 0
/dev/sda1 /boot/efi vfat rw,relatime,fmask=0077,dmask=0077,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro 0 0
tmpfs /run/user/1000 tmpfs rw,nosuid,nodev,relatime,size=796452k,nr_inodes=199113,mode=700,uid=1000,gid=1000 0 0
binfmt_misc /proc/sys/fs/binfmt_misc binfmt_misc rw,nosuid,nodev,noexec,relatime 0 0"""
        self.assertEqual(data, self.df.get_file_content(file=file))

        self.assertRaises(FileExistsError, self.df.get_file_content, file='Hello.42')

    def test_df_sizeof(self):
        """Test Utils sizeof"""
        # Ref: https://en.wikipedia.org/wiki/Metric_prefix
        self.assertEqual("1Y", self.df.sizeof(1000000000000000000000000))
        self.assertEqual("1Z", self.df.sizeof(1000000000000000000000))
        self.assertEqual("1E", self.df.sizeof(1000000000000000000))
        self.assertEqual("1P", self.df.sizeof(1000000000000000))
        self.assertEqual("1T", self.df.sizeof(1000000000000))
        self.assertEqual("1G", self.df.sizeof(1000000000))
        self.assertEqual("1M", self.df.sizeof(1000000))
        self.assertEqual("1k", self.df.sizeof(1000))
        self.assertEqual("10", self.df.sizeof(10))

        self.assertRaises(TypeError, self.df.sizeof, "42")

    def test_df_print_final(self):
        data = [['sysfs', '0', '0', '0', '-', '/sys'],
                ['total', 507297544, 191836496, 315461048, '38%', '-']]
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.df.df_print_final(block_size_text="512", tabular_data=data)
            self.assertEqual("""Filesystem          512       Used    Available    Capacity  Mounted on
sysfs                 0          0            0           -  /sys
total         507297544  191836496    315461048         38%  -
""", fake_out.getvalue())

    def test_df_print_help(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.df.df_print_help()
            self.assertIsNotNone(fake_out.getvalue())

    def test_df_get_info_to_print(self):
        data = [['sysfs', '-', '-', '-', '-', '/sys'],
                ['total', '42', '42', '42', '38%', '-']]
        self.assertEqual(('Size',
                          [['sysfs', '-', '-', '-', '-', '/sys'],
                           ['total', '21k', '21k', '21k', '38%', '-']]),
                         self.df.df_get_info_to_print(block_size=512, devices_list=data, human_readable=True)
                         )

    def test_df_get_totals(self):
        data = [['sysfs', '0', '0', '0', '-', '/sys'],
                ['sysfs', '-', '-', '-', '-', '/sys'],
                ['total', 42, 42, 42, '38%', '-']]
        self.assertEqual((42, 42, 42), self.df.df_get_totals(devices_list=data))

    def test_df_find_mount_point(self):
        self.assertEqual("/dev", self.df.df_find_mount_point("/dev/null"))
        self.assertEqual("/proc", self.df.df_find_mount_point("/proc/meminfo"))

    def test_df_get_device_information(self):
        data = self.df.df_get_device_information(file_system_name="ext4", file_system_root="/", block_size=512)
        self.assertEqual("ext4", data[0])
        self.assertEqual("/", data[5])

    def test_df(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.df.df()
            self.assertIsNotNone(fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.df.df(total=True)
            self.assertIsNotNone(fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.df.df(total=True, human_readable=True)
            self.assertIsNotNone(fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.df.df(file="/")
            self.assertIsNotNone(fake_out.getvalue())

        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.df.df(file="//")
            self.assertIsNotNone(fake_out.getvalue())

        with patch("sys.stderr", new=StringIO()) as fake_out:
            self.df.df(file="Hello.42")
            self.assertEqual("df: Hello.42: No such file or directory\n", fake_out.getvalue())


if __name__ == "__main__":
    unittest.main()
