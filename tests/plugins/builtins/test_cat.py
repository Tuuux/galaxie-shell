import unittest
import os
from io import StringIO
from unittest.mock import patch

from GLXShell.plugins.builtins.cat import GLXCat
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY


class TestGLXArch(unittest.TestCase):
    def setUp(self) -> None:
        if os.path.isfile(os.path.join(os.getcwd(), "test_cat_1.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_cat_1.tmp"))
        if os.path.isfile(os.path.join(os.getcwd(), "test_cat_2.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_cat_2.tmp"))

        self.tmp_file_1 = os.path.join(os.getcwd(), "test_cat_1.tmp")
        self.tmp_file_2 = os.path.join(os.getcwd(), "test_cat_2.tmp")

        file = open(self.tmp_file_1, "w")
        file.write("""Hello 41\n\n\tHello 41\n\n\t\tHello 41\n""")

        file.close()

        file = open(self.tmp_file_2, "w")
        file.write("""Hello 42\n\n\tHello 42\n\n\t\tHello 42\n""")
        file.close()

        self.cat = GLXCat()

    def tearDown(self):
        if os.path.isfile(os.path.join(os.getcwd(), "test_cat_1.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_cat_1.tmp"))
        if os.path.isfile(os.path.join(os.getcwd(), "test_cat_2.tmp")):
            os.remove(os.path.join(os.getcwd(), "test_cat_2.tmp"))

    def test_arch_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "cat ({appname}) v{version}\n{licence}\n".format(
                appname=PLUGIN_DESCRIPTION,
                version=PLUGIN_VERSION,
                licence=PLUGIN_WARRANTY,
            )
            self.cat.cat_print_version()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_cat(self):

        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_1, "r") as file:
                expected = """Hello 41

\tHello 41

\t\tHello 41
"""

                self.cat.cat(
                    file=[file],
                    number_nonblank=False,
                    number=False,
                    show_tabs=False,
                    squeeze_blank=False,
                    show_ends=False,
                )
                self.assertEqual(fake_out.getvalue(), expected)

    def test_cat_number_nonblank(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_1, "r") as file:
                expected = """     1  Hello 41

     2  	Hello 41

     3  		Hello 41
"""
                self.cat.cat(
                    file=[file],
                    number_nonblank=True,
                    number=False,
                    show_tabs=False,
                    squeeze_blank=False,
                    show_ends=False,
                )
                self.assertEqual(fake_out.getvalue(), expected)

    def test_cat_number(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_1, "r") as file:
                expected = """     1  Hello 41
     2  
     3  	Hello 41
     4  
     5  		Hello 41
     6  """
                self.cat.cat(
                    file=[file],
                    number_nonblank=False,
                    number=True,
                    show_tabs=False,
                    squeeze_blank=False,
                    show_ends=False,
                )
                self.assertEqual(fake_out.getvalue(), expected)

    def test_cat_show_tabs(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_1, "r") as file:
                expected = """Hello 41

^IHello 41

^I^IHello 41
"""
                self.cat.cat(
                    file=[file],
                    number_nonblank=False,
                    number=False,
                    show_tabs=True,
                    squeeze_blank=False,
                    show_ends=False,
                )
                self.assertEqual(fake_out.getvalue(), expected)

    def test_cat_squeeze_blank(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_1, "r") as file:
                expected = """Hello 41

\tHello 41

\t\tHello 41
"""
                self.cat.cat(
                    file=[file],
                    number_nonblank=False,
                    number=False,
                    show_tabs=False,
                    squeeze_blank=True,
                    show_ends=False,
                )
                self.assertEqual(fake_out.getvalue(), expected)

    def test_cat_show_ends(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_1, "r") as file:
                expected = """Hello 41$
$
\tHello 41$
$
\t\tHello 41$
"""
                self.cat.cat(
                    file=[file],
                    number_nonblank=False,
                    number=False,
                    show_tabs=False,
                    squeeze_blank=False,
                    show_ends=True,
                )
                self.assertEqual(fake_out.getvalue(), expected)

    def test_cat_all_v1(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_1, "r") as file1:
                with open(self.tmp_file_2, "r") as file2:
                    expected = """     1  Hello 41$
     2  $
     3  ^IHello 41$
     4  $
     5  ^I^IHello 41$
     6  $
     7  Hello 42$
     8  $
     9  ^IHello 42$
    10  $
    11  ^I^IHello 42$
    12  """
                    self.cat.cat(
                        file=[file1, file2],
                        number_nonblank=False,
                        number=True,
                        show_tabs=True,
                        squeeze_blank=True,
                        show_ends=True,
                    )
                    self.assertEqual(fake_out.getvalue(), expected)

    def test_cat_all_v2(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            with open(self.tmp_file_1, "r") as file1:
                with open(self.tmp_file_2, "r") as file2:
                    expected = """     1  Hello 41$
$
     2  ^IHello 41$
$
     3  ^I^IHello 41$
$
     4  Hello 42$
$
     5  ^IHello 42$
$
     6  ^I^IHello 42$
"""
                    self.cat.cat(
                        file=[file1, file2],
                        number_nonblank=True,
                        number=True,
                        show_tabs=True,
                        squeeze_blank=True,
                        show_ends=True,
                    )
                    self.assertEqual(fake_out.getvalue(), expected)

    # def test_cat_complete_cat(self):
    #
    #     self.assertEqual(
    #         [self.temp_dir_2],
    #         self.cat.complete_cat(
    #             text="{0}{1}".format(self.temp_dir_1, os.path.sep),
    #             line="cat",
    #             begidx=len(self.temp_dir_1),
    #             endidx=len(self.temp_dir_1),
    #         ),
    #     )


if __name__ == "__main__":
    unittest.main()
