import unittest
import os
from io import StringIO
from unittest.mock import patch

from GLXShell.plugins.builtins.which import GLXWhich
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY


class TestGLXWhich(unittest.TestCase):
    def setUp(self) -> None:
        self.which = GLXWhich()

    def test_which_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "which ({appname}) v{version}\n{licence}\n".format(
                appname=PLUGIN_DESCRIPTION,
                version=PLUGIN_VERSION,
                licence=PLUGIN_WARRANTY,
            )
            self.which.which_print_version()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_which(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            # expected_version = "{0}\n".format(self.which.result)
            self.which.which(all_info=True, filename="python")
            self.assertTrue(len(fake_out.getvalue().split("\n")[:-1]) > 1)

    def test_which_all(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            # expected_version = "{0}\n".format(self.which.result)
            self.which.which(all_info=False, filename="python")
            self.assertTrue(len(fake_out.getvalue().split("\n")[:-1]) == 1)


if __name__ == "__main__":
    unittest.main()
