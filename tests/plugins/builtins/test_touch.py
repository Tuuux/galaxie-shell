import unittest
from io import StringIO
from unittest.mock import patch

from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY
from GLXShell.plugins.builtins.touch import GLXTouch


class TestGLXTouch(unittest.TestCase):
    def setUp(self) -> None:
        self.touch = GLXTouch()

    def test_touch_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.touch.touch_print_version()
            self.assertEqual(fake_out.getvalue(),
                             "touch ({app_name}) v{version}\n{licence}\n".format(app_name=PLUGIN_DESCRIPTION,
                                                                                 version=PLUGIN_VERSION,
                                                                                 licence=PLUGIN_WARRANTY, )
                             )

    def test_touch(self):
        self.assertRaises(NotImplementedError, self.touch.touch)


if __name__ == "__main__":
    unittest.main()
