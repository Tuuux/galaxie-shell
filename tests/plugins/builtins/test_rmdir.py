import unittest
import os
from io import StringIO
from unittest.mock import patch

from GLXShell.plugins.builtins.rmdir import GLXRmDir
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY


class TestGLXRmDir(unittest.TestCase):
    def setUp(self) -> None:
        self.rmdir = GLXRmDir()
        self.temporary_directory_name_1 = "tests_rmdir"
        self.temporary_directory_name_2 = os.path.join(
            self.temporary_directory_name_1, "42"
        )

        if os.path.exists(self.temporary_directory_name_2):
            if os.path.isdir(self.temporary_directory_name_2):
                os.rmdir(self.temporary_directory_name_2)
            if os.path.isfile(self.temporary_directory_name_2) or os.path.islink(
                self.temporary_directory_name_2
            ):
                os.remove(self.temporary_directory_name_2)

        if os.path.exists(self.temporary_directory_name_1):
            if os.path.isdir(self.temporary_directory_name_1):
                os.rmdir(self.temporary_directory_name_1)
            if os.path.isfile(self.temporary_directory_name_1) or os.path.islink(
                self.temporary_directory_name_1
            ):
                os.remove(self.temporary_directory_name_1)

        if not os.path.exists(self.temporary_directory_name_1):
            os.mkdir(self.temporary_directory_name_1)
        if not os.path.exists(self.temporary_directory_name_2):
            os.mkdir(self.temporary_directory_name_2)

    def tearDown(self):
        if os.path.exists(self.temporary_directory_name_2):
            if os.path.isdir(self.temporary_directory_name_2):
                os.rmdir(self.temporary_directory_name_2)
            if os.path.isfile(self.temporary_directory_name_2) or os.path.islink(
                self.temporary_directory_name_2
            ):
                os.remove(self.temporary_directory_name_2)

        if os.path.exists(self.temporary_directory_name_1):
            if os.path.isdir(self.temporary_directory_name_1):
                os.rmdir(self.temporary_directory_name_1)
            if os.path.isfile(self.temporary_directory_name_1) or os.path.islink(
                self.temporary_directory_name_1
            ):
                os.remove(self.temporary_directory_name_1)

    def test_rmdir_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "rmdir ({appname}) v{version}\n{licence}\n".format(
                appname=PLUGIN_DESCRIPTION,
                version=PLUGIN_VERSION,
                licence=PLUGIN_WARRANTY,
            )
            self.rmdir.rmdir_print_version()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_rmdir(self):
        self.assertTrue(os.path.isdir(self.temporary_directory_name_1))
        self.assertTrue(os.path.isdir(self.temporary_directory_name_2))

        with patch("sys.stderr", new=StringIO()) as fake_out:
            expected = "rmdir: failed to remove '{0}': Directory not empty\n".format(
                self.temporary_directory_name_1
            )
            self.rmdir.rmdir(
                directories=[self.temporary_directory_name_1],
                parents=False,
                verbose=True,
                ignore_fail_on_non_empty=False,
            )
            self.assertEqual(fake_out.getvalue(), expected)

        with patch("sys.stderr", new=StringIO()) as fake_out:
            expected = "rmdir: failed to remove '{0}': Directory not empty\n".format(
                self.temporary_directory_name_1
            )
            self.rmdir.rmdir(
                directories=[self.temporary_directory_name_1],
                parents=True,
                verbose=True,
                ignore_fail_on_non_empty=False,
            )
            self.assertEqual(fake_out.getvalue(), expected)

        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = (
                "rmdir: removing directory '{0}'\n"
                "rmdir: removing directory '{1}'\n".format(
                    self.temporary_directory_name_2, self.temporary_directory_name_1
                )
            )
            self.rmdir.rmdir(
                directories=[self.temporary_directory_name_2],
                parents=True,
                verbose=True,
                ignore_fail_on_non_empty=False,
            )
            self.assertEqual(fake_out.getvalue(), expected)

        self.setUp()
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = "rmdir: removing directory '{0}'\n".format(
                self.temporary_directory_name_2
            )
            self.rmdir.rmdir(
                directories=[self.temporary_directory_name_2],
                parents=False,
                verbose=True,
                ignore_fail_on_non_empty=False,
            )
            self.assertEqual(fake_out.getvalue(), expected)

        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = "rmdir: removing directory '{0}'\n".format(
                self.temporary_directory_name_1
            )
            self.rmdir.rmdir(
                directories=[self.temporary_directory_name_1],
                parents=False,
                verbose=True,
                ignore_fail_on_non_empty=False,
            )
            self.assertEqual(fake_out.getvalue(), expected)

        with patch("sys.stderr", new=StringIO()) as fake_out:
            expected = (
                "rmdir: failed to remove '{0}': No such file or directory\n"
                "rmdir: failed to remove '{1}': No such file or directory\n".format(
                    self.temporary_directory_name_2, self.temporary_directory_name_1
                )
            )
            self.rmdir.rmdir(
                directories=[self.temporary_directory_name_2],
                parents=True,
                verbose=True,
                ignore_fail_on_non_empty=False,
            )
            self.assertEqual(fake_out.getvalue(), expected)

        with patch("sys.stderr", new=StringIO()) as fake_out:
            expected = (
                "rmdir: failed to remove '{0}': No such file or directory\n".format(
                    self.temporary_directory_name_2
                )
            )
            self.rmdir.rmdir(
                directories=[self.temporary_directory_name_2],
                parents=False,
                verbose=True,
                ignore_fail_on_non_empty=False,
            )
            self.assertEqual(fake_out.getvalue(), expected)

        self.setUp()
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = (
                "rmdir: removing directory '{0}'\n"
                "rmdir: removing directory '{1}'\n".format(
                    self.temporary_directory_name_2, self.temporary_directory_name_1
                )
            )
            self.rmdir.rmdir(
                directories=[self.temporary_directory_name_2],
                parents=True,
                verbose=True,
                ignore_fail_on_non_empty=False,
            )
            self.assertEqual(fake_out.getvalue(), expected)

        self.assertFalse(os.path.isdir(self.temporary_directory_name_1))
        self.assertFalse(os.path.isdir(self.temporary_directory_name_2))


if __name__ == "__main__":
    unittest.main()
