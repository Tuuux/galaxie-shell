import unittest
import os
from io import StringIO
from unittest.mock import patch

from GLXShell.plugins.builtins.pwd import GLXPwd
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY


class TestGLXPwd(unittest.TestCase):
    def setUp(self) -> None:
        self.pwd = GLXPwd()

    def test_pwd_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "pwd ({appname}) v{version}\n{licence}\n".format(
                appname=PLUGIN_DESCRIPTION,
                version=PLUGIN_VERSION,
                licence=PLUGIN_WARRANTY,
            )
            self.pwd.pwd_print_version()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_pwd_print_logical(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "{pwd}\n".format(pwd=os.path.normpath(os.getcwd()))
            self.pwd.pwd_print_logical()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_pwd_print_not_logical(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "{pwd}\n".format(pwd=os.path.realpath(os.getcwd()))
            self.pwd.pwd_print_not_logical()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_pwd(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "{pwd}\n".format(pwd=os.path.normpath(os.getcwd()))
            self.pwd.pwd(logical=True, physical=True)
            self.assertEqual(fake_out.getvalue(), expected_version)

        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "{pwd}\n".format(pwd=os.path.realpath(os.getcwd()))
            self.pwd.pwd(logical=False, physical=True)
            self.assertEqual(fake_out.getvalue(), expected_version)


if __name__ == "__main__":
    unittest.main()
