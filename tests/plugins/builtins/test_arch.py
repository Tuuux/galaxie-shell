import unittest
import os
from io import StringIO
from unittest.mock import patch

from GLXShell.plugins.builtins.arch import GLXArch
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY


class TestGLXArch(unittest.TestCase):
    def setUp(self) -> None:
        self.arch = GLXArch()

    def test_result(self):
        self.assertEqual(self.arch.result, os.uname().machine)

    def test_arch_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "arch ({appname}) v{version}\n{licence}\n".format(
                appname=PLUGIN_DESCRIPTION,
                version=PLUGIN_VERSION,
                licence=PLUGIN_WARRANTY,
            )
            self.arch.arch_print_version()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_arch(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "{0}\n".format(self.arch.result)
            self.arch.arch()
            self.assertEqual(fake_out.getvalue(), expected_version)


if __name__ == "__main__":
    unittest.main()
