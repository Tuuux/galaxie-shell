import unittest
import os
from io import StringIO
from unittest.mock import patch

from GLXShell.plugins.builtins.mkdir import GLXMkdir
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY


class TestGLXMkdir(unittest.TestCase):
    def setUp(self) -> None:
        self.mkdir = GLXMkdir()
        self.temporary_directory_name_1 = "tests_mkdir"
        self.temporary_directory_name_2 = os.path.join(
            self.temporary_directory_name_1, "42"
        )

        if os.path.exists(self.temporary_directory_name_2):
            if os.path.isdir(self.temporary_directory_name_2):
                os.rmdir(self.temporary_directory_name_2)
            if os.path.isfile(self.temporary_directory_name_2) or os.path.islink(
                self.temporary_directory_name_2
            ):
                os.remove(self.temporary_directory_name_2)

        if os.path.exists(self.temporary_directory_name_1):
            if os.path.isdir(self.temporary_directory_name_1):
                os.rmdir(self.temporary_directory_name_1)
            if os.path.isfile(self.temporary_directory_name_1) or os.path.islink(
                self.temporary_directory_name_1
            ):
                os.remove(self.temporary_directory_name_1)

    def tearDown(self):
        if os.path.exists(self.temporary_directory_name_2):
            if os.path.isdir(self.temporary_directory_name_2):
                os.rmdir(self.temporary_directory_name_2)
            if os.path.isfile(self.temporary_directory_name_2) or os.path.islink(
                self.temporary_directory_name_2
            ):
                os.remove(self.temporary_directory_name_2)

        if os.path.exists(self.temporary_directory_name_1):
            if os.path.isdir(self.temporary_directory_name_1):
                os.rmdir(self.temporary_directory_name_1)
            if os.path.isfile(self.temporary_directory_name_1) or os.path.islink(
                self.temporary_directory_name_1
            ):
                os.remove(self.temporary_directory_name_1)

    def test_mkdir_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "mkdir ({appname}) v{version}\n{licence}\n".format(
                appname=PLUGIN_DESCRIPTION,
                version=PLUGIN_VERSION,
                licence=PLUGIN_WARRANTY,
            )
            self.mkdir.mkdir_print_version()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_mkdir(self):
        self.assertFalse(os.path.isdir(self.temporary_directory_name_1))
        self.assertFalse(os.path.isdir(self.temporary_directory_name_2))

        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = (
                "mkdir: created directory '{0}'\n"
                "mkdir: created directory '{1}'\n".format(
                    self.temporary_directory_name_1, self.temporary_directory_name_2
                )
            )
            self.mkdir.mkdir(
                directories=[
                    self.temporary_directory_name_1,
                    self.temporary_directory_name_2,
                ],
                parents=False,
                verbose=True,
                mode="755",
            )
            self.assertEqual(fake_out.getvalue(), expected)

        with patch("sys.stderr", new=StringIO()) as fake_out:
            expected = (
                "mkdir: cannot create directory '{0}': File exists\n"
                "mkdir: cannot create directory '{1}': File exists\n".format(
                    self.temporary_directory_name_1, self.temporary_directory_name_2
                )
            )
            self.mkdir.mkdir(
                directories=[
                    self.temporary_directory_name_1,
                    self.temporary_directory_name_2,
                ],
                parents=False,
                verbose=True,
                mode="755",
            )
            self.assertEqual(fake_out.getvalue(), expected)

        self.tearDown()

        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected = (
                "mkdir: created directory '{0}'\n"
                "mkdir: created directory '{1}'\n".format(
                    self.temporary_directory_name_1, self.temporary_directory_name_2
                )
            )
            self.mkdir.mkdir(
                directories=[self.temporary_directory_name_2],
                parents=True,
                verbose=True,
                mode="755",
            )
            self.assertEqual(fake_out.getvalue(), expected)

        with patch("sys.stderr", new=StringIO()) as fake_out:
            expected = (
                "mkdir: cannot create directory '{0}': File exists\n"
                "mkdir: cannot create directory '{1}': File exists\n".format(
                    self.temporary_directory_name_1, self.temporary_directory_name_2
                )
            )
            self.mkdir.mkdir(
                directories=[self.temporary_directory_name_2],
                parents=True,
                verbose=True,
                mode="755",
            )
            self.assertEqual(fake_out.getvalue(), expected)


if __name__ == "__main__":
    unittest.main()
