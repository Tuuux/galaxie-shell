import unittest
import os
from io import StringIO
from unittest.mock import patch

from GLXShell.plugins.builtins.cd import GLXCd
from GLXShell.plugins.builtins import PLUGIN_VERSION
from GLXShell.plugins.builtins import PLUGIN_DESCRIPTION
from GLXShell.plugins.builtins import PLUGIN_WARRANTY


class TestGLXCd(unittest.TestCase):
    def setUp(self) -> None:
        self.cd = GLXCd()
        self.temp_dir_1 = "tests_cd"
        self.temp_dir_2 = os.path.join(self.temp_dir_1, "42")

        if os.path.exists(self.temp_dir_2):
            if os.path.isdir(self.temp_dir_2):
                os.rmdir(self.temp_dir_2)
            if os.path.isfile(self.temp_dir_2) or os.path.islink(self.temp_dir_2):
                os.remove(self.temp_dir_2)

        if os.path.exists(self.temp_dir_1):
            if os.path.isdir(self.temp_dir_1):
                os.rmdir(self.temp_dir_1)
            if os.path.isfile(self.temp_dir_1) or os.path.islink(self.temp_dir_1):
                os.remove(self.temp_dir_1)

    def tearDown(self):
        if os.path.exists(self.temp_dir_2):
            if os.path.isdir(self.temp_dir_2):
                os.rmdir(self.temp_dir_2)
            if os.path.isfile(self.temp_dir_2) or os.path.islink(self.temp_dir_2):
                os.remove(self.temp_dir_2)

        if os.path.exists(self.temp_dir_1):
            if os.path.isdir(self.temp_dir_1):
                os.rmdir(self.temp_dir_1)
            if os.path.isfile(self.temp_dir_1) or os.path.islink(self.temp_dir_1):
                os.remove(self.temp_dir_1)

    def test_cd_print_version(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            expected_version = "cd ({appname}) v{version}\n{licence}\n".format(
                appname=PLUGIN_DESCRIPTION,
                version=PLUGIN_VERSION,
                licence=PLUGIN_WARRANTY,
            )
            self.cd.cd_print_version()
            self.assertEqual(fake_out.getvalue(), expected_version)

    def test_cd_curpath(self):
        self.cd.curpath = None
        self.assertIsNone(self.cd.curpath)

        self.cd.curpath = "Hello.42"
        self.assertEqual("Hello.42", self.cd.curpath)

        self.assertRaises(TypeError, setattr, self.cd, "curpath", 42)

    def test_cd_directory(self):
        self.cd.directory = None
        self.assertIsNone(self.cd.directory)

        self.cd.directory = "Hello.42"
        self.assertEqual("Hello.42", self.cd.directory)

        self.assertRaises(TypeError, setattr, self.cd, "directory", 42)

    def test_cd_logical(self):
        self.cd.logical = False
        self.assertFalse(self.cd.logical)

        self.cd.logical = True
        self.assertTrue(self.cd.logical)

        self.cd.logical = None
        self.assertFalse(self.cd.logical)

        self.assertRaises(TypeError, setattr, self.cd, "logical", 42)

    def test_cd_physical(self):
        self.cd.physical = False
        self.assertFalse(self.cd.physical)

        self.cd.physical = True
        self.assertTrue(self.cd.physical)

        self.cd.physical = None
        self.assertFalse(self.cd.physical)

        self.assertRaises(TypeError, setattr, self.cd, "physical", 42)

    def test_cd_can_continue(self):
        self.cd.can_continue = False
        self.assertFalse(self.cd.can_continue)

        self.cd.can_continue = True
        self.assertTrue(self.cd.can_continue)

        self.cd.can_continue = None
        self.assertTrue(self.cd.can_continue)

        self.assertRaises(TypeError, setattr, self.cd, "can_continue", 42)

    def test_cd__step_1(self):
        self.cd.can_continue = True
        self.cd.directory = None
        old_home_var = None
        if os.environ.get("HOME"):
            old_home_var = os.environ.get("HOME")
            del os.environ["HOME"]

        self.cd._step_1()
        self.assertFalse(self.cd.can_continue)

        if old_home_var:
            os.environ["HOME"] = old_home_var

    def test_cd__step_2(self):
        self.cd.can_continue = True
        self.cd.directory = None
        os.environ["HOME"] = os.path.expanduser("~")

        self.cd._step_2()

        self.assertEqual(os.path.expanduser("~"), self.cd.directory)

    def test_cd__step_3(self):
        self.cd.can_continue = True
        self.cd.directory = os.path.expanduser("~")
        self.cd.curpath = None

        self.cd._step_3()

        self.assertEqual(self.cd.curpath, self.cd.directory)

    def test_cd__step_4(self):
        self.cd.can_continue = True
        self.cd.directory = "../"

        self.cd._step_4()

    def test_cd__step_5(self):
        self.cd.can_continue = True
        old_pwd = os.getcwd()
        old_cdpath_var = None
        if os.environ.get("CDPATH"):
            old_cdpath_var = os.environ.get("CDPATH")

        os.environ["CDPATH"] = "{0}:/usr".format(os.path.abspath(self.temp_dir_1))

        os.chdir(old_pwd)
        self.cd.directory = old_pwd
        self.cd._step_5()
        self.assertEqual(old_pwd, self.cd.curpath)

        os.mkdir(self.temp_dir_1)
        os.mkdir(self.temp_dir_2)

        self.cd.directory = "42"
        self.cd._step_5()
        self.assertEqual(os.path.abspath(self.temp_dir_2), self.cd.curpath)

        # os.chdir(old_pwd)
        # self.cd.directory = self.temp_dir_1
        # self.cd._step_5()
        # self.assertEqual(self.temp_dir_2, self.cd.curpath)

        os.chdir(old_pwd)
        self.cd.directory = self.temp_dir_1
        self.cd._step_5()
        self.assertEqual(
            ".{0}{1}".format(os.path.sep, self.temp_dir_1), self.cd.curpath
        )

        if os.environ.get("CDPATH"):
            del os.environ["CDPATH"]
        if old_cdpath_var:
            os.environ["CDPATH"] = old_cdpath_var

    def test_cd__step_6(self):
        self.cd.can_continue = True
        self.cd.directory = self.temp_dir_2
        self.cd._step_6()

        self.assertEqual(
            os.path.join(os.environ.get("PWD"), self.cd.directory), self.cd.curpath
        )

    def test_cd__step_7_no_search_file_or_directory(self):
        self.cd.can_continue = True
        self.cd.physical = True
        self.cd.curpath = os.path.join(os.getcwd(), self.temp_dir_1)
        with patch("sys.stderr", new=StringIO()) as fake_out:
            expected = "cd: {0}: No such file or directory\n".format(
                os.path.join(os.getcwd(), self.temp_dir_1)
            )
            self.cd._step_7()
            self.assertEqual(fake_out.getvalue(), expected)
        self.assertFalse(self.cd.can_continue)

    def test_cd__step_7(self):
        self.cd.can_continue = True
        self.cd.physical = True
        old_pwd = os.getcwd()
        os.mkdir(self.temp_dir_1)
        os.mkdir(self.temp_dir_2)

        self.cd.curpath = self.temp_dir_1
        os.environ["OLDPWD"] = os.path.abspath(os.path.join(os.getcwd(), ".."))
        self.cd._step_7()
        self.assertEqual(
            os.getcwd(),
            os.path.abspath(os.path.join(os.getcwd(), "..", self.temp_dir_1)),
        )

        self.assertFalse(self.cd.can_continue)



        os.chdir(old_pwd)

    def test_cd__step_8(self):
        self.cd.can_continue = True
        self.cd.curpath = "../../"
        old_curpath = self.cd.curpath
        self.cd._step_8()
        self.assertEqual(os.path.abspath(old_curpath), self.cd.curpath)

    def test_cd__step_9_no_search_file_or_directory(self):
        self.cd.can_continue = True
        self.cd.physical = True
        self.cd.curpath = os.path.join(os.getcwd(), self.temp_dir_1)
        with patch("sys.stderr", new=StringIO()) as fake_out:
            expected = "cd: {0}: No such file or directory\n".format(
                os.path.join(os.getcwd(), self.temp_dir_1)
            )
            self.cd._step_9()
            self.assertEqual(fake_out.getvalue(), expected)
        self.assertFalse(self.cd.can_continue)

    def test_cd__step_9(self):
        self.cd.can_continue = True
        self.cd.physical = True
        old_pwd = os.getcwd()
        os.mkdir(self.temp_dir_1)
        os.mkdir(self.temp_dir_2)

        self.cd.curpath = self.temp_dir_1
        self.assertEqual(os.getcwd(), os.getcwd())
        self.cd._step_9()
        self.assertEqual(
            os.getcwd(),
            os.path.abspath(os.path.join(os.getcwd(), "..", self.temp_dir_1)),
        )

        self.assertFalse(self.cd.can_continue)
        os.chdir(old_pwd)

    def test_cd(self):
        old_pwd = os.getcwd()
        old_pwd_var = None
        if os.environ.get("PWD"):
            old_pwd_var = os.environ.get("PWD")
        old_oldpwd_var = None
        if os.environ.get("OLDPWD"):
            old_oldpwd_var = os.environ.get("OLDPWD")

        os.mkdir(self.temp_dir_1)
        os.mkdir(self.temp_dir_2)

        self.cd.cd(directory=None, physical=True, logical=True)
        self.assertFalse(self.cd.physical)
        self.assertTrue(self.cd.logical)
        self.cd.cd(directory=None, physical=False, logical=False)
        self.assertFalse(self.cd.physical)
        self.assertTrue(self.cd.logical)

        os.chdir(old_pwd)

        os.environ["PWD"] = os.path.abspath(old_pwd)
        os.environ["OLDPWD"] = os.path.abspath(self.temp_dir_1)
        self.cd.cd(directory="-", physical=False, logical=False)

        self.assertEqual(
            self.cd.directory,
            os.path.abspath(os.path.join(os.getcwd(), "..", self.temp_dir_1)),
        )

        os.chdir(old_pwd)
        os.environ["PWD"] = os.path.abspath(old_pwd)
        del os.environ["OLDPWD"]
        self.cd.cd(directory="-", physical=False, logical=False)

        self.assertEqual(
            self.cd.directory,
            os.path.abspath(os.environ["PWD"]),
        )

        os.environ["OLDPWD"] = os.path.abspath(os.getcwd())
        os.chdir(old_pwd)
        os.environ["PWD"] = os.path.abspath(old_pwd)

        self.cd.cd(
            directory=os.path.abspath(self.temp_dir_1), physical=False, logical=False
        )

        self.assertEqual(
            self.cd.directory,
            os.path.abspath(os.path.join(os.getcwd(), "..", self.temp_dir_1)),
        )

        os.chdir(old_pwd)
        if old_pwd_var:
            os.environ["PWD"] = old_pwd_var
        if old_oldpwd_var:
            os.environ["OLDPWD"] = old_oldpwd_var

    def test_cd_complete_cd(self):
        os.mkdir(self.temp_dir_1)
        os.mkdir(self.temp_dir_2)
        self.assertEqual(
            [self.temp_dir_2],
            self.cd.complete_cd(
                text="{0}{1}".format(self.temp_dir_1, os.path.sep),
                line="cd",
                begidx=len(self.temp_dir_1),
                endidx=len(self.temp_dir_1),
            ),
        )


if __name__ == "__main__":
    unittest.main()
